package com.dhp.hins.view.household;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.dhp.hins.R;
import com.dhp.hins.adapter.AdapterDropDown;
import com.dhp.hins.item.ItemHousehold;
import com.dhp.hins.storage.LDatabase;
import com.dhp.hins.utils.LUtils;

import java.util.ArrayList;
import java.util.Arrays;

import es.dmoral.toasty.Toasty;

import static com.dhp.hins.utils.LConstants.KEY_HOUSEHOLD_ID;
import static com.dhp.hins.utils.LConstants.KEY_LAUNCH_TYPE;
import static com.dhp.hins.utils.LConstants.LAUNCH_TYPE_ADD;
import static com.dhp.hins.utils.LConstants.LAUNCH_TYPE_EDIT;
import static com.dhp.hins.utils.LConstants.SPINNER_HOUSE_TYPE;
import static com.dhp.hins.utils.LConstants.SPINNER_LOCALITY;
import static com.dhp.hins.utils.LUtils.getItemWithOthersIndex;
import static com.dhp.hins.utils.LUtils.getRadioIndex;
import static com.dhp.hins.utils.LUtils.getSpinnerIndex;

/**
 * Created by Prabhat on 14-Apr-17.
 */

public class ActivityHouseholdEntry extends AppCompatActivity {

    private int mLaunchType;

    private String[] mLocalities;
    private String[] mHouseTypes;

    private EditText mEtHouseNo1;
    private EditText mEtHouseNo2;
    private EditText mEtStreetName;
    private EditText mEtRespondent;
    private EditText mEtTotalMembers;
    private EditText mEtLandmark;
    private EditText mEtTotalRooms;
    private EditText mEtHouseHead;
    private EditText mEtHouseHeadSpouse;

    private EditText mEtStatusOther;

    private Spinner mSpLocality;
    private Spinner mSpHouseType;

    private RadioGroup mRgHouseStatus;
    private RadioGroup mRgHouseOwnership;

    private ItemHousehold mItemHousehold;

    private LDatabase mDb;
    private boolean autoCheck;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_household_entry);

        LUtils.initToolbar(this, true, getString(R.string.title_household));

        mLaunchType = getIntent().getIntExtra(KEY_LAUNCH_TYPE, LAUNCH_TYPE_ADD);

        mDb = LDatabase.getInstance(this);

        initValues();

        if (null != savedInstanceState) {
            // TODO: 26-Jul-17  
//            restoreValues(savedInstanceState);
        }

        initUi();

        if (LAUNCH_TYPE_EDIT == mLaunchType) {
            populateUi();
            populateUiMore();
        }

        mEtHouseNo1.requestFocus();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setMessage(getString(R.string.msg_go_back));
        builder.setPositiveButton(getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityHouseholdEntry.super.onBackPressed();
            }
        });

        builder.setNegativeButton(getString(R.string.btn_no), null);
        builder.show();
    }

    private void initValues() {
        mLocalities = mDb.getLocalities();
        mHouseTypes = mDb.getHouseTypes();

        if (LAUNCH_TYPE_EDIT == mLaunchType) {
            long householdIdKey = getIntent().getLongExtra(KEY_HOUSEHOLD_ID, 0);
            mItemHousehold = mDb.getHouseholdDetails(householdIdKey);
        } else {
            mItemHousehold = new ItemHousehold();
            // TODO: 29-Jul-17, Default locality is Indirapuram
//            mItemHousehold.houseLocality = mLocalities[1];
        }
    }

    private void initUi() {
        mEtHouseNo1 = (EditText) findViewById(R.id.et_house_no_1);
        mEtHouseNo2 = (EditText) findViewById(R.id.et_house_no_2);
        mEtStatusOther = (EditText) findViewById(R.id.et_status_other);
        mEtStatusOther.setVisibility(View.GONE);
        mEtStreetName = (EditText) findViewById(R.id.et_street_name);
        mEtLandmark = (EditText) findViewById(R.id.et_landmark);
        mEtHouseNo2 = (EditText) findViewById(R.id.et_house_no_2);
        mEtTotalRooms = (EditText) findViewById(R.id.et_total_rooms);
        mEtRespondent = (EditText) findViewById(R.id.et_respondent_name);
        mEtHouseHead = (EditText) findViewById(R.id.et_house_head);
        mEtHouseHeadSpouse = (EditText) findViewById(R.id.et_house_head_spouse);
        mEtTotalMembers = (EditText) findViewById(R.id.et_total_members);

        findViewById(R.id.btn_save_household).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveHousehold();
            }
        });

        mRgHouseStatus = (RadioGroup) findViewById(R.id.rg_house_status);
        mRgHouseStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (autoCheck) {
                    autoCheck = false;
                    return;
                }
                mItemHousehold.houseStatus = ((RadioButton) findViewById(indexId)).getText().toString();

                if (mItemHousehold.houseStatus.equals(getString(R.string.text_open))) {
                    showOpenStatusQues();
                } else {
                    clearOpenStatusQues();
                }

                if (mItemHousehold.houseStatus.equals("Others")) {
                    mEtStatusOther.setVisibility(View.VISIBLE);
                } else {
                    mEtStatusOther.setVisibility(View.GONE);
                }
            }
        });

        mSpLocality = (Spinner) findViewById(R.id.sp_locality);
        final AdapterDropDown<String> spLocalityAdapter = new AdapterDropDown<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(mLocalities)),
                SPINNER_LOCALITY
        );
        spLocalityAdapter.setDropDownViewResource(R.layout.spinner_layout);
        mSpLocality.setAdapter(spLocalityAdapter);
        mSpLocality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    mItemHousehold.houseLocality = mLocalities[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (LAUNCH_TYPE_EDIT != mLaunchType) {
            if (null != mLocalities && 2 < mLocalities.length) {
                mSpLocality.setSelection(2);
            }
        }

        mSpHouseType = (Spinner) findViewById(R.id.sp_type_of_house);
        final AdapterDropDown<String> spHouseTypeAdapter = new AdapterDropDown<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(mHouseTypes)),
                SPINNER_HOUSE_TYPE
        );
        spHouseTypeAdapter.setDropDownViewResource(R.layout.spinner_layout);
        mSpHouseType.setAdapter(spHouseTypeAdapter);
        mSpHouseType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String selectedItemText = (String) parent.getItemAtPosition(position);
                if (position > 0) {
                    mItemHousehold.houseType = mHouseTypes[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        mRgHouseOwnership = (RadioGroup) findViewById(R.id.rg_type_of_house);
        mRgHouseOwnership.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }
                mItemHousehold.houseOwnership = ((RadioButton) findViewById(indexId)).getText().toString();
            }
        });
    }

    private void populateUi() {
        mEtHouseNo1.setText(mItemHousehold.houseNo1);
        mEtHouseNo1.setSelection(mEtHouseNo1.getText().length());
        mEtHouseNo2.setText(mItemHousehold.houseNo2);

        mEtHouseNo1.setEnabled(false);
        mEtHouseNo2.setEnabled(false);

        String[] houseStatuses = getResources().getStringArray(R.array.house_status);

        int statusIndex = getItemWithOthersIndex(mItemHousehold.houseStatus, houseStatuses);

        if (-1 != statusIndex) {

            autoCheck = true;

            ((RadioButton) mRgHouseStatus.getChildAt(statusIndex))
                    .setChecked(true);
        }

        if ((houseStatuses.length - 1) == statusIndex) {
            mEtStatusOther.setText(mItemHousehold.houseStatus);
            mEtStatusOther.setVisibility(View.VISIBLE);
        }

        if (null == mItemHousehold.houseStatus) {
            showOpenStatusQues();
        }
    }

    private void populateUiMore() {
        if (0 < mItemHousehold.totalRooms) {
            mEtTotalRooms.setText("" + mItemHousehold.totalRooms);
        } else {
            mEtTotalRooms.setText("");
        }

        mEtStreetName.setText(mItemHousehold.streetName);
        mEtLandmark.setText(mItemHousehold.landmark);

        try {
            mSpLocality.setSelection(getSpinnerIndex(mItemHousehold.houseLocality, mLocalities));
        } catch (NullPointerException e) {
        }

        try {
            ((RadioButton) mRgHouseOwnership.getChildAt(getRadioIndex(mItemHousehold.houseOwnership,
                    getResources().getStringArray(R.array.house_ownership_types))))
                    .setChecked(true);
        } catch (NullPointerException e) {
            mRgHouseOwnership.clearCheck();
        }

        mEtRespondent.setText(mItemHousehold.houseRespondent);
        mEtHouseHead.setText(mItemHousehold.houseHead);
        mEtHouseHeadSpouse.setText(mItemHousehold.houseHeadSpouse);

        if (0 < mItemHousehold.totalMembers) {
            mEtTotalMembers.setText("" + mItemHousehold.totalMembers);
        } else {
            mEtTotalMembers.setText("");
        }
    }

    private void readValues() {
        mItemHousehold.houseNo1 = mEtHouseNo1.getText().toString();
        mItemHousehold.houseNo2 = mEtHouseNo2.getText().toString();

        if (null == mItemHousehold.houseStatus
                || (!mItemHousehold.houseStatus.equals("Open")
                && !mItemHousehold.houseStatus.equals("Locked")
                && !mItemHousehold.houseStatus.equals("Vacant"))) {
            mItemHousehold.houseStatus = mEtStatusOther.getText().toString();
        }

        mItemHousehold.streetName = mEtStreetName.getText().toString();
        mItemHousehold.landmark = mEtLandmark.getText().toString();

        try {
            mItemHousehold.totalRooms = Integer.parseInt(mEtTotalRooms.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mItemHousehold.houseRespondent = mEtRespondent.getText().toString();
        mItemHousehold.houseHead = mEtHouseHead.getText().toString();
        mItemHousehold.houseHeadSpouse = mEtHouseHeadSpouse.getText().toString();

        try {
            mItemHousehold.totalMembers = Integer.parseInt(mEtTotalMembers.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isAllFieldsEntered() {
        if (mItemHousehold.houseNo1.equals("") || mItemHousehold.houseNo2.equals("")) {
            Toasty.warning(this, getString(R.string.error_house_no), Toast.LENGTH_LONG).show();
            return false;

        } else if (null == mItemHousehold.houseStatus || mItemHousehold.houseStatus.isEmpty()) {
            Toasty.warning(this, getString(R.string.error_status), Toast.LENGTH_LONG).show();
            return false;

        } else if (null == mItemHousehold.houseLocality) {
            Toasty.warning(this, getString(R.string.error_locality), Toast.LENGTH_LONG).show();
            return false;

        } else if (!mItemHousehold.houseStatus.equals(mDb.getHouseStatuses()[0])) { // !Open
            return true;

        } else if (null == mItemHousehold.houseType) {
            Toasty.warning(this, getString(R.string.error_house_type), Toast.LENGTH_LONG).show();
            return false;

        } else if (null == mItemHousehold.houseOwnership) {
            Toasty.warning(this, getString(R.string.error_house_rent_type), Toast.LENGTH_LONG).show();
            return false;

        } else if (!(mItemHousehold.totalRooms > 0)) {
            Toasty.warning(this, getString(R.string.error_rooms), Toast.LENGTH_LONG).show();
            return false;

        } else if (mItemHousehold.houseRespondent.equals("")) {
            Toasty.warning(this, getString(R.string.error_respondent), Toast.LENGTH_LONG).show();
            return false;

        } else if (mItemHousehold.houseHead.equals("")) {
            Toasty.warning(this, getString(R.string.error_house_head), Toast.LENGTH_LONG).show();
            return false;

//        } else if (mItemHousehold.houseHeadSpouse.equals("")) {
//            Toasty.warning(this, getString(R.string.error_house_head_spouse), Toast.LENGTH_LONG).show();
//            return false;

        } else if (!(mItemHousehold.totalMembers > 0)) {
            Toasty.warning(this, getString(R.string.error_members), Toast.LENGTH_LONG).show();
            return false;

        } else {
            return true;
        }
    }

    private void saveHousehold() {
        readValues();

        if (!isAllFieldsEntered()) {
            return;
        }

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setMessage(getString(R.string.message_confirm_save));
        builder.setPositiveButton(getString(R.string.btn_save), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (LAUNCH_TYPE_EDIT == mLaunchType) {
                    String id = mDb.updateHousehold(mItemHousehold);
                    Toasty.success(ActivityHouseholdEntry.this, getString(R.string.msg_update_success)
                            + "\nHouse No. " + id, Toast.LENGTH_LONG).show();
                } else {
                    String id = mDb.addHousehold(mItemHousehold);
                    Toasty.success(ActivityHouseholdEntry.this, getString(R.string.msg_save_success)
                            + "\nHouse No. " + id, Toast.LENGTH_LONG).show();
                }

                finish();
            }
        });
        builder.setNegativeButton(getString(R.string.btn_cancel), null);
        builder.show();
    }

    private void clearOpenStatusQues() {
        findViewById(R.id.ll_open_status_ques).setVisibility(View.GONE);
        mItemHousehold.houseType = null;
        mItemHousehold.houseOwnership = null;
        mItemHousehold.totalRooms = 0;
        mItemHousehold.houseRespondent = "";
        mItemHousehold.houseHead = "";
        mItemHousehold.houseHeadSpouse = "";
        mItemHousehold.totalMembers = 0;

        initUi();
        populateUiMore();
    }

    private void showOpenStatusQues() {
        findViewById(R.id.ll_open_status_ques).setVisibility(View.VISIBLE);
    }
}
