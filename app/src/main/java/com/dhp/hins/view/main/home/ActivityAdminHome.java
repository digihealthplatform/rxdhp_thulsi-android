package com.dhp.hins.view.main.home;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.hins.BuildConfig;
import com.dhp.hins.R;
import com.dhp.hins.adapter.AdapterLocality;
import com.dhp.hins.adapter.AdapterNurse;
import com.dhp.hins.item.ItemCamp;
import com.dhp.hins.item.ItemLocality;
import com.dhp.hins.item.ItemNurse;
import com.dhp.hins.storage.LDatabase;
import com.dhp.hins.utils.LUtils;
import com.dhp.hins.view.main.login.ActivityLogin;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.Calendar;

import es.dmoral.toasty.Toasty;

import static com.dhp.hins.storage.LDatabase.DATABASE_NAME;
import static com.dhp.hins.storage.LDatabase.TABLE_CAMP;
import static com.dhp.hins.utils.LConstants.CODE_READ_PERMISSION;
import static com.dhp.hins.utils.LConstants.CODE_WRITE_PERMISSION;
import static com.dhp.hins.utils.LConstants.KEY_DEVICE_ID;
import static com.dhp.hins.utils.LConstants.KEY_INSTALLATION_DATE;
import static com.dhp.hins.utils.LConstants.KEY_PERSON_LOGGED_IN;
import static com.dhp.hins.utils.LUtils.getFormattedDate;

/**
 * Created by Prabhat on 14-Apr-17.
 */

public class ActivityAdminHome extends AppCompatActivity {

    String mCampId;
    String mCampTitle;
    String mStartDate;
    String mEndDate;
    String mDeviceId;

    private RecyclerView mRvNurse;
    private RecyclerView mRvLocality;

    private TextView mTvCampId;
    private TextView mTvCampTitle;
    private TextView mTvCampStartDate;
    private TextView mTvCampEndDate;

    private LDatabase          mDb;
    private InputMethodManager mInputMethodManager;
    private TextView           mTvDeviceId;
    private AdapterNurse       mAdapterNurse;
    private AdapterLocality    mAdapterLocality;
    private long               mStartDateMilli;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_admin_home);

        LUtils.initToolbar(this, false, getString(R.string.app_name));

        mDb = LDatabase.getInstance(this);

        mInputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        initUi();

        populateUi();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_admin, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_export_configuration: {
                exportDatabaseWithPermissionCheck();
            }
            break;

            case R.id.action_import_configuration: {
                importDatabaseWithPermissionCheck();
            }
            break;

            case R.id.action_about: {
                AlertDialog.Builder builder;

                builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

                builder.setTitle(getString(R.string.title_about));
                builder.setMessage(getString(R.string.msg_application_version) + " " + BuildConfig.VERSION_NAME
                        + "\n" + getString(R.string.msg_installation_date) + " " + LUtils.getStringPref(KEY_INSTALLATION_DATE, ""));
                builder.setPositiveButton(getString(R.string.btn_ok), null);
                builder.show();
            }
            break;

            case R.id.action_logout: {
                AlertDialog.Builder builder;

                builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

                builder.setMessage(getString(R.string.msg_logout));
                builder.setPositiveButton(getString(R.string.btn_logout), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LUtils.putStringPref(KEY_PERSON_LOGGED_IN, "");
                        finish();
                        startActivity(new Intent(ActivityAdminHome.this, ActivityLogin.class));
                    }
                });
                builder.setNegativeButton(getString(R.string.btn_cancel), null);
                builder.show();
            }
            break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void importDatabaseWithPermissionCheck() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (PackageManager.PERMISSION_GRANTED != permissionCheck) {

            ActivityCompat.requestPermissions((Activity) this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    CODE_READ_PERMISSION);
        } else {
            importDatabase();
        }
    }

    private void exportDatabaseWithPermissionCheck() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (PackageManager.PERMISSION_GRANTED != permissionCheck) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CODE_WRITE_PERMISSION);
        } else {
            exportDatabase();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case CODE_WRITE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    exportDatabase();
                } else {
                    Toasty.error(this, getString(R.string.message_permission_denied_write), Toast.LENGTH_SHORT).show();
                }
            }
            break;

            case CODE_READ_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    importDatabase();
                } else {
                    Toasty.error(this, getString(R.string.message_permission_denied_write), Toast.LENGTH_SHORT).show();
                }
            }
            break;
        }
    }

    private void exportDatabase() {

        try {
            File outputPath = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.export_configuration_path));

            if (!outputPath.exists()) {
                outputPath.mkdirs();
            }

            Calendar calendar = Calendar.getInstance();

            if (outputPath.canWrite()) {

                String backupDBPath = getString(R.string.prefix_export_configuration) + ".db";

                File currentDB = getDatabasePath(DATABASE_NAME);
                File backupDB  = new File(outputPath, backupDBPath);

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();

                Toasty.success(this, getString(R.string.message_export_database_success), Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toasty.error(this, getString(R.string.message_export_error), Toast.LENGTH_LONG).show();
        }
    }

    private void importDatabase() {
        String configPath = Environment.getExternalStorageDirectory() + getString(R.string.configuration_file_path);

        final File file = new File(configPath);

        if (!file.exists()) {
            Toasty.error(this, getString(R.string.msg_config_not_found), Toast.LENGTH_SHORT).show();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setMessage(getString(R.string.message_import_dp));
        builder.setPositiveButton(getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                replaceDb(file.getPath());
            }
        });
        builder.setNegativeButton(getString(R.string.btn_cancel), null);
        builder.show();
    }

    private void replaceDb(String fromPath) {

//        SQLiteDatabase checkDB = SQLiteDatabase.openDatabase(fromPath, null, SQLiteDatabase.OPEN_READONLY);
//
//        LDbTransfer.transfer(checkDB, this);

        try {
            // Check if it is valid
            SQLiteDatabase checkDB = SQLiteDatabase.openDatabase(fromPath, null, SQLiteDatabase.OPEN_READONLY);

            try {
                Cursor cursor = checkDB.rawQuery("SELECT " + LDatabase.CAMP_KEY + " FROM " + TABLE_CAMP, null);
                cursor.close();

            } catch (SQLException e) {
                Toasty.error(this, getString(R.string.message_error_invalid_db), Toast.LENGTH_LONG).show();
                return;
            }

            File currentDB = getDatabasePath(DATABASE_NAME);
            File backupDB  = new File(fromPath);

            FileChannel dst = new FileOutputStream(currentDB).getChannel();
            FileChannel src = new FileInputStream(backupDB).getChannel();
            dst.transferFrom(src, 0, src.size());
            src.close();
            dst.close();

            Toasty.success(this, getString(R.string.message_import_success), Toast.LENGTH_LONG).show();

            recreate();

        } catch (Exception e) {
            e.printStackTrace();
            Toasty.error(this, getString(R.string.message_error), Toast.LENGTH_LONG).show();
        }
    }

    private void initUi() {
        mRvNurse = (RecyclerView) findViewById(R.id.rv_nurse_list);
        mRvNurse.setLayoutManager(new LinearLayoutManager(this));

        mRvLocality = (RecyclerView) findViewById(R.id.rv_locality_list);
        mRvLocality.setLayoutManager(new LinearLayoutManager(this));

        mTvCampId = (TextView) findViewById(R.id.tv_camp_id);
        mTvCampTitle = (TextView) findViewById(R.id.tv_camp_title);
        mTvCampStartDate = (TextView) findViewById(R.id.tv_start_date);
        mTvCampEndDate = (TextView) findViewById(R.id.tv_end_date);
        mTvDeviceId = (TextView) findViewById(R.id.tv_device_id);

        findViewById(R.id.tv_edit_camp_details).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editCampDetails();
            }
        });

        findViewById(R.id.tv_edit_device_details).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editDeviceDetails();
            }
        });

        findViewById(R.id.tv_add_nurse).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNurse();
            }
        });

        findViewById(R.id.tv_add_locality).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addLocality();
            }
        });
    }

    private void populateUi() {
        populateCampDetails();
        populateDeviceDetails();
        populateNurseList();
        populateLocalityList();
    }

    private void populateCampDetails() {

        ItemCamp itemCamp = mDb.getCampDetails();

        mCampId = "";
        mCampTitle = "";
        mStartDate = "";
        mEndDate = "";

        if (null != itemCamp) {
            mCampId = itemCamp.campId;
            mCampTitle = itemCamp.campTitle;
            mStartDate = itemCamp.startDate;
            mEndDate = itemCamp.endDate;

            String[] dateSplit = mStartDate.split("/");

            mStartDateMilli = getTimeInMillis(Integer.valueOf(dateSplit[0]),
                    Integer.valueOf(dateSplit[1]) - 1,
                    Integer.valueOf(dateSplit[2]));

        }

        mTvCampId.setText(getString(R.string.label_camp_id) + " " + mCampId);
        mTvCampTitle.setText(getString(R.string.label_camp_title) + " " + mCampTitle);
        mTvCampStartDate.setText(getString(R.string.label_start_date) + " " + mStartDate);
        mTvCampEndDate.setText(getString(R.string.label_end_date) + " " + mEndDate);
    }

    private void populateDeviceDetails() {
        mDeviceId = LUtils.getStringPref(KEY_DEVICE_ID, "");
        mTvDeviceId.setText(getString(R.string.label_device_id) + " " + mDeviceId);
    }

    private void populateNurseList() {
        mAdapterNurse = new AdapterNurse(this, mDb.getNurseList());
        mRvNurse.setAdapter(mAdapterNurse);
    }

    private void populateLocalityList() {
        mAdapterLocality = new AdapterLocality(this, mDb.getLocalityList());
        mRvLocality.setAdapter(mAdapterLocality);
    }

    private void editCampDetails() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_add_camp_details, null);

        final Calendar calendar = Calendar.getInstance();

        final EditText etCampId    = (EditText) dialogView.findViewById(R.id.et_camp_id);
        final EditText etCampTitle = (EditText) dialogView.findViewById(R.id.et_camp_title);
        final EditText etStartDate = (EditText) dialogView.findViewById(R.id.et_start_date);
        final EditText etEndDate   = (EditText) dialogView.findViewById(R.id.et_end_date);

        etCampId.setText(mCampId);
        etCampId.setSelection(etCampId.getText().length());

        etCampTitle.setText(mCampTitle);
        etCampTitle.setSelection(etCampTitle.getText().length());

        etStartDate.setText(mStartDate);
        etStartDate.setSelection(etStartDate.getText().length());

        etEndDate.setText(mEndDate);
        etEndDate.setSelection(etEndDate.getText().length());

        dialogView.findViewById(R.id.et_start_date).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (MotionEvent.ACTION_UP == motionEvent.getAction()) {

                    DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                    mStartDate = getFormattedDate(dayOfMonth, monthOfYear + 1, year);

                                    mEndDate = mStartDate;

                                    etStartDate.setText(mStartDate);
                                    etStartDate.setSelection(etStartDate.getText().length());

                                    etEndDate.setText(mStartDate);
                                    etEndDate.setSelection(etStartDate.getText().length());

                                    mStartDateMilli = getTimeInMillis(dayOfMonth, monthOfYear, year);
                                }
                            },
                            calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DATE));

                    datePickerDialog.show(getFragmentManager(), "DatePickerDialog");

                }
                return false;
            }
        });

        dialogView.findViewById(R.id.et_end_date).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (MotionEvent.ACTION_UP == motionEvent.getAction()) {

                    DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                                    if (getTimeInMillis(dayOfMonth, monthOfYear, year) < mStartDateMilli) {
                                        Toasty.warning(ActivityAdminHome.this, getString(R.string.msg_end_date_cant_be_less), Toast.LENGTH_SHORT).show();
                                    } else {
                                        mEndDate = getFormattedDate(dayOfMonth, monthOfYear + 1, year);
                                        etEndDate.setText(mEndDate);
                                        etEndDate.setSelection(etEndDate.getText().length());
                                    }
                                }
                            },

                            calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DATE));

                    datePickerDialog.show(getFragmentManager(), "DatePickerDialog");

                }
                return false;
            }
        });

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setView(dialogView);

        builder.setPositiveButton(getString(R.string.btn_save), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(getString(R.string.btn_cancel), null);

        final AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String campId    = etCampId.getText().toString();
                String campTitle = etCampTitle.getText().toString();

                if (!campId.equals("") && !campTitle.equals("") && !mStartDate.equals("") && !mEndDate.equals("")) {

                    mCampId = campId;
                    mCampTitle = campTitle;

                    mDb.saveCampDetails(campId, campTitle, mStartDate, mEndDate);

                    mInputMethodManager.hideSoftInputFromWindow(etCampId.getWindowToken(), 0);

                    dialog.dismiss();

                    populateCampDetails();
                    Toasty.success(ActivityAdminHome.this, getString(R.string.msg_update_success), Toast.LENGTH_SHORT).show();
                } else {
                    Toasty.warning(ActivityAdminHome.this, getString(R.string.msg_fill_all_fields), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void editDeviceDetails() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_add_device_details, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setView(dialogView);

        builder.setPositiveButton(getString(R.string.btn_save), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(getString(R.string.btn_cancel), null);

        final AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);

        final EditText etDeviceId = (EditText) dialogView.findViewById(R.id.et_device_id);
        etDeviceId.setText(mDeviceId);
        etDeviceId.setSelection(etDeviceId.getText().length());

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String deviceId = etDeviceId.getText().toString();

                if (!deviceId.equals("")) {

                    mDeviceId = deviceId;
                    LUtils.putStringPref(KEY_DEVICE_ID, deviceId);

                    mInputMethodManager.hideSoftInputFromWindow(etDeviceId.getWindowToken(), 0);

                    dialog.dismiss();
                    populateDeviceDetails();

                    Toasty.success(ActivityAdminHome.this, getString(R.string.msg_update_success), Toast.LENGTH_SHORT).show();
                } else {
                    Toasty.warning(ActivityAdminHome.this, getString(R.string.msg_fill_all_fields), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addNurse() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_add_nurse, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setView(dialogView);

        builder.setPositiveButton(getString(R.string.btn_save), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(getString(R.string.btn_cancel), null);

        final AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);

        final EditText etNurseName     = (EditText) dialogView.findViewById(R.id.et_nurse_name);
        final EditText etNurseUserName = (EditText) dialogView.findViewById(R.id.et_nurse_user_name);
        final EditText etNursePassword = (EditText) dialogView.findViewById(R.id.et_nurse_password);

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nurseName     = etNurseName.getText().toString();
                String nursePassword = etNursePassword.getText().toString();
                String nurseUserName = etNurseUserName.getText().toString();

                if (!nurseName.equals("") && !nursePassword.equals("") && !nurseUserName.equals("")) {

                    if (mDb.nurseExists(nurseUserName)) {
                        Toasty.warning(ActivityAdminHome.this, getString(R.string.msg_nurse_exits), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    ItemNurse itemNurse = mDb.addNurse(nurseName, nurseUserName, nursePassword);

                    mAdapterNurse.addItem(itemNurse);

                    mInputMethodManager.hideSoftInputFromWindow(etNursePassword.getWindowToken(), 0);

                    dialog.dismiss();
                    Toasty.success(ActivityAdminHome.this, getString(R.string.msg_add_success), Toast.LENGTH_SHORT).show();

                } else {
                    Toasty.warning(ActivityAdminHome.this, getString(R.string.msg_fill_all_fields), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addLocality() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_add_locality, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setView(dialogView);

        builder.setPositiveButton(getString(R.string.btn_save), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(getString(R.string.btn_cancel), null);

        final AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);

        final EditText etLocality = (EditText) dialogView.findViewById(R.id.et_locality);

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String locality = etLocality.getText().toString();

                if (!locality.equals("")) {

                    if (mDb.localityExists(locality)) {
                        Toasty.warning(ActivityAdminHome.this, getString(R.string.msg_locality_exists), Toast.LENGTH_SHORT).show();

                        return;
                    }

                    ItemLocality itemLocality = mDb.addLocality(locality);

                    mAdapterLocality.addItem(itemLocality);

                    mInputMethodManager.hideSoftInputFromWindow(etLocality.getWindowToken(), 0);

                    dialog.dismiss();
                    Toasty.success(ActivityAdminHome.this, getString(R.string.msg_add_success), Toast.LENGTH_SHORT).show();
                } else {
                    Toasty.warning(ActivityAdminHome.this, getString(R.string.msg_fill_all_fields), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private long getTimeInMillis(int date, int month, int year) {

        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.DATE, date);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTimeInMillis();
    }
}
