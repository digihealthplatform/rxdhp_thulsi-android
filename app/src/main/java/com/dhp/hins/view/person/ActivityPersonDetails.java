package com.dhp.hins.view.person;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.hins.R;
import com.dhp.hins.item.ItemDisease;
import com.dhp.hins.item.ItemPerson;
import com.dhp.hins.storage.LDatabase;
import com.dhp.hins.utils.LUtils;

import es.dmoral.toasty.Toasty;

import static com.dhp.hins.utils.LConstants.DEFAULT_BP_UNIT;
import static com.dhp.hins.utils.LConstants.DEFAULT_HB_UNIT;
import static com.dhp.hins.utils.LConstants.DEFAULT_INCOME_UNIT;
import static com.dhp.hins.utils.LConstants.DEFAULT_RBS_UNIT;
import static com.dhp.hins.utils.LConstants.KEY_PERSON_ID;
import static com.dhp.hins.utils.LUtils.getSexIndex;

/**
 * Created by Prabhat on 14-Apr-17.
 */

public class ActivityPersonDetails extends AppCompatActivity {

    private ItemPerson mItemPerson;

    private TextView mTvPersonId;
    private TextView mTvFamilyId;
    private TextView mTvName;
    private TextView mTvRelation;
    private TextView mTvAge;
    private TextView mTvSex;
    private TextView mTvResident;
    private TextView mTvDob;
    private TextView mTvEducation;
    private TextView mTvLiteracy;
    private TextView mTvOccupation;
    private TextView mTvOccupationType;
    private TextView mTvIncome;
    private TextView mTvIdentity;
    private TextView mTvIdentityNo;
    private TextView mTvHeight;
    private TextView mTvWeight;
    private TextView mTvBmi;
    private TextView mTvBp;
    private TextView mTvRbs;
    private TextView mTvHb;
    private TextView mTvMarital;
    private TextView mTvLanguages;

    private String[] mHeightUnitList;

    private TextView mTvSchool;
    private TextView mTvSchoolTitle;
    private TextView mTvHabits;
    private TextView mTvHabitsTitle;
    private TextView mTvExercise;
    private TextView mTvExerciseTitle;
    private TextView mTvRbsAllowTitle;
    private TextView mTvRbsAllow;
    private TextView mTvHbAllowTitle;
    private TextView mTvHbAllow;
    private TextView mTvDiseases;
    private TextView mTvDisabilities;
    private TextView mTvSick;
    private TextView mTvAilment;
    private LDatabase mDb;

    private TextView mTvInactiveReasonLabel;
    private TextView mTvInactiveReason;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_person_details);

        LUtils.initToolbar(this, true, getString(R.string.title_person));

        long personIdKey;

        if (null != savedInstanceState) {
            // TODO: 16-Apr-17  
            personIdKey = -1;
        } else {
            personIdKey = getIntent().getLongExtra(KEY_PERSON_ID, -1);
        }

        mDb = LDatabase.getInstance(this);

        mHeightUnitList = mDb.getHeightUnitList();

        mItemPerson = mDb.getPersonDetails(personIdKey);

        initUi();

        populateUi();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_person, menu);

        if (!mDb.isPersonActive(mItemPerson.personIdKey)) {
            menu.findItem(R.id.action_inactive).setChecked(true);
            mTvInactiveReasonLabel.setVisibility(View.VISIBLE);
            mTvInactiveReason.setVisibility(View.VISIBLE);
            mTvInactiveReason.setText(mDb.getPersonInactiveReason(mItemPerson.personIdKey));
            findViewById(R.id.sv_main).setBackgroundColor(ContextCompat.getColor(this, R.color.inactive_color));
        } else {
            menu.findItem(R.id.action_inactive).setChecked(false);
            mTvInactiveReasonLabel.setVisibility(View.GONE);
            mTvInactiveReason.setVisibility(View.GONE);
            findViewById(R.id.sv_main).setBackgroundColor(ContextCompat.getColor(this, R.color.mdtp_white));
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
            }
            break;

            case R.id.action_print_data: {
                if (mItemPerson.familyId.equals("")) {
                    Toasty.warning(ActivityPersonDetails.this, getString(R.string.msg_fill_family_id), Toast.LENGTH_LONG).show();
                } else {
                    // TODO: 17-Apr-17  
                }
            }
            break;

            case R.id.action_inactive: {
                if (mDb.isPersonActive(mItemPerson.personIdKey)) {
                    confirmInactive();
                } else {
                    confirmActive();
                }
            }
            break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void confirmActive() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage(getString(R.string.message_confirm_active));
        builder.setPositiveButton(getString(R.string.btn_make_active), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDb.setActivePerson(mItemPerson.personIdKey, "No", "");
                invalidateOptionsMenu();
            }
        });
        builder.setNegativeButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                invalidateOptionsMenu();
            }
        });

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                invalidateOptionsMenu();
            }
        });

        builder.show();
    }

    private void confirmInactive() {
        final View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_inactive, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setTitle("Reason for making it inactive:");
        builder.setView(dialogView);

        final EditText etReason = (EditText) dialogView.findViewById(R.id.et_inactive_reason);

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                invalidateOptionsMenu();
            }
        });

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDb.setActivePerson(mItemPerson.personIdKey, "Yes", etReason.getText().toString());
                invalidateOptionsMenu();
                noteDialog.dismiss();
            }
        });

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                invalidateOptionsMenu();
            }
        });
    }

    private void initUi() {
        mTvPersonId = (TextView) findViewById(R.id.tv_person_id);
        mTvFamilyId = (TextView) findViewById(R.id.tv_family_id);
        mTvName = (TextView) findViewById(R.id.tv_person_name);

        mTvDob = (TextView) findViewById(R.id.tv_dob);
        mTvAge = (TextView) findViewById(R.id.tv_age);
        mTvSex = (TextView) findViewById(R.id.tv_sex);
        mTvRelation = (TextView) findViewById(R.id.tv_relation);
        mTvResident = (TextView) findViewById(R.id.tv_resident);
        mTvMarital = (TextView) findViewById(R.id.tv_marital);
        mTvLanguages = (TextView) findViewById(R.id.tv_languages);

        mTvEducation = (TextView) findViewById(R.id.tv_education);
        mTvLiteracy = (TextView) findViewById(R.id.tv_literacy);
        mTvOccupation = (TextView) findViewById(R.id.tv_occupation);
        mTvOccupationType = (TextView) findViewById(R.id.tv_occupation_type);
        mTvIncome = (TextView) findViewById(R.id.tv_income);
        mTvIdentity = (TextView) findViewById(R.id.tv_identity_type);
        mTvIdentityNo = (TextView) findViewById(R.id.tv_identity_number);

        mTvSchool = (TextView) findViewById(R.id.tv_school);
        mTvSchoolTitle = (TextView) findViewById(R.id.tv_school_title);
        mTvHabits = (TextView) findViewById(R.id.tv_habits);
        mTvHabitsTitle = (TextView) findViewById(R.id.tv_habits_title);
        mTvExercise = (TextView) findViewById(R.id.tv_exercise);
        mTvExerciseTitle = (TextView) findViewById(R.id.tv_exercise_title);
        mTvRbsAllowTitle = (TextView) findViewById(R.id.tv_rbs_allow_title);
        mTvRbsAllow = (TextView) findViewById(R.id.tv_rbs_allow);
        mTvHbAllowTitle = (TextView) findViewById(R.id.tv_hb_allow_title);
        mTvHbAllow = (TextView) findViewById(R.id.tv_hb_allow);
        mTvDiseases = (TextView) findViewById(R.id.tv_diseases);
        mTvDisabilities = (TextView) findViewById(R.id.tv_disabilities);
        mTvSick = (TextView) findViewById(R.id.tv_sick);
        mTvAilment = (TextView) findViewById(R.id.tv_ailment);

        mTvHeight = (TextView) findViewById(R.id.tv_height);
        mTvWeight = (TextView) findViewById(R.id.tv_weight);
        mTvBmi = (TextView) findViewById(R.id.tv_bmi);
        mTvBp = (TextView) findViewById(R.id.tv_bp);
        mTvRbs = (TextView) findViewById(R.id.tv_rbs);
        mTvHb = (TextView) findViewById(R.id.tv_hb);

        mTvInactiveReasonLabel = (TextView) findViewById(R.id.tv_inactive_reason_label);
        mTvInactiveReason = (TextView) findViewById(R.id.tv_inactive_reason);
    }

    private void populateUi() {
        mTvFamilyId.setText(mItemPerson.familyId);
        mTvPersonId.setText(mItemPerson.personId);
        mTvName.setText(mItemPerson.name);

        if (!mItemPerson.dob.equals("")) {
            mTvDob.setVisibility(View.VISIBLE);
            mTvDob.setText(mItemPerson.dob);
            findViewById(R.id.tv_dob_title).setVisibility(View.VISIBLE);
        }

        if (mItemPerson.ageYear > 0) {
            String s = "";
            if (mItemPerson.ageYear > 1) {
                s = "s";
            }
            mTvAge.setText("" + mItemPerson.ageYear + " Year" + s);
        } else if (mItemPerson.ageMonth > 0) {
            String s = "";
            if (mItemPerson.ageMonth > 1) {
                s = "s";
            }
            mTvAge.setText("" + mItemPerson.ageMonth + " Month" + s);
        } else if (mItemPerson.ageDay > 0) {
            String s = "";
            if (mItemPerson.ageDay > 1) {
                s = "s";
            }
            mTvAge.setText("" + mItemPerson.ageDay + " Day" + s);
        }
        mTvSex.setText(mItemPerson.sex);
        mTvRelation.setText(mItemPerson.relationToHoh);
        mTvResident.setText(mItemPerson.resident);
        mTvMarital.setText(mItemPerson.marital);
        mTvEducation.setText(mItemPerson.education);
        mTvLiteracy.setText(mItemPerson.literacy);
        mTvOccupation.setText(mItemPerson.occupation);
        mTvOccupationType.setText(mItemPerson.occupationType);

        if (mItemPerson.income > 0) {
            mTvIncome.setText("" + mItemPerson.income + " " + DEFAULT_INCOME_UNIT);

            if (null != mItemPerson.incomeType) {
                mTvIncome.setText(mTvIncome.getText().toString() + " (" + mItemPerson.incomeType + ")");
            }
        }

        mTvIdentity.setText(mItemPerson.identityType);
        mTvIdentityNo.setText(mItemPerson.identityNo);

        if (3 <= mItemPerson.ageYear && 6 >= mItemPerson.ageYear) {
            // Anganawadi
            mTvSchoolTitle.setText(getString(R.string.ques_anganwadi));
            mTvSchoolTitle.setVisibility(View.VISIBLE);
            mTvSchool.setText(mItemPerson.school);
            mTvSchool.setVisibility(View.VISIBLE);
        } else if (7 <= mItemPerson.ageYear && 16 >= mItemPerson.ageYear) {
            // School
            mTvSchoolTitle.setText(getString(R.string.ques_school));
            mTvSchoolTitle.setVisibility(View.VISIBLE);
            mTvSchool.setText(mItemPerson.school);
            mTvSchool.setVisibility(View.VISIBLE);
        }

        if (10 < mItemPerson.ageYear) {
            mTvHabitsTitle.setVisibility(View.VISIBLE);
            mTvHabits.setVisibility(View.VISIBLE);

            String habits = "";

            if (null != mItemPerson.smoking) {
                habits = "Smoking : " + mItemPerson.smoking + "\n";
            }

            if (null != mItemPerson.tobacco) {
                habits += "Tobacco Chewing : " + mItemPerson.tobacco + "\n";
            }

            if (null != mItemPerson.drinking) {
                habits += "Drinking : " + mItemPerson.drinking + "\n";
            }
            mTvHabits.setText(habits);

            mTvExerciseTitle.setVisibility(View.VISIBLE);
            mTvExercise.setVisibility(View.VISIBLE);
            mTvExercise.setText(mItemPerson.exercise);
        }

        String diseases = "";
        if (null != mItemPerson.diseaseList) {
            for (ItemDisease itemDisease : mItemPerson.diseaseList) {
                diseases += itemDisease.disease + "\n";
            }
        }

        mTvDiseases.setText(diseases);

        mTvSick.setText(mItemPerson.sick);
        mTvAilment.setText(mItemPerson.ailment);

        String disabilities = "";
        if (null != mItemPerson.disabilityList) {
            for (String disability : mItemPerson.disabilityList) {
                disabilities += disability + "\n";
            }
        }
        mTvDisabilities.setText(disabilities);

        if (null != mItemPerson.heightUnit) {
            mTvHeight.setText(mItemPerson.height + " (" + mItemPerson.heightUnit + ")");
        }

        if (null != mItemPerson.weightUnit) {
            mTvWeight.setText(mItemPerson.weight + " (" + mItemPerson.weightUnit + ")");
        }

        findBmi();

        if (!mItemPerson.bpDiastolic.equals("") && !mItemPerson.bpSystolic.equals("")) {
            mTvBp.setText(mItemPerson.bpSystolic + "/" + mItemPerson.bpDiastolic + " (" + DEFAULT_BP_UNIT + ")");
            mTvBp.setVisibility(View.VISIBLE);
            findViewById(R.id.tv_bp_title).setVisibility(View.VISIBLE);
        }

        if (mItemPerson.ageYear >= 30) {
            mTvRbsAllowTitle.setVisibility(View.VISIBLE);
            mTvRbsAllow.setVisibility(View.VISIBLE);

            if (null != mItemPerson.rbsAllow) {
                mTvRbsAllow.setText(mItemPerson.rbsAllow);
            }
        }

        if (mItemPerson.ageYear >= 12 && 1 == getSexIndex(mItemPerson.sex)) {
            mTvHbAllowTitle.setVisibility(View.VISIBLE);
            mTvHbAllow.setVisibility(View.VISIBLE);

            if (null != mItemPerson.hbAllow) {
                mTvHbAllow.setText(mItemPerson.hbAllow);
            }
        }

        if (!mItemPerson.rbs.equals("")) {
            mTvRbs.setText(mItemPerson.rbs + " (" + DEFAULT_RBS_UNIT + ")");
            mTvRbs.setVisibility(View.VISIBLE);
            findViewById(R.id.tv_rbs_title).setVisibility(View.VISIBLE);
        }

        if (!mItemPerson.hb.equals("")) {
            mTvHb.setText(mItemPerson.hb + " (" + DEFAULT_HB_UNIT + ")");
            mTvHb.setVisibility(View.VISIBLE);
            findViewById(R.id.tv_hb_title).setVisibility(View.VISIBLE);
        }

        try {
            String languages = mItemPerson.languagesString.replace(";", "\n");
            mTvLanguages.setText(languages);
        } catch (Exception e) {

        }
    }

    private void findBmi() {

        try {
            float height = Float.parseFloat(mItemPerson.height);
            float weight = Float.parseFloat(mItemPerson.weight);

            float bmi = 0.0f;

            // Find height in metre

            if (mItemPerson.heightUnit.equals(mHeightUnitList[1])) { // cm
                height = height / 100.0f;
                bmi = weight / (height * height);

                double bmiRoundOff = Math.round(bmi * 100.0) / 100.0;

                mTvBmi.setText("" + bmiRoundOff + " " + getString(R.string.unit_bmi));
            } else if (mItemPerson.heightUnit.equals(mHeightUnitList[2])) { // inch
                height = height * 0.0254f;
                bmi = weight / (height * height);

                double bmiRoundOff = Math.round(bmi * 100.0) / 100.0;

                mTvBmi.setText("" + bmiRoundOff + " " + getString(R.string.unit_bmi));
            }
        } catch (Exception e) {

        }
    }
}
