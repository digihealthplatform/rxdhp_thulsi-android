package com.dhp.hins.view.family;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.hins.R;
import com.dhp.hins.item.ItemDeathInfo;
import com.dhp.hins.item.ItemFamily;
import com.dhp.hins.item.ItemPerson;
import com.dhp.hins.storage.LDatabase;
import com.dhp.hins.utils.LUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;
import java.util.UUID;

import es.dmoral.toasty.Toasty;

import static com.dhp.hins.utils.LConstants.KEY_FAMILY_ID;
import static com.dhp.hins.utils.LUtils.getYesNoIndex;

/**
 * Created by Prabhat on 14-Apr-17.
 */

public class ActivityFamilyDetails extends AppCompatActivity {

    private int readBufferPosition;

    private byte FONT_TYPE;
    private byte[] readBuffer;

    private volatile boolean stopWorker;

    private TextView mTvFamilyId;
    private TextView mTvRationCard;
    private TextView mTvInsurance;
    private TextView mTvMealTimings;
    private TextView mTvCookingFuel;
    private TextView mTvTreatmentPlace;
    private TextView mTvDeath;

    private TextView mTvInactiveReasonLabel;
    private TextView mTvInactiveReason;

    private ItemFamily mItemFamily;

    private LDatabase mDb;

    private OutputStream mmOutputStream;
    private InputStream mmInputStream;
    private Thread workerThread;

    private BluetoothSocket mmSocket;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDevice mmDevice;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_family_details);

        LUtils.initToolbar(this, true, getString(R.string.title_family));

        long familyIdKey;

        if (null != savedInstanceState) {
            // TODO: 16-Apr-17  
            familyIdKey = -1;
        } else {
            familyIdKey = getIntent().getLongExtra(KEY_FAMILY_ID, -1);
        }

        mDb = LDatabase.getInstance(this);

        mItemFamily = mDb.getFamilyDetails(familyIdKey);

        initUi();

        populateUi();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_family, menu);

        if (!mDb.isFamilyActive(mItemFamily.familyIdKey)) {
            menu.findItem(R.id.action_inactive).setChecked(true);
            mTvInactiveReasonLabel.setVisibility(View.VISIBLE);
            mTvInactiveReason.setVisibility(View.VISIBLE);
            mTvInactiveReason.setText(mDb.getFamilyInactiveReason(mItemFamily.familyIdKey));
            findViewById(R.id.sv_main).setBackgroundColor(ContextCompat.getColor(this, R.color.inactive_color));
        } else {
            menu.findItem(R.id.action_inactive).setChecked(false);
            mTvInactiveReasonLabel.setVisibility(View.GONE);
            mTvInactiveReason.setVisibility(View.GONE);
            findViewById(R.id.sv_main).setBackgroundColor(ContextCompat.getColor(this, R.color.mdtp_white));
        }

        return super.onCreateOptionsMenu(menu);
    }

    private void initUi() {
        mTvFamilyId = (TextView) findViewById(R.id.tv_family_id);
        mTvRationCard = (TextView) findViewById(R.id.tv_ration_card);
        mTvInsurance = (TextView) findViewById(R.id.tv_insurance);
        mTvMealTimings = (TextView) findViewById(R.id.tv_meal_timings);
        mTvCookingFuel = (TextView) findViewById(R.id.tv_cooking_fuel);
        mTvTreatmentPlace = (TextView) findViewById(R.id.tv_treatment_place);
        mTvDeath = (TextView) findViewById(R.id.tv_death);

        mTvInactiveReasonLabel = (TextView) findViewById(R.id.tv_inactive_reason_label);
        mTvInactiveReason = (TextView) findViewById(R.id.tv_inactive_reason);
    }

    private void populateUi() {
        mTvFamilyId.setText(mItemFamily.familyId);

        if (0 == getYesNoIndex(mItemFamily.rationCard)) {
            mTvRationCard.setText(mItemFamily.rationCardType);
        } else {
            mTvRationCard.setText(getString(R.string.text_no));
        }

        if (0 == getYesNoIndex(mItemFamily.insurance)) {
            mTvInsurance.setText(mItemFamily.insuranceType);
        } else {
            mTvInsurance.setText(getString(R.string.text_no));
        }

        mTvMealTimings.setText(mItemFamily.mealTimings);
        mTvCookingFuel.setText(mItemFamily.cookingFuel);
        mTvTreatmentPlace.setText(mItemFamily.treatmentPlace);

        if (null != mItemFamily.deathInfoList) {
            String text = "";

            for (ItemDeathInfo deathInfo : mItemFamily.deathInfoList) {
                text += "Name: " + deathInfo.name;
                text += "\nAge: " + deathInfo.age;
                text += " ( " + deathInfo.ageUnit + ")";
                text += "\nRelationship to HoH: " + deathInfo.relation;
                text += "\nDeath Cause: " + deathInfo.deathCause;
                text += "\n\n";
            }

            mTvDeath.setText(text);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
            }

            case R.id.action_print_data: {
                printFamilyDetails();
            }
            break;

            case R.id.action_inactive: {
                if (mDb.isFamilyActive(mItemFamily.familyIdKey)) {
                    confirmInactive();
                } else {
                    confirmActive();
                }
            }
            break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void confirmActive() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage(getString(R.string.message_confirm_active));
        builder.setPositiveButton(getString(R.string.btn_make_active), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDb.setActiveFamily(mItemFamily.familyIdKey, "No", "");
                invalidateOptionsMenu();
            }
        });
        builder.setNegativeButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                invalidateOptionsMenu();
            }
        });

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                invalidateOptionsMenu();
            }
        });

        builder.show();
    }

    private void confirmInactive() {
        final View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_inactive, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setTitle("Reason for making it inactive:");
        builder.setView(dialogView);

        final EditText etReason = (EditText) dialogView.findViewById(R.id.et_inactive_reason);

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                invalidateOptionsMenu();
            }
        });

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDb.setActiveFamily(mItemFamily.familyIdKey, "Yes", etReason.getText().toString());
                invalidateOptionsMenu();
                noteDialog.dismiss();
            }
        });

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                invalidateOptionsMenu();
            }
        });
    }

    private void printFamilyDetails() {
        findBT();
        try {
            openBT();
            sendData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void findBT() {

        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBluetoothAdapter == null) {
                // TODO: 29-Apr-17  
                Toasty.error(this, getString(R.string.msg_print_error), Toast.LENGTH_LONG).show();
            }

            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetooth, 0);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {

//                    if (device.getName().equals("MTP-II")) {    //("RPP300")) {
                    mmDevice = device;
                    break;
//                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void openBT() throws IOException {
        try {

            // Standard SerialPortService ID
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            mmSocket.connect();

            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();

            beginListenForData();


        } catch (Exception e) {
            Toasty.error(this, getString(R.string.msg_print_error), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    /*
     * after opening a connection to bluetooth printer device,
     * we have to listen and check if a data were sent to be printed.
     */
    void beginListenForData() {
        try {
            final Handler handler = new Handler();

            // this is the ASCII code for a newline character
            final byte delimiter = 10;

            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];

            workerThread = new Thread(new Runnable() {
                public void run() {

                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {

                        try {

                            int bytesAvailable = mmInputStream.available();

                            if (bytesAvailable > 0) {

                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);

                                for (int i = 0; i < bytesAvailable; i++) {

                                    byte b = packetBytes[i];
                                    if (b == delimiter) {

                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length
                                        );

                                        // specify US-ASCII encoding
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;


                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }

                        } catch (IOException ex) {
                            stopWorker = true;
                        }

                    }
                }
            });

            workerThread.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void sendData() throws IOException {
        ArrayList<ItemPerson> itemPersons = mDb.getPersonList(mItemFamily.familyId);

        if (0 == itemPersons.size()) {
            Toasty.warning(this, getString(R.string.msg_no_members), Toast.LENGTH_SHORT).show();
            return;
        }

        try {

            for (ItemPerson itemPerson : itemPersons) {

                byte[] arrayOfByte1 = {27, 33, 0};
                byte[] format = {27, 33, 0};

                byte[] left = new byte[]{0x1b, 0x61, 0x00};
                byte[] center = new byte[]{0x1b, 0x61, 0x01};

                //------------------------------- FORMAT 1 --------------------------------//

                Calendar c = Calendar.getInstance();

                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                String formattedDate = df.format(c.getTime());

                String date_time = " Date & Time " + formattedDate + "\n\n";

                mmOutputStream.write(left);
                mmOutputStream.write(format);
                mmOutputStream.write(date_time.getBytes(), 0, date_time.getBytes().length);

                OutputStream opstream = null;

                try {
                    opstream = mmSocket.getOutputStream();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mmOutputStream = opstream;

                try {
                    mmOutputStream = mmSocket.getOutputStream();
                    byte[] printformat = {0x1B, 0 * 21, FONT_TYPE};
                    mmOutputStream.write(printformat);

                } catch (IOException e) {
                    e.printStackTrace();
                }

                //------------------------------- FORMAT 2 --------------------------------//

                byte[] format2 = {27, 33, 0};
                format2[2] = (byte) (((byte) (0x80 | arrayOfByte1[2])) + ((byte) (0x8 | arrayOfByte1[2])) + ((byte) (0x10 | arrayOfByte1[2])));

                String heading = "BAPTIST HOSPITAL\n";

                heading += "\n";
                mmOutputStream.write(center);
                mmOutputStream.write(format2);
                mmOutputStream.write(heading.getBytes(), 0, heading.getBytes().length);

                // ------------------------------- FORMAT 3 ----------------------

                float weight = Float.parseFloat(itemPerson.weight);
                float height = Float.parseFloat(itemPerson.height);
                ;

                double bmiRoundOff = 0.0;

                if (itemPerson.heightUnit.equals(getString(R.string.unit_cm))) {
                    height = height / 100.0f;
                    float bmi = weight / (height * height);

                    bmiRoundOff = Math.round(bmi * 100.0) / 100.0;
                } else if (itemPerson.heightUnit.equals(getString(R.string.unit_in))) {
                    height = height * 0.0254f;
                    float bmi = weight / (height * height);
                    bmiRoundOff = Math.round(bmi * 100.0) / 100.0;
                }

                int bpSys = 0;
                int bpDia = 0;
                int rbs = 0;
                int hb = 0;

                try {
                    rbs = Integer.parseInt(itemPerson.rbs);
                } catch (Exception e) {

                }

                try {
                    bpSys = Integer.parseInt(itemPerson.bpSystolic);
                    bpDia = Integer.parseInt(itemPerson.bpDiastolic);
                } catch (Exception e) {

                }

                try {
                    hb = Integer.parseInt(itemPerson.hb);
                } catch (Exception e) {

                }

                String keyBmi, keyBp, keyRbs, keyHb;

                if ((bmiRoundOff > 24.99) || (bmiRoundOff < 18)) {
                    keyBmi = " * BMI(kg/m2) : " + bmiRoundOff;
                } else {
                    keyBmi = " BMI(kg/m2)   : " + bmiRoundOff;
                }

                if (itemPerson.bpDiastolic.equals("")) {
                    keyBp = " BP(mmHg)     : N/A";
                } else if (((bpSys > 140) || (bpDia > 90) || (bpSys < 80) || (bpDia < 60))) {
                    keyBp = " * BP(mmHg)   : " + itemPerson.bpSystolic + "/" + itemPerson.bpDiastolic;
                } else {
                    keyBp = " BP(mmHg)     : " + itemPerson.bpSystolic + "/" + itemPerson.bpDiastolic;
                }

                if (itemPerson.rbs.equals("")) {
                    keyRbs = " RBS(mg/dL)   : N/A";
                } else if ((rbs > 140) || (rbs < 60)) {
                    keyRbs = " * RBS(mg/dL) : " + itemPerson.rbs;
                } else {
                    keyRbs = " RBS(mg/dL)   : " + itemPerson.rbs;
                }

                if (itemPerson.hb.equals("")) {
                    keyHb = " HB(g/dL)     : N/A";
                } else if ((hb < 8)) {
                    keyHb = " * HB(g/dL)   : " + itemPerson.hb;
                } else {
                    keyHb = " HB(g/dL)     : " + itemPerson.hb;
                }

                int age = 0;

                String ageUnit = "";

                if (itemPerson.ageYear > 0) {
                    age = itemPerson.ageYear;
                    ageUnit = "yy";
                } else if (itemPerson.ageMonth > 0) {
                    age = itemPerson.ageMonth;
                    ageUnit = "mm";
                } else if (itemPerson.ageDay > 0) {
                    age = itemPerson.ageDay;
                    ageUnit = "dd";
                }

                String heightUnit = itemPerson.heightUnit;

                String reg_info = " Name  : " + itemPerson.name + "\n" +
                        " Reg.Id       : " + itemPerson.personId + "\n" +
                        " Age(" + ageUnit + ")      : " + age + "\n" +
                        " Gender       : " + itemPerson.sex + "\n" +
                        " Height(" + heightUnit + ")   : " + itemPerson.height + "\n" +
                        " Weight(kg)   : " + itemPerson.weight + "\n" +
                        keyBmi + "\n" +
                        keyBp + "\n" +
                        keyRbs + "\n" +
                        keyHb + "\n\n" +
                        " *Abnormal";

                reg_info += "\n\n\n";
                mmOutputStream.write(left);
                mmOutputStream.write(format);
                mmOutputStream.write(reg_info.getBytes(), 0, reg_info.getBytes().length);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void closeBT() throws IOException {
        try {
            stopWorker = true;
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
