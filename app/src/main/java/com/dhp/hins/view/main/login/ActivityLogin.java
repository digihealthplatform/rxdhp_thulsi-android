package com.dhp.hins.view.main.login;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.dhp.hins.BuildConfig;
import com.dhp.hins.R;
import com.dhp.hins.utils.LUtils;
import com.dhp.hins.view.main.home.ActivityAdminHome;
import com.dhp.hins.view.main.home.ActivityNurseHome;

import java.io.File;

import es.dmoral.toasty.Toasty;

import static com.dhp.hins.utils.LConstants.CODE_WRITE_PERMISSION;
import static com.dhp.hins.utils.LConstants.KEY_INSTALLATION_DATE;
import static com.dhp.hins.utils.LConstants.KEY_NURSE_USER_NAME;
import static com.dhp.hins.utils.LConstants.KEY_PERSON_LOGGED_IN;
import static com.dhp.hins.utils.LConstants.LOGGED_IN_ADMIN;
import static com.dhp.hins.utils.LConstants.LOGGED_IN_NURSE;

/**
 * Created by Prabhat on 14-Apr-17.
 */

public class ActivityLogin extends AppCompatActivity implements View.OnClickListener {

    private InputMethodManager mInputMethodManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        LUtils.initToolbar(this, false, getString(R.string.app_name));

        initUi();

        mInputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        
        createFolderWithPermissionCheck();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_about: {
                AlertDialog.Builder builder;

                builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

                builder.setTitle(getString(R.string.title_about));
                builder.setMessage(getString(R.string.msg_application_version) + " " + BuildConfig.VERSION_NAME
                        + "\n" + getString(R.string.msg_installation_date) + " " + LUtils.getStringPref(KEY_INSTALLATION_DATE, ""));
                builder.setPositiveButton(getString(R.string.btn_ok), null);
                builder.show();
            } break;
        }

        return super.onOptionsItemSelected(item);
    }
    
    private void initUi() {
        findViewById(R.id.btn_configure_app).setOnClickListener(this);
        findViewById(R.id.btn_nurse_login).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_configure_app: {
                configureApp();
            }
            break;

            case R.id.btn_nurse_login: {
                nurseLogin();
            }
            break;
        }
    }

    private void configureApp() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_admin_login, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setTitle(getString(R.string.title_admin_login));
        builder.setView(dialogView);

        builder.setPositiveButton(getString(R.string.btn_login), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(getString(R.string.btn_cancel), null);

        final AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);

        final EditText etAdminUserName = (EditText) dialogView.findViewById(R.id.et_admin_user_name);
        final EditText etAdminPassword = (EditText) dialogView.findViewById(R.id.et_admin_password);
        
        etAdminPassword.setTypeface(Typeface.DEFAULT);

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String adminUserName = etAdminUserName.getText().toString();
                String adminPassword = etAdminPassword.getText().toString();

                if (LUtils.checkAdminLogin(adminUserName, adminPassword)) {
                    startActivity(new Intent(ActivityLogin.this, ActivityAdminHome.class));
                    mInputMethodManager.hideSoftInputFromWindow(etAdminUserName.getWindowToken(), 0);

                    LUtils.putStringPref(KEY_PERSON_LOGGED_IN, LOGGED_IN_ADMIN);
                    
                    dialog.dismiss();
                    
                    finish();
                } else {
                    Toasty.error(ActivityLogin.this, getString(R.string.msg_wrong_admin_credentials), Toast.LENGTH_SHORT).show();
                }                
            }
        });
    }

    private void nurseLogin() {

        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_nurse_login, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setTitle(getString(R.string.title_nurse_login));
        builder.setView(dialogView);

        builder.setPositiveButton(getString(R.string.btn_login), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(getString(R.string.btn_cancel), null);

        final AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);

        final EditText etNurseUserName = (EditText) dialogView.findViewById(R.id.et_nurse_user_name);
        final EditText etNursePassword = (EditText) dialogView.findViewById(R.id.et_nurse_password);

        etNursePassword.setTypeface(Typeface.DEFAULT);

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nurseUserName = etNurseUserName.getText().toString();
                String nursePassword = etNursePassword.getText().toString();

                if (LUtils.checkNurseLogin(nurseUserName, nursePassword)) {
                    LUtils.putStringPref(KEY_NURSE_USER_NAME, nurseUserName);
                    LUtils.putStringPref(KEY_PERSON_LOGGED_IN, LOGGED_IN_NURSE);
                    startActivity(new Intent(ActivityLogin.this, ActivityNurseHome.class));
                    
                    mInputMethodManager.hideSoftInputFromWindow(etNurseUserName.getWindowToken(), 0);

                    dialog.dismiss();

                    finish();
                } else {
                    Toasty.error(ActivityLogin.this, getString(R.string.msg_wrong_nurse_credentials), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    
    private void createFolderWithPermissionCheck() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (PackageManager.PERMISSION_GRANTED != permissionCheck) {

            ActivityCompat.requestPermissions((Activity) this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    CODE_WRITE_PERMISSION);
        } else {
            createFolder();
        }
    }
    
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case CODE_WRITE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    createFolder();
                } else {
                    Toasty.error(this, getString(R.string.message_permission_denied_write), Toast.LENGTH_SHORT).show();
                }
            } break;
        }
    }

    private void createFolder() {
        File outputPath = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.export_configuration_path));

        if (!outputPath.exists()) {
            outputPath.mkdirs();
        }
    }
}
