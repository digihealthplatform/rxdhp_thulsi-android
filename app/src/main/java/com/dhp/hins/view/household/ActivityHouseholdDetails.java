package com.dhp.hins.view.household;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dhp.hins.R;
import com.dhp.hins.item.ItemHousehold;
import com.dhp.hins.storage.LDatabase;
import com.dhp.hins.utils.LConstants;
import com.dhp.hins.utils.LUtils;

import static com.dhp.hins.utils.LConstants.KEY_HOUSEHOLD_ID;

/**
 * Created by Prabhat on 14-Apr-17.
 */

public class ActivityHouseholdDetails extends AppCompatActivity {

    private int totalSurveyDone;

    private ItemHousehold mItemHousehold;

    private TextView mTvSurveyDate;
    private TextView mTvSurveyPerson;

    private TextView mTvHouseholdNo;
    private TextView mTvInactiveReasonLabel;
    private TextView mTvInactiveReason;
    private TextView mTvStatus;
    private TextView mTvStreetName;
    private TextView mTvLandmark;
    private TextView mTvLocality;
    private TextView mTvHouseType;
    private TextView mTvHouseOwnership;
    private TextView mTvTotalRooms;
    private TextView mTvRespondent;
    private TextView mTvHouseHead;
    private TextView mTvHouseHeadSpouse;
    private TextView mTvTotalMembers;
    private TextView mTvTotalSurveyDone;
    private LDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_household_details);

        LUtils.initToolbar(this, true, getString(R.string.title_household));

        long householdIdKey;

        if (null != savedInstanceState) {
            // TODO: 16-Apr-17, Restore 
            householdIdKey = -1;
        } else {
            householdIdKey = getIntent().getLongExtra(KEY_HOUSEHOLD_ID, -1);
        }

        db = LDatabase.getInstance(this);

        mItemHousehold = db.getHouseholdDetails(householdIdKey);
        totalSurveyDone = db.getTotalSurveyDone(mItemHousehold.houseNo1 + "/" + mItemHousehold.houseNo2);

        initUi();
        populateUi();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_household, menu);

        if (!db.isHouseholdActive(mItemHousehold.householdIdKey)) {
            menu.findItem(R.id.action_inactive).setChecked(true);
            mTvInactiveReasonLabel.setVisibility(View.VISIBLE);
            mTvInactiveReason.setVisibility(View.VISIBLE);
            mTvInactiveReason.setText(db.getHouseInactiveReason(mItemHousehold.householdIdKey));
            findViewById(R.id.sv_main).setBackgroundColor(ContextCompat.getColor(this, R.color.inactive_color));
        } else {
            menu.findItem(R.id.action_inactive).setChecked(false);
            mTvInactiveReasonLabel.setVisibility(View.GONE);
            mTvInactiveReason.setVisibility(View.GONE);
            findViewById(R.id.sv_main).setBackgroundColor(ContextCompat.getColor(this, R.color.mdtp_white));
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
            }
            break;

            case R.id.action_inactive: {
                if (db.isHouseholdActive(mItemHousehold.householdIdKey)) {
                    confirmInactive();
                } else {
                    confirmActive();
                }
            }
            break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void confirmActive() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage(getString(R.string.message_confirm_active));
        builder.setPositiveButton(getString(R.string.btn_make_active), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                db.setActiveHousehold(mItemHousehold.householdIdKey, "No", "");
                invalidateOptionsMenu();
            }
        });
        builder.setNegativeButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                invalidateOptionsMenu();
            }
        });

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                invalidateOptionsMenu();
            }
        });

        builder.show();
    }

    private void confirmInactive() {
        final View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_inactive, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setTitle("Reason for making it inactive:");
        builder.setView(dialogView);

        final EditText etReason = (EditText) dialogView.findViewById(R.id.et_inactive_reason);

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                invalidateOptionsMenu();
            }
        });

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.setActiveHousehold(mItemHousehold.householdIdKey, "Yes", etReason.getText().toString());
                invalidateOptionsMenu();
                noteDialog.dismiss();
            }
        });

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                invalidateOptionsMenu();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // TODO: 26-Jul-17, Save 
        super.onSaveInstanceState(outState);
    }

    private void initUi() {
        mTvSurveyDate = (TextView) findViewById(R.id.tv_survey_date);
        mTvSurveyPerson = (TextView) findViewById(R.id.tv_survey_person);

        mTvHouseholdNo = (TextView) findViewById(R.id.tv_household_no);
        mTvInactiveReasonLabel = (TextView) findViewById(R.id.tv_inactive_reason_label);
        mTvInactiveReason = (TextView) findViewById(R.id.tv_inactive_reason);
        mTvStatus = (TextView) findViewById(R.id.tv_status);
        mTvStreetName = (TextView) findViewById(R.id.tv_street_name);
        mTvLandmark = (TextView) findViewById(R.id.tv_landmark);
        mTvLocality = (TextView) findViewById(R.id.tv_locality);
        mTvHouseType = (TextView) findViewById(R.id.tv_house_type);
        mTvHouseOwnership = (TextView) findViewById(R.id.tv_house_ownership);
        mTvTotalRooms = (TextView) findViewById(R.id.tv_total_rooms);
        mTvRespondent = (TextView) findViewById(R.id.tv_respondent);
        mTvHouseHead = (TextView) findViewById(R.id.tv_house_head);
        mTvHouseHeadSpouse = (TextView) findViewById(R.id.tv_house_head_spouse);
        mTvTotalMembers = (TextView) findViewById(R.id.tv_total_members);
        mTvTotalSurveyDone = (TextView) findViewById(R.id.tv_total_survey_done);
    }

    private void populateUi() {
        mTvSurveyDate.setText(mItemHousehold.surveyDate);
        mTvSurveyPerson.setText(mItemHousehold.surveyPerson);

        mTvHouseholdNo.setText(mItemHousehold.houseNo1 + "/" + mItemHousehold.houseNo2);
        mTvStatus.setText(mItemHousehold.houseStatus);
        mTvStreetName.setText(mItemHousehold.streetName);
        mTvLandmark.setText(mItemHousehold.landmark);
        mTvLocality.setText(mItemHousehold.houseLocality);
        mTvHouseType.setText(mItemHousehold.houseType);
        mTvHouseOwnership.setText(mItemHousehold.houseOwnership);

        if (0 < mItemHousehold.totalRooms) {
            mTvTotalRooms.setText("" + mItemHousehold.totalRooms);
        }

        mTvRespondent.setText(mItemHousehold.houseRespondent);
        mTvHouseHead.setText(mItemHousehold.houseHead);
        mTvHouseHeadSpouse.setText(mItemHousehold.houseHeadSpouse);

        if (0 < mItemHousehold.totalMembers) {
            mTvTotalMembers.setText("" + mItemHousehold.totalMembers);
            mTvTotalSurveyDone.setText(totalSurveyDone + " / " + mItemHousehold.totalMembers);
        } else {
            mTvTotalSurveyDone.setText(totalSurveyDone + " / __");
        }
    }
}
