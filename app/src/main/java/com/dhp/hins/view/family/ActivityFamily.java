package com.dhp.hins.view.family;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.dhp.hins.R;
import com.dhp.hins.adapter.AdapterFamily;
import com.dhp.hins.utils.LUtils;

import static com.dhp.hins.utils.LConstants.KEY_LAUNCH_TYPE;
import static com.dhp.hins.utils.LConstants.LAUNCH_TYPE_ADD;

/**
 * Created by Prabhat on 14-Apr-17.
 */

public class ActivityFamily extends AppCompatActivity {

    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_family);

        LUtils.initToolbar(this, true, getString(R.string.title_family));
        initUi();
    }

    @Override
    protected void onResume() {
        super.onResume();

        AdapterFamily adapterFamily = new AdapterFamily(this);

        mRecyclerView.setAdapter(adapterFamily);

        if (0 == adapterFamily.getItemCount()) {
            findViewById(R.id.tv_empty).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.tv_empty).setVisibility(View.GONE);
        }
    }

    private void initUi() {
        findViewById(R.id.fab_add_new_family).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ActivityFamily.this, ActivityFamilyEntry.class);
                intent.putExtra(KEY_LAUNCH_TYPE, LAUNCH_TYPE_ADD);
                startActivity(intent);
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_family_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_family_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
            }
            break;
            
            case R.id.action_search:
                Intent intent = new Intent(this, ActivityFamilySearch.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
