package com.dhp.hins.view.family;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.hins.R;
import com.dhp.hins.adapter.AdapterDeathInfo;
import com.dhp.hins.adapter.AdapterDropDown;
import com.dhp.hins.item.ItemDeathInfo;
import com.dhp.hins.item.ItemFamily;
import com.dhp.hins.storage.LDatabase;
import com.dhp.hins.utils.DialogSearch;
import com.dhp.hins.utils.DialogSearchCallbacks;
import com.dhp.hins.utils.LUtils;
import com.dhp.hins.view.person.ActivityPersonEntry;

import java.util.ArrayList;
import java.util.Arrays;

import es.dmoral.toasty.Toasty;

import static com.dhp.hins.utils.LConstants.KEY_FAMILY_ID;
import static com.dhp.hins.utils.LConstants.KEY_LAUNCH_TYPE;
import static com.dhp.hins.utils.LConstants.KEY_RECENT_HOUSE_NUMBER;
import static com.dhp.hins.utils.LConstants.LAUNCH_TYPE_ADD;
import static com.dhp.hins.utils.LConstants.LAUNCH_TYPE_EDIT;
import static com.dhp.hins.utils.LConstants.SPINNER_HOUSEHOLD_ID;
import static com.dhp.hins.utils.LConstants.SPINNER_INSURANCE;
import static com.dhp.hins.utils.LConstants.SPINNER_RATION_TYPE;
import static com.dhp.hins.utils.LConstants.SPINNER_TREATMENT;
import static com.dhp.hins.utils.LUtils.getContainsSpinnerIndex;
import static com.dhp.hins.utils.LUtils.getRadioWithOthersIndex;
import static com.dhp.hins.utils.LUtils.getSpinnerIndex;
import static com.dhp.hins.utils.LUtils.getYesNoIndex;

/**
 * Created by Prabhat on 14-Apr-17.
 */

public class ActivityFamilyEntry extends AppCompatActivity {

    private int mLaunchType;

    private boolean mIsCookingFuelOther;
    private boolean mIsCookingFuelOtherPopulated;

    private String[] mHouseNumbers;
    private String[] mRationCardTypes;
    private String[] mInsuranceTypes;
    private String[] mTreatmentPlaces;

    private static final String TAG = ActivityFamilyEntry.class.getSimpleName();

    private TextView mEtCookingFuelOther;

    private EditText mEtHouseNo;
    private Spinner mSpRationType;
    private Spinner mSpInsuranceType;
    private Spinner mSpTreatmentPlace;

    private RadioGroup mRgRation;
    private RadioGroup mRgInsurance;
    private RadioGroup mRgDeath;
    private RadioGroup mRgCookingFuel;

    private CheckBox mCbMorning;
    private CheckBox mCbAfternoon;
    private CheckBox mCbNight;

    private View mBtnAddMoreDeathInfo;

    private RecyclerView mRvDeathInfo;

    private ItemFamily mItemFamily;
    private LDatabase mDb;
    private AdapterDeathInfo mAdapterDeathInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_family_entry);

        LUtils.initToolbar(this, true, getString(R.string.title_family));

        mLaunchType = getIntent().getIntExtra(KEY_LAUNCH_TYPE, LAUNCH_TYPE_ADD);

        mDb = LDatabase.getInstance(this);

        initValues();

        if (null != savedInstanceState) {
            // TODO: 26-Jul-17  
//            restoreValues(savedInstanceState);
        }

        initUi();

        if (LAUNCH_TYPE_EDIT == mLaunchType) {
            populateUi();
        } else {
            mCbMorning.setChecked(true);
            mCbAfternoon.setChecked(true);
            mCbNight.setChecked(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setMessage(getString(R.string.msg_go_back));
        builder.setPositiveButton(getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityFamilyEntry.super.onBackPressed();
            }
        });

        builder.setNegativeButton(getString(R.string.btn_no), null);
        builder.show();
    }

    private void initValues() {
        mHouseNumbers = mDb.getHouseNumbers();
        mRationCardTypes = mDb.getRationTypes();
        mInsuranceTypes = mDb.getInsuranceTypes();
        mTreatmentPlaces = mDb.getTreatmentPlaces();

        if (LAUNCH_TYPE_EDIT == mLaunchType) {
            long familyIdKey = getIntent().getLongExtra(KEY_FAMILY_ID, 0);
            mItemFamily = mDb.getFamilyDetails(familyIdKey);
        } else {
            mItemFamily = new ItemFamily();
            mItemFamily.houseNumber = LUtils.getStringPref(KEY_RECENT_HOUSE_NUMBER, "");
        }
    }

    private void initUi() {
        mEtHouseNo = (EditText) findViewById(R.id.et_house_no);
//        mEtHouseNo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (position > 0) {
//                    mItemFamily.houseNumber = mHouseNumbers[position];
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });
//        setHouseholdIdAdapter();

        mEtHouseNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DialogSearch(ActivityFamilyEntry.this, new DialogSearchCallbacks() {
                    @Override
                    public void onSelected(String houseNo) {
                        mItemFamily.houseNumber = houseNo;
                        mEtHouseNo.setText(houseNo);
                    }
                }).withItems(mHouseNumbers).showDialog();

                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            }
        });

        mRgRation = (RadioGroup) findViewById(R.id.rg_ration);
        mRgRation.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                mItemFamily.rationCard = ((RadioButton) findViewById(indexId)).getText().toString();

                if (0 == getYesNoIndex(mItemFamily.rationCard)) {
                    mSpRationType.setEnabled(true);
                } else {
                    mItemFamily.rationCardType = null;
                    setRationTypeAdapter();
                    mSpRationType.setEnabled(false);
                }
            }
        });

        mSpRationType = (Spinner) findViewById(R.id.sp_ration_type);
        mSpRationType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    mItemFamily.rationCardType = mRationCardTypes[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        setRationTypeAdapter();
        mSpRationType.setEnabled(false);

        mRgInsurance = (RadioGroup) findViewById(R.id.rg_insurance);
        mRgInsurance.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                mItemFamily.insurance = ((RadioButton) findViewById(indexId)).getText().toString();

                if (0 == getYesNoIndex(mItemFamily.insurance)) {
                    mSpInsuranceType.setEnabled(true);
                } else {
                    // Clear selection
                    mItemFamily.insuranceType = null;
                    setInsuranceTypeAdapter();

                    mSpInsuranceType.setEnabled(false);
                }
            }
        });

        mSpInsuranceType = (Spinner) findViewById(R.id.sp_insurance_type);
        mSpInsuranceType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    mItemFamily.insuranceType = mInsuranceTypes[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        setInsuranceTypeAdapter();
        mSpInsuranceType.setEnabled(false);

        mSpTreatmentPlace = (Spinner) findViewById(R.id.sp_treatment_place);
        mSpTreatmentPlace.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    mItemFamily.treatmentPlace = mTreatmentPlaces[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        setTreatmentPlaceAdapter();

        mCbMorning = (CheckBox) findViewById(R.id.cb_morning);
        mCbAfternoon = (CheckBox) findViewById(R.id.cb_afternoon);
        mCbNight = (CheckBox) findViewById(R.id.cb_night);

        mRgCookingFuel = (RadioGroup) findViewById(R.id.rg_cooking_fuel);
        mRgCookingFuel.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (!mIsCookingFuelOtherPopulated) {
                    mItemFamily.cookingFuel = ((RadioButton) findViewById(indexId)).getText().toString();
                } else {
                    mIsCookingFuelOtherPopulated = false;
                    return;
                }

                if (mItemFamily.cookingFuel.equals(getString(R.string.text_other))) {
                    mEtCookingFuelOther.setEnabled(true);
                    mIsCookingFuelOther = true;
                } else {
                    mEtCookingFuelOther.setText("");
                    mEtCookingFuelOther.setEnabled(false);
                    mIsCookingFuelOther = false;
                }
            }
        });

        mEtCookingFuelOther = (TextView) findViewById(R.id.et_cooking_fuel_other);
        mEtCookingFuelOther.setEnabled(false);

        mRgDeath = (RadioGroup) findViewById(R.id.rg_death);
        mRgDeath.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                mItemFamily.recentDeath = ((RadioButton) findViewById(indexId)).getText().toString();

                if (0 == getYesNoIndex(mItemFamily.recentDeath)) {
                    showRecentDeathInfo();
                } else {
                    hideDeathInfo();
                }
            }
        });

        mRvDeathInfo = (RecyclerView) findViewById(R.id.rv_death_info);
        mRvDeathInfo.setLayoutManager(new LinearLayoutManager(this));
        mAdapterDeathInfo = new AdapterDeathInfo(this);
        mRvDeathInfo.setAdapter(mAdapterDeathInfo);

        mBtnAddMoreDeathInfo = findViewById(R.id.btn_add_more_death_info);
        mBtnAddMoreDeathInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapterDeathInfo.addNewItem(false);
            }
        });
        mBtnAddMoreDeathInfo.setVisibility(View.GONE);

        findViewById(R.id.btn_save_family).

                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        saveFamilyDetails();
                    }
                });
    }

    private void populateUi() {
        mEtHouseNo.setEnabled(false);
        mEtHouseNo.setText(mItemFamily.houseNumber);
//        mEtHouseNo.setSelection(getSpinnerIndex(mItemFamily.houseNumber, mHouseNumbers));

        ((RadioButton) mRgRation.getChildAt(getYesNoIndex(mItemFamily.rationCard))).setChecked(true);
        if (0 == getYesNoIndex(mItemFamily.rationCard)) {
            mSpRationType.setEnabled(true);
            mSpRationType.setSelection(getContainsSpinnerIndex(mItemFamily.rationCardType, mRationCardTypes));
        } else {
            mSpRationType.setEnabled(false);
        }

        ((RadioButton) mRgInsurance.getChildAt(getYesNoIndex(mItemFamily.insurance))).setChecked(true);
        mSpInsuranceType.setSelection(getSpinnerIndex(mItemFamily.insuranceType, mInsuranceTypes));
        if (0 == getYesNoIndex(mItemFamily.insurance)) {
            mSpInsuranceType.setEnabled(true);
        } else {
            mSpInsuranceType.setEnabled(false);
        }

        int cookingFuelIndex = getRadioWithOthersIndex(mItemFamily.cookingFuel,
                getResources().getStringArray(R.array.cooking_fuels));

        if (cookingFuelIndex == getResources().getStringArray(R.array.cooking_fuels).length - 1) {
            mIsCookingFuelOtherPopulated = true;
            mEtCookingFuelOther.setEnabled(true);
            mEtCookingFuelOther.setText(mItemFamily.cookingFuel);
            mIsCookingFuelOther = true;
        }

        if (-1 != cookingFuelIndex) {
            ((RadioButton) mRgCookingFuel.getChildAt(cookingFuelIndex))
                    .setChecked(true);
        }

        if (null != mItemFamily.mealTimings) {
            // Check check boxes
            if (mItemFamily.mealTimings.contains(getString(R.string.cb_morning))) {
                mCbMorning.setChecked(true);
            }

            if (mItemFamily.mealTimings.contains(getString(R.string.cb_afternoon))) {
                mCbAfternoon.setChecked(true);
            }

            if (mItemFamily.mealTimings.contains(getString(R.string.cb_night))) {
                mCbNight.setChecked(true);
            }
        }

        mSpTreatmentPlace.setSelection(getContainsSpinnerIndex(mItemFamily.treatmentPlace, mTreatmentPlaces));

        if (-1 != getYesNoIndex(mItemFamily.recentDeath)) {
            ((RadioButton) mRgDeath.getChildAt(getYesNoIndex(mItemFamily.recentDeath))).setChecked(true);
            mAdapterDeathInfo.setItems(mItemFamily.deathInfoList);
        }
    }

    private void saveFamilyDetails() {
        readValues();

        if (!isAllFieldsEntered()) {
            return;
        }

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setMessage(getString(R.string.message_confirm_save));
        builder.setPositiveButton(getString(R.string.btn_save), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (LAUNCH_TYPE_EDIT == mLaunchType) {
                    String id = mDb.updateFamily(mItemFamily);
                    Toasty.success(ActivityFamilyEntry.this, getString(R.string.msg_update_success)
                            + "\nFamily ID: " + id, Toast.LENGTH_LONG).show();
                } else {
                    String id = mDb.addFamily(mItemFamily);
                    Toasty.success(ActivityFamilyEntry.this, getString(R.string.msg_save_success)
                            + "\nFamily ID: " + id, Toast.LENGTH_LONG).show();
                }

                finish();
            }
        });
        builder.setNegativeButton(getString(R.string.btn_cancel), null);
        builder.show();
    }

    private void readValues() {
        mItemFamily.mealTimings = "";
        boolean isFirst = true;

        if (mCbMorning.isChecked()) {
            mItemFamily.mealTimings = getString(R.string.cb_morning);
            isFirst = false;
        }
        if (mCbAfternoon.isChecked()) {
            if (!isFirst) {
                mItemFamily.mealTimings += ", ";
            }
            mItemFamily.mealTimings += getString(R.string.cb_afternoon);
            isFirst = false;
        }
        if (mCbNight.isChecked()) {
            if (!isFirst) {
                mItemFamily.mealTimings += ", ";
            }
            mItemFamily.mealTimings += getString(R.string.cb_night);
        }

        if (mIsCookingFuelOther) {
            mItemFamily.cookingFuel = mEtCookingFuelOther.getText().toString();
        }

        mItemFamily.deathInfoList = mAdapterDeathInfo.getItems();
    }

    private boolean isAllFieldsEntered() {
        if (-1 == getYesNoIndex(mItemFamily.rationCard)) {
            Toasty.warning(this, getString(R.string.error_ration), Toast.LENGTH_LONG).show();
            return false;

        } else if (0 == getYesNoIndex(mItemFamily.rationCard)
                && null == mItemFamily.rationCardType) {
            Toasty.warning(this, getString(R.string.error_ration_type), Toast.LENGTH_LONG).show();
            return false;

        } else if (-1 == getYesNoIndex(mItemFamily.insurance)) {
            Toasty.warning(this, getString(R.string.error_insurance), Toast.LENGTH_LONG).show();
            return false;

        } else if (0 == getYesNoIndex(mItemFamily.insurance)
                && null == mItemFamily.insuranceType) {
            Toasty.warning(this, getString(R.string.error_insurance_type), Toast.LENGTH_LONG).show();
            return false;

        } else if (null == mItemFamily.mealTimings || mItemFamily.mealTimings.equals("")) {
            Toasty.warning(this, getString(R.string.error_meal_times), Toast.LENGTH_LONG).show();
            return false;

        } else if (null == mItemFamily.cookingFuel || mItemFamily.cookingFuel.equals("")) {
            Toasty.warning(this, getString(R.string.error_cooking_fuel), Toast.LENGTH_LONG).show();
            return false;

        } else if (null == mItemFamily.treatmentPlace) {
            Toasty.warning(this, getString(R.string.error_treatment_place), Toast.LENGTH_LONG).show();
            return false;

        } else if (null == mItemFamily.recentDeath) {
            Toasty.warning(this, getString(R.string.error_death), Toast.LENGTH_LONG).show();
            return false;

        } else if (mItemFamily.recentDeath.equals(getString(R.string.text_yes))) {
            // Check death info list
            if (null == mItemFamily.deathInfoList) {
                Toasty.warning(this, getString(R.string.error_fill_death_info), Toast.LENGTH_LONG).show();
                return false;
            }

            for (ItemDeathInfo itemDeathInfo : mItemFamily.deathInfoList) {
                if (null == itemDeathInfo.name || itemDeathInfo.name.equals("")
                        || 1 > itemDeathInfo.age
                        || null == itemDeathInfo.relation || itemDeathInfo.relation.equals("")
                        || null == itemDeathInfo.deathCause || itemDeathInfo.deathCause.equals("")) {

                    Toasty.warning(this, getString(R.string.error_fill_death_info), Toast.LENGTH_LONG).show();
                    return false;
                }
            }
            return true;

        } else {
            return true;
        }
    }

//    private void setHouseholdIdAdapter() {
//        final AdapterDropDown<String> spHouseholdAdapter = new AdapterDropDown<>(
//                this,
//                R.layout.spinner_layout,
//                new ArrayList<>(Arrays.asList(mHouseNumbers)),
//                SPINNER_HOUSEHOLD_ID
//        );
//        spHouseholdAdapter.setDropDownViewResource(R.layout.spinner_layout);
//        mEtHouseNo.setAdapter(spHouseholdAdapter);
//    }

    private void setRationTypeAdapter() {
        final AdapterDropDown<String> adapter = new AdapterDropDown<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(mRationCardTypes)),
                SPINNER_RATION_TYPE
        );
        adapter.setDropDownViewResource(R.layout.spinner_layout);
        mSpRationType.setAdapter(adapter);
    }

    private void setInsuranceTypeAdapter() {
        final AdapterDropDown<String> spInsuranceAdapter = new AdapterDropDown<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(mInsuranceTypes)),
                SPINNER_INSURANCE
        );
        spInsuranceAdapter.setDropDownViewResource(R.layout.spinner_layout);
        mSpInsuranceType.setAdapter(spInsuranceAdapter);
    }

    private void setTreatmentPlaceAdapter() {
        final AdapterDropDown<String> spTreatmentAdapter = new AdapterDropDown<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(mTreatmentPlaces)),
                SPINNER_TREATMENT
        );

        spTreatmentAdapter.setDropDownViewResource(R.layout.spinner_layout);
        mSpTreatmentPlace.setAdapter(spTreatmentAdapter);
    }

    private void showRecentDeathInfo() {
        // Show button
        mBtnAddMoreDeathInfo.setVisibility(View.VISIBLE);
        mAdapterDeathInfo.setItems(new ArrayList<ItemDeathInfo>());
        mAdapterDeathInfo.addNewItem(true);
    }

    private void hideDeathInfo() {
        // Hide button
        mBtnAddMoreDeathInfo.setVisibility(View.GONE);
        mAdapterDeathInfo.setItems(null);
    }
}
