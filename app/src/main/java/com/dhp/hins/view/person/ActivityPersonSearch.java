package com.dhp.hins.view.person;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.dhp.hins.R;
import com.dhp.hins.adapter.AdapterPerson;
import com.dhp.hins.utils.LUtils;

/**
 * Created by Prabhat on 05-Aug-17.
 */

public class ActivityPersonSearch extends AppCompatActivity {

    private RecyclerView mRecyclerView;

    private InputMethodManager mInputMethodManager;
    private EditText           mEditText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_person_search);

        LUtils.initToolbar(this, true, getString(R.string.title_family));

        mInputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_family_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        final AdapterPerson adapterPerson = new AdapterPerson(this);
        mRecyclerView.setAdapter(adapterPerson);

        if (0 == adapterPerson.getItemCount()) {
            findViewById(R.id.tv_empty).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.tv_empty).setVisibility(View.GONE);
        }

        mEditText = (EditText) findViewById(R.id.tv_search);

        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapterPerson.onSearchTextChanged(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        mInputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mInputMethodManager.hideSoftInputFromWindow(mEditText.getWindowToken(), 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_household_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mInputMethodManager.hideSoftInputFromWindow(mEditText.getWindowToken(), 0);

        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
