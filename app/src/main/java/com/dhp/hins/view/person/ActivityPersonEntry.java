package com.dhp.hins.view.person;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.hins.R;
import com.dhp.hins.adapter.AdapterDropDown;
import com.dhp.hins.item.ItemDisease;
import com.dhp.hins.item.ItemPerson;
import com.dhp.hins.storage.LDatabase;
import com.dhp.hins.utils.DialogSearch;
import com.dhp.hins.utils.DialogSearchCallbacks;
import com.dhp.hins.utils.LColors;
import com.dhp.hins.utils.LUtils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import es.dmoral.toasty.Toasty;

import static android.view.MotionEvent.ACTION_UP;
import static com.dhp.hins.utils.LConstants.DHP_HB_APP;
import static com.dhp.hins.utils.LConstants.KEY_LAUNCH_TYPE;
import static com.dhp.hins.utils.LConstants.KEY_PERSON_ID;
import static com.dhp.hins.utils.LConstants.KEY_RECENT_FAMILY_ID;
import static com.dhp.hins.utils.LConstants.LAUNCH_TYPE_ADD;
import static com.dhp.hins.utils.LConstants.LAUNCH_TYPE_EDIT;
import static com.dhp.hins.utils.LConstants.MAX_BP_DIASTOLIC;
import static com.dhp.hins.utils.LConstants.MAX_BP_SYSTOLIC;
import static com.dhp.hins.utils.LConstants.MAX_HB;
import static com.dhp.hins.utils.LConstants.MAX_HEIGHT;
import static com.dhp.hins.utils.LConstants.MAX_RBS;
import static com.dhp.hins.utils.LConstants.MAX_WEIGHT;
import static com.dhp.hins.utils.LConstants.MIN_AGE_FOR_HB;
import static com.dhp.hins.utils.LConstants.MIN_AGE_FOR_MEASUREMENTS;
import static com.dhp.hins.utils.LConstants.MIN_AGE_FOR_QUESTIONNAIRE;
import static com.dhp.hins.utils.LConstants.MIN_BP_DIASTOLIC;
import static com.dhp.hins.utils.LConstants.MIN_BP_SYSTOLIC;
import static com.dhp.hins.utils.LConstants.MIN_HB;
import static com.dhp.hins.utils.LConstants.MIN_HEIGHT;
import static com.dhp.hins.utils.LConstants.MIN_RBS;
import static com.dhp.hins.utils.LConstants.MIN_WEIGHT;
import static com.dhp.hins.utils.LConstants.SPINNER_AGE_TIME;
import static com.dhp.hins.utils.LConstants.SPINNER_FAMILY_ID;
import static com.dhp.hins.utils.LConstants.SPINNER_HEIGHT_UNIT;
import static com.dhp.hins.utils.LConstants.SPINNER_IDENTITY;
import static com.dhp.hins.utils.LConstants.SPINNER_LITERACY;
import static com.dhp.hins.utils.LConstants.SPINNER_MARITAL;
import static com.dhp.hins.utils.LConstants.SPINNER_OCCUPATION;
import static com.dhp.hins.utils.LConstants.SPINNER_RESIDENT;
import static com.dhp.hins.utils.LUtils.fromHtml;
import static com.dhp.hins.utils.LUtils.getDifferenceInDays;
import static com.dhp.hins.utils.LUtils.getFormattedDate;
import static com.dhp.hins.utils.LUtils.getItemWithOthersIndex;
import static com.dhp.hins.utils.LUtils.getRadioIndex;
import static com.dhp.hins.utils.LUtils.getSpinnerIndex;
import static com.dhp.hins.utils.LUtils.getYesNoIndex;
import static com.dhp.hins.utils.LUtils.hasIncome;
import static com.dhp.hins.utils.LUtils.isFutureDate;
import static com.dhp.hins.utils.LUtils.isRegular;

/**
 * Created by Prabhat on 14-Apr-17.
 */

public class ActivityPersonEntry extends AppCompatActivity {

    private int mLaunchType;

    private boolean mSexAutoCheck;
    private boolean mAgeAutoCheck;

    private static final String TAG = ActivityPersonEntry.class.getSimpleName();

    private String mAgeTime;
    private String mIdentityTypeOther;

    private String[] mFamilyIds;
    private String[] mRelationTypes;
    private String[] mResidentTypeList;
    private String[] mEducationList;
    private String[] mAgeTimeSpans;
    private String[] mMaritalList;
    private String[] mLiteracyList;
    private String[] mOccupationList;
    private String[] mOccupationTypeList;
    private String[] mIncomeTypeList;
    private String[] mIdentityList;
    private String[] mTreatmentStatusList;
    private String[] mHeightUnitList;

    private EditText mEtPersonName;
    private EditText mEtPersonAge;
    private EditText mEtBmi;
    private EditText mEtDob;
    private EditText mEtIncome;
    private EditText mEtIdentityTypeOther;
    private EditText mEtIdentityNo;
    private EditText mEtEducationOther;
    private EditText mEtOccupationOther;

    private EditText mEtDiseaseOther;
    private EditText mEtDisabilityOther;

    private EditText mEtHeight;
    private EditText mEtWeight;
    private EditText mEtBpSystolic;
    private EditText mEtBpDiastolic;
    private EditText mEtRbs;
    private EditText mEtHb;
    private EditText mEtAilment;

    private RadioGroup mRgPersonSex;
    private RadioGroup mRgOccupationRegular;
    private RadioGroup mRgSchool;
    private RadioGroup mRgSmoking;
    private RadioGroup mRgTobacco;
    private RadioGroup mRgDrinking;
    private RadioGroup mRgExercise;
    private RadioGroup mRgRbsAllow;
    private RadioGroup mRgHbAllow;
    private RadioGroup mRgSick;

    private EditText etFamilyId;
    private Spinner  mSpAgeTime;
    private Spinner  mSpRelation;
    private Spinner  mSpResident;
    private Spinner  mSpEducation;
    private Spinner  mSpMarital;
    private Spinner  mSpLiteracy;
    private Spinner  mSpOccupation;
    private Spinner  mSpIdentityType;
    private Spinner  mSpIncomeType;

    private CheckBox mCbDiabetes;
    private CheckBox mCbHyperTension;
    private CheckBox mCbKidney;
    private CheckBox mCbHeart;
    private CheckBox mCbEyeProblem;
    private CheckBox mCbMenstrual;
    private CheckBox mCbDental;
    private CheckBox mCbDiseaseOther;

    private Spinner mSpDiabetes;
    private Spinner mSpHyperTension;
    private Spinner mSpKidney;
    private Spinner mSpHeart;
    private Spinner mSpEyeProblem;
    private Spinner mSpMenstrual;
    private Spinner mSpDental;
    private Spinner mSpOther;

    private CheckBox cbUrdu;
    private CheckBox cbEnglish;
    private CheckBox cbTamil;
    private CheckBox cbKannada;
    private CheckBox cbLangOther;
    private EditText etLangOther;

    private CheckBox mCbLocomotor;
    private CheckBox mCbMentalRetardation;
    private CheckBox mCbMentalIllness;
    private CheckBox mCbHearing;
    private CheckBox mCbLeprosy;
    private CheckBox mCbBlindness;
    private CheckBox mCbLowVision;
    private CheckBox mCbDisabilityOther;

    private Spinner mSpHeightUnit;

    private ImageView    mIvShowHideMeasurements;
    private TextView     mTvSchoolTitle;
    private TextView     mTvHabitTitle;
    private TextView     mTvExerciseTitle;
    private LinearLayout mLlMeasurements;

    private LDatabase  mDb;
    private ItemPerson mItemPerson;
    private String     mDisabilityOther;

    private boolean mCbDiabetesAutoCheck;
    private boolean mCbHypertensionAutoCheck;
    private boolean mCbKidneyAutoCheck;
    private boolean mCbHeartAutoCheck;
    private boolean mCbEyeProblemAutoCheck;
    private boolean mCbMenstrualAutoCheck;
    private boolean mCbDentalAutoCheck;
    private boolean mCbOtherAutoCheck;

    private EditText mEtBmiUnit;
    private EditText mEtBpUnit;
    private EditText mEtRbsUnit;
    private EditText mEtHbUnit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_person_entry);

        LUtils.initToolbar(this, true, getString(R.string.title_person));

        mLaunchType = getIntent().getIntExtra(KEY_LAUNCH_TYPE, LAUNCH_TYPE_ADD);

        mDb = LDatabase.getInstance(this);

        initValues();

        if (null != savedInstanceState) {
            // TODO: 27-Jul-17
//            restoreValues(savedInstanceState);
        }

        initUi();

        if (LAUNCH_TYPE_EDIT == mLaunchType) {
            populateUi();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setMessage(getString(R.string.msg_go_back));
        builder.setPositiveButton(getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityPersonEntry.super.onBackPressed();
            }
        });

        builder.setNegativeButton(getString(R.string.btn_no), null);
        builder.show();
    }

    private void initValues() {
        mFamilyIds = mDb.getFamilyIds();
        mResidentTypeList = mDb.getResidentTypeList();
        mEducationList = mDb.getEducationList();
        mLiteracyList = mDb.getLiteracyList();
        mOccupationList = mDb.getOccupationList();
        mOccupationTypeList = mDb.getOccupationTypeList();
        mIncomeTypeList = mDb.getIncomeTypeList();
        mIdentityList = mDb.getIdentityList();
        mTreatmentStatusList = mDb.getTreatmentStatusList();
        mHeightUnitList = mDb.getHeightUnitList();

        if (LAUNCH_TYPE_EDIT == mLaunchType) {
            long personIdKey = getIntent().getLongExtra(KEY_PERSON_ID, 0);
            mItemPerson = mDb.getPersonDetails(personIdKey);
        } else {
            mItemPerson = new ItemPerson();
            mItemPerson.familyId = LUtils.getStringPref(KEY_RECENT_FAMILY_ID, "");
            mItemPerson.diseaseList = new ArrayList<>(4);
        }
    }

    private void initUi() {
        etFamilyId = (EditText) findViewById(R.id.et_family_id);
//        etFamilyId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (position > 0) {
//                    mItemPerson.familyId = mFamilyIds[position];
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });
//        setFamilyIdAdapter();

        etFamilyId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DialogSearch(ActivityPersonEntry.this, new DialogSearchCallbacks() {
                    @Override
                    public void onSelected(String familyId) {
                        mItemPerson.familyId = familyId;
                        etFamilyId.setText(familyId);
                    }
                }).withItems(mFamilyIds).showDialog();

                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            }
        });

        mEtPersonName = (EditText) findViewById(R.id.et_person_name);

        mEtDob = (EditText) findViewById(R.id.et_person_dob);
        mEtDob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (ACTION_UP == motionEvent.getAction()) {

                    final Calendar calendar = Calendar.getInstance();

                    DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                    onDobSet(year, monthOfYear, dayOfMonth);
                                }
                            },

                            calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DATE));

                    datePickerDialog.show(getFragmentManager(), "DatePickerDialog");
                }

                return false;
            }
        });
        findViewById(R.id.iv_clear_dob).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemPerson.dob = "";
                mEtDob.setText("");

                mEtPersonAge.setEnabled(true);
                mSpAgeTime.setEnabled(true);
            }
        });

        mEtPersonAge = (EditText) findViewById(R.id.et_person_age);
        mEtPersonAge.addTextChangedListener(new AgeTextWatcher());

        mSpAgeTime = (Spinner) findViewById(R.id.sp_age_time);
        mSpAgeTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    mAgeTime = mAgeTimeSpans[position];

                    if (!mAgeAutoCheck) {
                        onAgeSet();
                    } else {
                        mAgeAutoCheck = false;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        setAgeTimeAdapter();
        mSpAgeTime.setSelection(1);

        mRgPersonSex = (RadioGroup) findViewById(R.id.rg_person_sex);
        mRgPersonSex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                onSexSelected(indexId);
            }
        });

        mSpRelation = (Spinner) findViewById(R.id.sp_relation_to_hh);
        mSpRelation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (ACTION_UP == event.getAction()) {
                    new DialogSearch(ActivityPersonEntry.this, new DialogSearchCallbacks() {
                        @Override
                        public void onSelected(String relation) {
                            final ArrayAdapter<String> adapter
                                    = new ArrayAdapter<>(ActivityPersonEntry.this,
                                    R.layout.spinner_layout, new String[]{relation});
                            mSpRelation.setAdapter(adapter);
                            mItemPerson.relationToHoh = relation;

                            if (mItemPerson.relationToHoh.equals("Self")) {
                                hideDontKnowQues();
                            } else {
                                showDontKnowQues();
                            }
                        }
                    }).withItems(mDb.getRelationTypes(getSexIndex(mItemPerson.sex))).showDialog();

                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                    mSpRelation.requestFocus();
                }
                return true;
            }
        });

        setRelationAdapter();
        mSpRelation.setEnabled(false);

        mSpResident = (Spinner) findViewById(R.id.sp_resident_status);
        mSpResident.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    mItemPerson.resident = mResidentTypeList[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        setResidentAdapter();

        mSpEducation = (Spinner) findViewById(R.id.sp_education);
        mSpEducation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position < 1) {
                    return;
                }

                mItemPerson.education = mEducationList[position];

                if (mItemPerson.education.equals("Other")) {
                    mEtEducationOther.setVisibility(View.VISIBLE);
                } else {
                    mEtEducationOther.setText("");
                    mEtEducationOther.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        setEducationAdapter();

        mSpMarital = (Spinner) findViewById(R.id.sp_marital_status);
        mSpMarital.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    mItemPerson.marital = mMaritalList[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        mSpMarital.setAdapter(getMaritalAdapter(0));

        mSpLiteracy = (Spinner) findViewById(R.id.sp_literacy);
        mSpLiteracy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    mItemPerson.literacy = mLiteracyList[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        setLiteracyAdapter();

        mSpOccupation = (Spinner) findViewById(R.id.sp_occupation);
        mSpOccupation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                onOccupationSelected(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        setOccupationAdapter();

        mRgOccupationRegular = (RadioGroup) findViewById(R.id.rg_occupation_type);
        mRgOccupationRegular.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                mItemPerson.occupationType = ((RadioButton) findViewById(indexId)).getText().toString();
            }
        });

        mRgSick = (RadioGroup) findViewById(R.id.rg_sick);
        mRgSick.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                mItemPerson.sick = ((RadioButton) findViewById(indexId)).getText().toString();

                if (mItemPerson.sick.equals("Yes")) {
                    findViewById(R.id.tv_ailment_title).setVisibility(View.VISIBLE);
                    mEtAilment.setVisibility(View.VISIBLE);
                } else {
                    mItemPerson.ailment = "";
                    mEtAilment.setText("");
                    mEtAilment.setVisibility(View.GONE);
                    findViewById(R.id.tv_ailment_title).setVisibility(View.GONE);
                }
            }
        });

        mEtIncome = (EditText) findViewById(R.id.et_income);

        mSpIdentityType = (Spinner) findViewById(R.id.sp_identity_type);
        mSpIdentityType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                onIdentitySelected(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        setIdentityAdapter();

        mSpIncomeType = (Spinner) findViewById(R.id.sp_income_type);
        mSpIncomeType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (0 < position) {
                    mItemPerson.incomeType = mIncomeTypeList[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        setIncomeTypeAdapter();

        mEtIdentityTypeOther = (EditText) findViewById(R.id.et_identity_type_other);
        mEtIdentityTypeOther.setVisibility(View.GONE);
        mEtEducationOther = (EditText) findViewById(R.id.et_education_other);
        mEtEducationOther.setVisibility(View.GONE);
        mEtOccupationOther = (EditText) findViewById(R.id.et_occupation_other);
        mEtOccupationOther.setVisibility(View.GONE);

        mEtIdentityNo = (EditText) findViewById(R.id.et_identity_number);
        mEtAilment = (EditText) findViewById(R.id.et_ailment);

        mEtHeight = (EditText) findViewById(R.id.et_height);
        mEtHeight.addTextChangedListener(new BmiTextWatcher());

        mSpHeightUnit = (Spinner) findViewById(R.id.sp_height_unit);
        mSpHeightUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    mItemPerson.heightUnit = mHeightUnitList[position];
                    findBmi();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        setHeightUnitAdapter();

        mEtWeight = (EditText) findViewById(R.id.et_weight);
        mEtWeight.addTextChangedListener(new BmiTextWatcher());

        mEtBmi = (EditText) findViewById(R.id.et_bmi);

        mEtBpSystolic = (EditText) findViewById(R.id.et_bp_systolic);
        mEtBpSystolic.addTextChangedListener(new BpSystolicWatcher());
        mEtBpDiastolic = (EditText) findViewById(R.id.et_bp_diastolic);
        mEtBpDiastolic.addTextChangedListener(new BpDiastolicWatcher());

        mEtRbs = (EditText) findViewById(R.id.et_rbs);
        mEtRbs.addTextChangedListener(new RbsTextWatcher());
        mEtHb = (EditText) findViewById(R.id.et_hb);
        mEtHb.addTextChangedListener(new HbTextWatcher());

        findViewById(R.id.iv_read_hb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage(DHP_HB_APP);

                if (launchIntent != null) {
                    startActivity(launchIntent);
                }
            }
        });

        mLlMeasurements = (LinearLayout) findViewById(R.id.ll_measurements);
        mIvShowHideMeasurements = (ImageView) findViewById(R.id.iv_show_hide_measurements);
        mIvShowHideMeasurements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHideMeasurements();
            }
        });

        mTvSchoolTitle = (TextView) findViewById(R.id.tv_school_title);
        mRgSchool = (RadioGroup) findViewById(R.id.rg_school);
        mRgSchool.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }
                mItemPerson.school = ((RadioButton) findViewById(indexId)).getText().toString();
            }
        });


        mTvHabitTitle = (TextView) findViewById(R.id.tv_habit_title);
        mRgSmoking = (RadioGroup) findViewById(R.id.rg_smoking);
        mRgSmoking.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }
                mItemPerson.smoking = ((RadioButton) findViewById(indexId)).getText().toString();
            }
        });
        mRgTobacco = (RadioGroup) findViewById(R.id.rg_tobacco);
        mRgTobacco.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }
                mItemPerson.tobacco = ((RadioButton) findViewById(indexId)).getText().toString();
            }
        });
        mRgDrinking = (RadioGroup) findViewById(R.id.rg_drinking);
        mRgDrinking.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }
                mItemPerson.drinking = ((RadioButton) findViewById(indexId)).getText().toString();
            }
        });

        mTvExerciseTitle = (TextView) findViewById(R.id.tv_exercise_title);
        mRgExercise = (RadioGroup) findViewById(R.id.rg_exercise);
        mRgExercise.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }
                mItemPerson.exercise = ((RadioButton) findViewById(indexId)).getText().toString();
            }
        });

        mRgRbsAllow = (RadioGroup) findViewById(R.id.rg_rbs_allow);
        mRgRbsAllow.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                mItemPerson.rbsAllow = ((RadioButton) findViewById(checkedId)).getText().toString();

                if (mItemPerson.rbsAllow.equals("Yes")) {
                    mEtRbs.setEnabled(true);
                } else {
                    mItemPerson.rbs = null;
                    mEtRbs.setText("");
                    mEtRbs.setEnabled(false);
                }
            }
        });
        mEtRbs.setEnabled(false);

        mRgHbAllow = (RadioGroup) findViewById(R.id.rg_hb_allow);
        mRgHbAllow.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                mItemPerson.hbAllow = ((RadioButton) findViewById(checkedId)).getText().toString();
                if (mItemPerson.hbAllow.equals("Yes")) {
                    mEtHb.setEnabled(true);
                } else {
                    mItemPerson.hb = null;
                    mEtHb.setText("");
                    mEtHb.setEnabled(false);
                }
            }
        });
        mEtHb.setEnabled(false);

        findViewById(R.id.btn_save_person).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                savePersonDetails();
            }
        });

        findViewById(R.id.iv_help_resident).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHelpDialog(getString(R.string.help_resident));
            }
        });
        findViewById(R.id.iv_help_literacy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHelpDialog(getString(R.string.help_literacy));
            }
        });
        findViewById(R.id.iv_help_studied_upto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHelpDialog(getString(R.string.help_studied_upto));
            }
        });
        findViewById(R.id.iv_help_occupation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHelpDialog(getString(R.string.help_occupation));
            }
        });

        mCbDiabetes = (CheckBox) findViewById(R.id.cb_diabetes);
        mCbDiabetes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCbDiabetesAutoCheck) {
                    mCbDiabetesAutoCheck = false;
                    return;
                }

                if (isChecked) {
                    addDisease(getString(R.string.cb_diabetes));
                    mSpDiabetes.setEnabled(true);
                } else {
                    removeDisease(getString(R.string.cb_diabetes));
                    mSpDiabetes.setEnabled(false);
                    setTreatmentSpinner(mSpDiabetes);
                }
            }
        });
        mCbHyperTension = (CheckBox) findViewById(R.id.cb_hypertension);
        mCbHyperTension.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCbHypertensionAutoCheck) {
                    mCbHypertensionAutoCheck = false;
                    return;
                }

                if (isChecked) {
                    addDisease(getString(R.string.cb_hypertension));
                    mSpHyperTension.setEnabled(true);
                } else {
                    removeDisease(getString(R.string.cb_hypertension));
                    mSpHyperTension.setEnabled(false);
                    setTreatmentSpinner(mSpHyperTension);
                }
            }
        });
        mCbKidney = (CheckBox) findViewById(R.id.cb_kidney_disease);
        mCbKidney.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCbKidneyAutoCheck) {
                    mCbKidneyAutoCheck = false;
                    return;
                }

                if (isChecked) {
                    addDisease(getString(R.string.cb_kidney_disease));
                    mSpKidney.setEnabled(true);
                } else {
                    removeDisease(getString(R.string.cb_kidney_disease));
                    mSpKidney.setEnabled(false);
                    setTreatmentSpinner(mSpKidney);
                }
            }
        });
        mCbHeart = (CheckBox) findViewById(R.id.cb_heart_problem);
        mCbHeart.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCbHeartAutoCheck) {
                    mCbHeartAutoCheck = false;
                    return;
                }

                if (isChecked) {
                    addDisease(getString(R.string.cb_heart_problem));
                    mSpHeart.setEnabled(true);
                } else {
                    removeDisease(getString(R.string.cb_heart_problem));
                    mSpHeart.setEnabled(false);
                    setTreatmentSpinner(mSpHeart);
                }
            }
        });
        mCbEyeProblem = (CheckBox) findViewById(R.id.cb_eye_problem);
        mCbEyeProblem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCbEyeProblemAutoCheck) {
                    mCbEyeProblemAutoCheck = false;
                    return;
                }

                if (isChecked) {
                    addDisease(getString(R.string.cb_eye_problems));
                    mSpEyeProblem.setEnabled(true);
                } else {
                    removeDisease(getString(R.string.cb_eye_problems));
                    mSpEyeProblem.setEnabled(false);
                    setTreatmentSpinner(mSpEyeProblem);
                }
            }
        });
        mCbMenstrual = (CheckBox) findViewById(R.id.cb_menstrual);
        mCbMenstrual.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCbMenstrualAutoCheck) {
                    mCbMenstrualAutoCheck = false;
                    return;
                }

                if (isChecked) {
                    addDisease(getString(R.string.cb_menstrual_problems));
                    mSpMenstrual.setEnabled(true);
                } else {
                    removeDisease(getString(R.string.cb_menstrual_problems));
                    mSpMenstrual.setEnabled(false);
                    setTreatmentSpinner(mSpMenstrual);
                }
            }
        });
        mCbDental = (CheckBox) findViewById(R.id.cb_dental);
        mCbDental.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCbDentalAutoCheck) {
                    mCbDentalAutoCheck = false;
                    return;
                }

                if (isChecked) {
                    addDisease(getString(R.string.cb_dental_problems));
                    mSpDental.setEnabled(true);
                } else {
                    removeDisease(getString(R.string.cb_dental_problems));
                    mSpDental.setEnabled(false);
                    setTreatmentSpinner(mSpDental);
                }
            }
        });
        mCbDiseaseOther = (CheckBox) findViewById(R.id.cb_disease_other);
        mCbDiseaseOther.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCbOtherAutoCheck) {
                    mCbOtherAutoCheck = false;
                    return;
                }

                if (isChecked) {
                    addDisease(getString(R.string.cb_other));
                    mEtDiseaseOther.setVisibility(View.VISIBLE);
                    mSpOther.setEnabled(true);
                } else {
                    mEtDiseaseOther.setText("");
                    mEtDiseaseOther.setVisibility(View.GONE);
                    mSpOther.setEnabled(false);
                    setTreatmentSpinner(mSpOther);

                    removeDiseaseOther();
                }
            }
        });

        mEtDiseaseOther = (EditText) findViewById(R.id.et_disease_other);
        mEtDisabilityOther = (EditText) findViewById(R.id.et_disability_other);

        mSpDiabetes = (Spinner) findViewById(R.id.sp_diabetes);
        mSpDiabetes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    updateDisease(getString(R.string.cb_diabetes), position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSpHyperTension = (Spinner) findViewById(R.id.sp_hypertension);
        mSpHyperTension.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    updateDisease(getString(R.string.cb_hypertension), position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSpKidney = (Spinner) findViewById(R.id.sp_kidney);
        mSpKidney.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    updateDisease(getString(R.string.cb_kidney_disease), position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSpHeart = (Spinner) findViewById(R.id.sp_heart);
        mSpHeart.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    updateDisease(getString(R.string.cb_heart_problem), position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSpEyeProblem = (Spinner) findViewById(R.id.sp_eye_problem);
        mSpEyeProblem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    updateDisease(getString(R.string.cb_eye_problems), position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSpMenstrual = (Spinner) findViewById(R.id.sp_menstrual);
        mSpMenstrual.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    updateDisease(getString(R.string.cb_menstrual_problems), position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSpDental = (Spinner) findViewById(R.id.sp_dental);
        mSpDental.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    updateDisease(getString(R.string.cb_dental_problems), position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSpOther = (Spinner) findViewById(R.id.sp_other);
        mSpOther.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    updateDiseaseOther(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        setAnswerSpinner();

        mSpDiabetes.setEnabled(false);
        mSpHyperTension.setEnabled(false);
        mSpKidney.setEnabled(false);
        mSpHeart.setEnabled(false);
        mSpEyeProblem.setEnabled(false);
        mSpMenstrual.setEnabled(false);
        mSpDental.setEnabled(false);
        mSpOther.setEnabled(false);

        cbLangOther = (CheckBox) findViewById(R.id.cb_lang_other);
        cbLangOther.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) {
                    etLangOther.setVisibility(View.VISIBLE);
                } else {
                    etLangOther.setText("");
                    etLangOther.setVisibility(View.GONE);
                }
            }
        });
        etLangOther = (EditText) findViewById(R.id.et_lang_other);

        cbUrdu = (CheckBox) findViewById(R.id.cb_urdu);
        cbEnglish = (CheckBox) findViewById(R.id.cb_english);
        cbTamil = (CheckBox) findViewById(R.id.cb_tamil);
        cbKannada = (CheckBox) findViewById(R.id.cb_kannada);
        cbLangOther = (CheckBox) findViewById(R.id.cb_lang_other);
        etLangOther = (EditText) findViewById(R.id.et_lang_other);

        mCbLocomotor = (CheckBox) findViewById(R.id.cb_locomotor);
        mCbMentalRetardation = (CheckBox) findViewById(R.id.cb_mental_retardation);
        mCbMentalIllness = (CheckBox) findViewById(R.id.cb_mental_illness);
        mCbHearing = (CheckBox) findViewById(R.id.cb_hearing);
        mCbLeprosy = (CheckBox) findViewById(R.id.cb_leprosy);
        mCbBlindness = (CheckBox) findViewById(R.id.cb_blindness);
        mCbLowVision = (CheckBox) findViewById(R.id.cb_low_vision);
        mCbDisabilityOther = (CheckBox) findViewById(R.id.cb_disability_other);
        mCbDisabilityOther.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mEtDisabilityOther.setVisibility(View.VISIBLE);
                } else {
                    mEtDisabilityOther.setText("");
                    mEtDisabilityOther.setVisibility(View.GONE);
                }
            }
        });

        mEtBmiUnit = (EditText) findViewById(R.id.et_bmi_unit);
        mEtBpUnit = (EditText) findViewById(R.id.et_bp_unit);
        mEtRbsUnit = (EditText) findViewById(R.id.et_rbs_unit);
        mEtHbUnit = (EditText) findViewById(R.id.et_hb_unit);
    }


    private void hideDontKnowQues() {
        mRgSmoking.getChildAt(2).setVisibility(View.GONE);
        mRgTobacco.getChildAt(2).setVisibility(View.GONE);
        mRgDrinking.getChildAt(2).setVisibility(View.GONE);
        mRgExercise.getChildAt(2).setVisibility(View.GONE);

        clearAnswers();
        setAnswerSpinner();
    }

    private void showDontKnowQues() {
        mRgSmoking.getChildAt(2).setVisibility(View.VISIBLE);
        mRgTobacco.getChildAt(2).setVisibility(View.VISIBLE);
        mRgDrinking.getChildAt(2).setVisibility(View.VISIBLE);
        mRgExercise.getChildAt(2).setVisibility(View.VISIBLE);

        clearAnswers();
        setAnswerSpinner();
    }

    private void clearAnswers() {
        mRgSchool.clearCheck();
        mRgSmoking.clearCheck();
        mRgTobacco.clearCheck();
        mRgDrinking.clearCheck();
        mRgExercise.clearCheck();

        mItemPerson.school = null;
        mItemPerson.smoking = null;
        mItemPerson.tobacco = null;
        mItemPerson.drinking = null;
        mItemPerson.exercise = null;

        if (LAUNCH_TYPE_EDIT != mLaunchType) {
            ((RadioButton) mRgSmoking.getChildAt(1)).setChecked(true);
            ((RadioButton) mRgTobacco.getChildAt(1)).setChecked(true);
            ((RadioButton) mRgDrinking.getChildAt(1)).setChecked(true);
            ((RadioButton) mRgExercise.getChildAt(1)).setChecked(true);
        }
    }

    private void setAnswerSpinner() {
        setTreatmentSpinner(mSpDiabetes);
        setTreatmentSpinner(mSpHyperTension);
        setTreatmentSpinner(mSpKidney);
        setTreatmentSpinner(mSpHeart);
        setTreatmentSpinner(mSpEyeProblem);
        setTreatmentSpinner(mSpMenstrual);
        setTreatmentSpinner(mSpDental);
        setTreatmentSpinner(mSpOther);
    }

    private void addDisease(String item) {
        mItemPerson.diseaseList.add(new ItemDisease(item, null));
    }

    private void removeDisease(String disease) {
        int i;

        if (null == mItemPerson.diseaseList) {
            return;
        }

        for (i = 0; i < mItemPerson.diseaseList.size(); i++) {
            ItemDisease itemDisease = mItemPerson.diseaseList.get(i);
            if (itemDisease.disease.equals(disease)) {
                mItemPerson.diseaseList.remove(i);
                break;
            }
        }
    }

    private void removeDiseaseOther() {
        mItemPerson.diseaseList.remove(getOtherDiseasePosition());
    }

    private void updateDisease(String disease, int position) {
        int i;

        for (i = 0; i < mItemPerson.diseaseList.size(); i++) {
            ItemDisease itemDisease = mItemPerson.diseaseList.get(i);
            if (itemDisease.disease.equals(disease)) {
                mItemPerson.diseaseList.get(i).treatment = mTreatmentStatusList[position];
                break;
            }
        }
    }

    private void updateDiseaseOther(int position) {
        int index = getOtherDiseasePosition();
        if (-1 != index) {
            mItemPerson.diseaseList.get(index).treatment = mTreatmentStatusList[position];
        }
    }

    private int getOtherDiseasePosition() {
        int position = -1;

        for (int i = 0; i < mItemPerson.diseaseList.size(); ++i) {
            String disease = mItemPerson.diseaseList.get(i).disease;

            if (!disease.equals(getString(R.string.cb_diabetes))
                    && !disease.equals(getString(R.string.cb_hypertension))
                    && !disease.equals(getString(R.string.cb_kidney_disease))
                    && !disease.equals(getString(R.string.cb_heart_problem))
                    && !disease.equals(getString(R.string.cb_eye_problems))
                    && !disease.equals(getString(R.string.cb_menstrual_problems))
                    && !disease.equals(getString(R.string.cb_dental_problems))) {
                position = i;
                break;
            }
        }

        return position;
    }

    private int getDisabilityPosition(String disability) {
        for (int i = 0; i < mItemPerson.disabilityList.size(); i++) {
            String itemDisability = mItemPerson.disabilityList.get(i);
            if (itemDisability.equals(disability)) {
                return i;
            }
        }

        return -1;
    }

    private int getOtherDisabilityPosition() {
        int position = -1;

        for (int i = 0; i < mItemPerson.disabilityList.size(); ++i) {
            String disability = mItemPerson.disabilityList.get(i);

            if (!disability.equals(getString(R.string.cb_locomotor))
                    && !disability.equals(getString(R.string.cb_mental_retardation))
                    && !disability.equals(getString(R.string.cb_mental_illness))
                    && !disability.equals(getString(R.string.cb_hearing))
                    && !disability.equals(getString(R.string.cb_leprosy))
                    && !disability.equals(getString(R.string.cb_blindness))
                    && !disability.equals(getString(R.string.cb_low_vision))) {
                position = i;
                break;
            }
        }

        return position;
    }

    private int getLanguagePosition(String language) {

        if (null == mItemPerson.languages) {
            return -1;
        }

        for (int i = 0; i < mItemPerson.languages.length; i++) {
            String itemLanguage = mItemPerson.languages[i];
            if (itemLanguage.equals(language)) {
                return i;
            }
        }

        return -1;
    }

    private int getOtherLanguagePosition() {
        int position = -1;

        if (null == mItemPerson.languages) {
            return -1;
        }

        for (int i = 0; i < mItemPerson.languages.length; ++i) {
            String itemLanguage = mItemPerson.languages[i];

            if (!itemLanguage.equals(getString(R.string.cb_urdu))
                    && !itemLanguage.equals(getString(R.string.cb_english))
                    && !itemLanguage.equals(getString(R.string.cb_tamil))
                    && !itemLanguage.equals(getString(R.string.cb_kannada))) {
                position = i;
                break;
            }
        }

        return position;
    }

    private void setTreatmentSpinner(Spinner spinner) {
        final AdapterDropDown<String> adapter = new AdapterDropDown<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.yes_no_dont_know))),
                SPINNER_FAMILY_ID
        );

        spinner.setAdapter(adapter);
    }

    private void setRelationAdapter() {
        final ArrayAdapter<String> adapter
                = new ArrayAdapter<>(this, R.layout.spinner_layout, new String[]{getString(R.string.text_select)});
        mSpRelation.setAdapter(adapter);
    }

    private void populateUi() {
        etFamilyId.setText(mItemPerson.familyId);
        etFamilyId.setEnabled(false);

        mEtPersonName.setText(mItemPerson.name);
        mEtPersonName.setSelection(mEtPersonName.getText().length());
//        mEtPersonName.requestFocus();

        mSpMarital.setAdapter(getMaritalAdapter(getSexIndex((mItemPerson.sex))));

        mEtDob.setText(mItemPerson.dob);
        mEtDob.setSelection(mEtDob.getText().length());

        mAgeAutoCheck = true;
        if (0 < mItemPerson.ageYear) {
            mEtPersonAge.setText("" + mItemPerson.ageYear);
            mSpAgeTime.setSelection(1);

        } else if (0 < mItemPerson.ageMonth) {
            mEtPersonAge.setText("" + mItemPerson.ageMonth);
            mSpAgeTime.setSelection(2);

        } else if (0 < mItemPerson.ageDay) {
            mEtPersonAge.setText("" + mItemPerson.ageDay);
            mSpAgeTime.setSelection(3);
        }

        if (3 <= mItemPerson.ageYear && 6 >= mItemPerson.ageYear) {
            // Anganawadi
            mTvSchoolTitle.setVisibility(View.VISIBLE);
            mTvSchoolTitle.setText(getString(R.string.ques_anganwadi));
            mRgSchool.setVisibility(View.VISIBLE);
        } else if (7 <= mItemPerson.ageYear && 16 >= mItemPerson.ageYear) {
            // School
            mTvSchoolTitle.setVisibility(View.VISIBLE);
            mTvSchoolTitle.setText(getString(R.string.ques_school));
            mRgSchool.setVisibility(View.VISIBLE);
        }

        if (10 < mItemPerson.ageYear) {
            // Show habits
            mTvHabitTitle.setVisibility(View.VISIBLE);
            findViewById(R.id.ll_habits).setVisibility(View.VISIBLE);
            // Show exercise
            mTvExerciseTitle.setVisibility(View.VISIBLE);
            mRgExercise.setVisibility(View.VISIBLE);
        }

        if (MIN_AGE_FOR_QUESTIONNAIRE < mItemPerson.ageYear) {
            showQuestions();
        }

        mEtPersonAge.setSelection(mEtPersonAge.getText().length());
        if (!mItemPerson.dob.equals("")) {
            mSpAgeTime.setEnabled(false);
            mEtPersonAge.setEnabled(false);
        }

        mSexAutoCheck = true;
        ((RadioButton) mRgPersonSex.getChildAt(getSexIndex(mItemPerson.sex))).setChecked(true);

        mSpRelation.setAdapter(setRelationAdapter(true));

        if (mItemPerson.relationToHoh.equals("Self")) {
            mRgSmoking.getChildAt(2).setVisibility(View.GONE);
            mRgTobacco.getChildAt(2).setVisibility(View.GONE);
            mRgDrinking.getChildAt(2).setVisibility(View.GONE);
            mRgExercise.getChildAt(2).setVisibility(View.GONE);
        }

        mSpResident.setSelection(getSpinnerIndex(mItemPerson.resident, mResidentTypeList));
        mSpMarital.setSelection(getSpinnerIndex(mItemPerson.marital, mMaritalList));
        mSpLiteracy.setSelection(getSpinnerIndex(mItemPerson.literacy, mLiteracyList));


        int occupationIndex = getItemWithOthersIndex(mItemPerson.occupation, mOccupationList);

        if (-1 != occupationIndex) {
            mSpOccupation.setSelection(occupationIndex);
        }

        if (occupationIndex == mOccupationList.length - 1) {
            mEtOccupationOther.setText(mItemPerson.occupation);
            mEtOccupationOther.setVisibility(View.VISIBLE);
        }

        if (isRegular(mItemPerson.occupation)) {
            findViewById(R.id.tv_occupation_type).setVisibility(View.VISIBLE);
            findViewById(R.id.rg_occupation_type).setVisibility(View.VISIBLE);

            int index = getRadioIndex(mItemPerson.occupationType, mOccupationTypeList);

            if (0 <= index) {
                ((RadioButton) mRgOccupationRegular.getChildAt(index)).setChecked(true);
            }
        }

        if (hasIncome(mItemPerson.occupation)) {
            mEtIncome.setText("" + mItemPerson.income);
            mSpIncomeType.setSelection(getSpinnerIndex(mItemPerson.incomeType, mIncomeTypeList));
            findViewById(R.id.tv_income_title).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_income_value).setVisibility(View.VISIBLE);
        }

//        int index = getRadioIndex(mItemPerson.occupationType, mOccupationTypeList);
//
//        if (-1 != index) {
//            ((RadioButton) mRgOccupationRegular.getChildAt(index)).setChecked(true);
//        }
//
//        if (0 < mItemPerson.income) {
//            mEtIncome.setText("" + mItemPerson.income);
//            mSpIncomeType.setSelection(getSpinnerIndex(mItemPerson.income.));
//        }

        int identityTypeIndex = getItemWithOthersIndex(mItemPerson.identityType, mIdentityList);

        if (-1 != identityTypeIndex) {
            mSpIdentityType.setSelection(identityTypeIndex);
            mSpIdentityType.setEnabled(true);
            mEtIdentityNo.setText(mItemPerson.identityNo);

            if (mItemPerson.identityType.equals("Aadhaar")) { // Adhaar
                InputFilter[] filterArray = new InputFilter[1];
                filterArray[0] = new InputFilter.LengthFilter(14);
                mEtIdentityNo.setFilters(filterArray);
            } else {
                InputFilter[] filterArray = new InputFilter[1];
                filterArray[0] = new InputFilter.LengthFilter(50);
                mEtIdentityNo.setFilters(filterArray);
            }
        }

        if (identityTypeIndex == mIdentityList.length - 1) {
            mEtIdentityTypeOther.setText(mItemPerson.identityType);
            mEtIdentityTypeOther.setVisibility(View.VISIBLE);
        }

        int educationIndex = getItemWithOthersIndex(mItemPerson.education, mEducationList);

        if (-1 != educationIndex) {
            mSpEducation.setSelection(educationIndex);
        }
        if (educationIndex == mEducationList.length - 1) {
            mEtEducationOther.setText(mItemPerson.education);
            mEtEducationOther.setVisibility(View.VISIBLE);
        }

        if (-1 != getYesNoIndex(mItemPerson.school)) {
            ((RadioButton) mRgSchool.getChildAt(getYesNoIndex(mItemPerson.school))).setChecked(true);
        }
        if (-1 != getYesNoIndex(mItemPerson.smoking)) {
            ((RadioButton) mRgSmoking.getChildAt(getYesNoIndex(mItemPerson.smoking))).setChecked(true);
        }
        if (-1 != getYesNoIndex(mItemPerson.tobacco)) {
            ((RadioButton) mRgTobacco.getChildAt(getYesNoIndex(mItemPerson.tobacco))).setChecked(true);
        }
        if (-1 != getYesNoIndex(mItemPerson.drinking)) {
            ((RadioButton) mRgDrinking.getChildAt(getYesNoIndex(mItemPerson.drinking))).setChecked(true);
        }
        if (-1 != getYesNoIndex(mItemPerson.exercise)) {
            ((RadioButton) mRgExercise.getChildAt(getYesNoIndex(mItemPerson.exercise))).setChecked(true);
        }

        if (null != mItemPerson.sick && !mItemPerson.sick.equals("")) {
            ((RadioButton) mRgSick.getChildAt(getYesNoIndex(mItemPerson.sick))).setChecked(true);
        }

        if (null != mItemPerson.ailment) {
            mEtAilment.setText(mItemPerson.ailment);
        }

        // TODO: 02-Aug-17, Populate
        int diseasePosition = getDiseasePosition(getString(R.string.cb_diabetes));
        if (diseasePosition >= 0) {
            mCbDiabetesAutoCheck = true;
            mCbDiabetes.setChecked(true);
            mSpDiabetes.setEnabled(true);
            mSpDiabetes.setSelection(getYesNoIndex(mItemPerson.diseaseList.get(diseasePosition).treatment) + 1);
        }

        diseasePosition = getDiseasePosition(getString(R.string.cb_hypertension));
        if (diseasePosition >= 0) {
            mCbHypertensionAutoCheck = true;
            mCbHyperTension.setChecked(true);
            mSpHyperTension.setEnabled(true);
            mSpHyperTension.setSelection(getYesNoIndex(mItemPerson.diseaseList.get(diseasePosition).treatment) + 1);
        }

        diseasePosition = getDiseasePosition(getString(R.string.cb_kidney_disease));
        if (diseasePosition >= 0) {
            mCbKidneyAutoCheck = true;
            mCbKidney.setChecked(true);
            mSpKidney.setEnabled(true);
            mSpKidney.setSelection(getYesNoIndex(mItemPerson.diseaseList.get(diseasePosition).treatment) + 1);
        }

        diseasePosition = getDiseasePosition(getString(R.string.cb_heart_problem));
        if (diseasePosition >= 0) {
            mCbHeartAutoCheck = true;
            mCbHeart.setChecked(true);
            mSpHeart.setEnabled(true);
            mSpHeart.setSelection(getYesNoIndex(mItemPerson.diseaseList.get(diseasePosition).treatment) + 1);
        }

        diseasePosition = getDiseasePosition(getString(R.string.cb_eye_problems));
        if (diseasePosition >= 0) {
            mCbEyeProblemAutoCheck = true;
            mCbEyeProblem.setChecked(true);
            mSpEyeProblem.setEnabled(true);
            mSpEyeProblem.setSelection(getYesNoIndex(mItemPerson.diseaseList.get(diseasePosition).treatment) + 1);
        }

        diseasePosition = getDiseasePosition(getString(R.string.cb_menstrual_problems));
        if (diseasePosition >= 0) {
            mCbMenstrualAutoCheck = true;
            mCbMenstrual.setChecked(true);
            mSpMenstrual.setEnabled(true);
            mSpMenstrual.setSelection(getYesNoIndex(mItemPerson.diseaseList.get(diseasePosition).treatment) + 1);
        }

        diseasePosition = getDiseasePosition(getString(R.string.cb_dental_problems));
        if (diseasePosition >= 0) {
            mCbDentalAutoCheck = true;
            mCbDental.setChecked(true);
            mSpDental.setEnabled(true);
            mSpDental.setSelection(getYesNoIndex(mItemPerson.diseaseList.get(diseasePosition).treatment) + 1);
        }

        diseasePosition = getOtherDiseasePosition();
        if (diseasePosition >= 0) {
            mCbOtherAutoCheck = true;
            mCbDiseaseOther.setChecked(true);
            mSpOther.setEnabled(true);
            mSpOther.setSelection(getYesNoIndex(mItemPerson.diseaseList.get(diseasePosition).treatment) + 1);
            mEtDiseaseOther.setVisibility(View.VISIBLE);
            mEtDiseaseOther.setText(mItemPerson.diseaseList.get(diseasePosition).disease);
        }

        int languagePosition = getLanguagePosition("Urdu");
        if (0 <= languagePosition) {
            cbUrdu.setChecked(true);
        }

        languagePosition = getLanguagePosition("Kannada");
        if (0 <= languagePosition) {
            cbKannada.setChecked(true);
        }

        languagePosition = getLanguagePosition("Tamil");
        if (0 <= languagePosition) {
            cbTamil.setChecked(true);
        }

        languagePosition = getLanguagePosition("English");
        if (0 <= languagePosition) {
            cbEnglish.setChecked(true);
        }

        languagePosition = getOtherLanguagePosition();
        if (0 <= languagePosition) {
            cbLangOther.setChecked(true);
            etLangOther.setVisibility(View.VISIBLE);
            etLangOther.setText(mItemPerson.languages[languagePosition]);
        }

        int disabilityPosition = getDisabilityPosition(getString(R.string.cb_locomotor));
        if (0 <= disabilityPosition) {
            mCbLocomotor.setChecked(true);
        }

        disabilityPosition = getDisabilityPosition(getString(R.string.cb_mental_retardation));
        if (0 <= disabilityPosition) {
            mCbMentalRetardation.setChecked(true);
        }

        disabilityPosition = getDisabilityPosition(getString(R.string.cb_mental_illness));
        if (0 <= disabilityPosition) {
            mCbMentalIllness.setChecked(true);
        }

        disabilityPosition = getDisabilityPosition(getString(R.string.cb_hearing));
        if (0 <= disabilityPosition) {
            mCbHearing.setChecked(true);
        }

        disabilityPosition = getDisabilityPosition(getString(R.string.cb_leprosy));
        if (0 <= disabilityPosition) {
            mCbLeprosy.setChecked(true);
        }

        disabilityPosition = getDisabilityPosition(getString(R.string.cb_blindness));
        if (0 <= disabilityPosition) {
            mCbBlindness.setChecked(true);
        }

        disabilityPosition = getDisabilityPosition(getString(R.string.cb_low_vision));
        if (0 <= disabilityPosition) {
            mCbLowVision.setChecked(true);
        }

        disabilityPosition = getOtherDisabilityPosition();
        if (0 <= disabilityPosition) {
            mCbDisabilityOther.setChecked(true);
            mEtDisabilityOther.setVisibility(View.VISIBLE);
            mEtDisabilityOther.setText(mItemPerson.disabilityList.get(disabilityPosition));
        }

        mEtHeight.setText(mItemPerson.height);
        mSpHeightUnit.setSelection(getSpinnerIndex(mItemPerson.heightUnit, mHeightUnitList));

        mEtWeight.setText(mItemPerson.weight);

        if (30 <= mItemPerson.ageYear) {
            mEtBpSystolic.setText(mItemPerson.bpSystolic);
            mEtBpDiastolic.setText(mItemPerson.bpDiastolic);
            mEtRbs.setText(mItemPerson.rbs);

            showMeasurements();
        }

        if (1 == getSexIndex(mItemPerson.sex) && 12 <= mItemPerson.ageYear) {
            mEtHb.setEnabled(true);
            mEtHb.setText(mItemPerson.hb);
            mEtHb.setVisibility(View.VISIBLE);

            showHb();
        }

        if (null != mItemPerson.rbsAllow) {
            ((RadioButton) mRgRbsAllow.getChildAt(getYesNoIndex(mItemPerson.rbsAllow))).setChecked(true);
        } else {
            mEtRbs.setEnabled(true);
        }

        if (null != mItemPerson.hbAllow) {
            ((RadioButton) mRgHbAllow.getChildAt(getYesNoIndex(mItemPerson.hbAllow))).setChecked(true);
        } else {
            mEtHb.setEnabled(true);
        }
    }

    private void showHb() {
        findViewById(R.id.tv_hb_allow_title).setVisibility(View.VISIBLE);
        findViewById(R.id.rg_hb_allow).setVisibility(View.VISIBLE);

        findViewById(R.id.tv_hb_title).setVisibility(View.VISIBLE);
        findViewById(R.id.ll_hb_value).setVisibility(View.VISIBLE);
    }

    private void hideHb() {
        findViewById(R.id.tv_hb_allow_title).setVisibility(View.GONE);
        findViewById(R.id.rg_hb_allow).setVisibility(View.GONE);

        findViewById(R.id.tv_hb_title).setVisibility(View.GONE);
        findViewById(R.id.ll_hb_value).setVisibility(View.GONE);

        mItemPerson.hb = null;
        mItemPerson.hbAllow = null;
    }

    private int getDiseasePosition(String disease) {

        for (int i = 0; i < mItemPerson.diseaseList.size(); i++) {
            ItemDisease itemDisease = mItemPerson.diseaseList.get(i);
            if (itemDisease.disease.equals(disease)) {
                return i;
            }
        }
        return -1;
    }

    private void showHelpDialog(String message) {
        final View dialogView = getLayoutInflater().inflate(R.layout.dialog_show_help, null);

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);
        builder.setView(dialogView);
        ((TextView) dialogView.findViewById(R.id.tv_help_info)).setText(fromHtml(message));
//        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.btn_ok), null);
        builder.show();
    }

    private void setHeightUnitAdapter() {
        final AdapterDropDown<String> mHeightUnitAdapter = new AdapterDropDown<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(mHeightUnitList)),
                SPINNER_HEIGHT_UNIT
        );

        mHeightUnitAdapter.setDropDownViewResource(R.layout.spinner_layout);
        mSpHeightUnit.setAdapter(mHeightUnitAdapter);
    }

    private void onIdentitySelected(int position) {
        if (position > 0) {
            mItemPerson.identityType = mIdentityList[position];

            mEtIdentityNo.setVisibility(View.VISIBLE);
            findViewById(R.id.tv_identity_number).setVisibility(View.VISIBLE);

            if (mItemPerson.identityType.equals(getString(R.string.text_other))) {
                mEtIdentityTypeOther.setVisibility(View.VISIBLE);
            } else {
                mEtIdentityTypeOther.setVisibility(View.GONE);
                mEtIdentityTypeOther.setText("");

                if (mItemPerson.identityType.equals(getString(R.string.none))) {
                    findViewById(R.id.tv_identity_number).setVisibility(View.GONE);
                    mEtIdentityNo.setVisibility(View.GONE);
                    mEtIdentityNo.setText("");
                }

                if (mItemPerson.identityType.equals("Aadhaar")) { // Adhaar
                    InputFilter[] filterArray = new InputFilter[1];
                    filterArray[0] = new InputFilter.LengthFilter(14);
                    mEtIdentityNo.setFilters(filterArray);
                } else {
                    InputFilter[] filterArray = new InputFilter[1];
                    filterArray[0] = new InputFilter.LengthFilter(50);
                    mEtIdentityNo.setFilters(filterArray);
                }
            }
        }
    }

    private void onOccupationSelected(int position) {
        if (position < 1) {
            return;
        }

        if (position < 1) {
            return;
        }

        mItemPerson.occupation = mOccupationList[position];

        if (mItemPerson.occupation.equals("Other")) {
            mEtOccupationOther.setVisibility(View.VISIBLE);
        } else {
            mEtOccupationOther.setText("");
            mEtOccupationOther.setVisibility(View.GONE);
        }

        if (isRegular(mItemPerson.occupation)) {
            setRgEnabled(mRgOccupationRegular, true);
            findViewById(R.id.tv_occupation_type).setVisibility(View.VISIBLE);
            findViewById(R.id.rg_occupation_type).setVisibility(View.VISIBLE);
        } else {
            mItemPerson.occupationType = null;
            setRgEnabled(mRgOccupationRegular, false);
            setIncomeTypeAdapter();
            findViewById(R.id.tv_occupation_type).setVisibility(View.GONE);
            findViewById(R.id.rg_occupation_type).setVisibility(View.GONE);
        }

        if (hasIncome(mItemPerson.occupation)) {
            findViewById(R.id.tv_income_title).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_income_value).setVisibility(View.VISIBLE);
        } else {
            mItemPerson.income = 0;
            mEtIncome.setText("");
            mItemPerson.incomeType = null;
            setIncomeTypeAdapter();
            findViewById(R.id.tv_income_title).setVisibility(View.GONE);
            findViewById(R.id.ll_income_value).setVisibility(View.GONE);
        }
    }

    private void setIdentityAdapter() {
        final AdapterDropDown<String> mIdentityAdapter = new AdapterDropDown<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(mIdentityList)),
                SPINNER_IDENTITY
        );
        mIdentityAdapter.setDropDownViewResource(R.layout.spinner_layout);
        mSpIdentityType.setAdapter(mIdentityAdapter);
    }

    private void setOccupationAdapter() {
        final AdapterDropDown<String> mOccupationAdapter = new AdapterDropDown<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(mOccupationList)),
                SPINNER_OCCUPATION
        );
        mOccupationAdapter.setDropDownViewResource(R.layout.spinner_layout);
        mSpOccupation.setAdapter(mOccupationAdapter);
    }

    private void setLiteracyAdapter() {
        final AdapterDropDown<String> mLiteracyAdapter = new AdapterDropDown<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(mLiteracyList)),
                SPINNER_LITERACY
        );
        mLiteracyAdapter.setDropDownViewResource(R.layout.spinner_layout);
        mSpLiteracy.setAdapter(mLiteracyAdapter);
    }

    private void setIncomeTypeAdapter() {
        final AdapterDropDown<String> adapter = new AdapterDropDown<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(mIncomeTypeList)),
                SPINNER_LITERACY
        );
        adapter.setDropDownViewResource(R.layout.spinner_layout);
        mSpIncomeType.setAdapter(adapter);
        mSpIncomeType.setSelection(1);
    }

    private void setResidentAdapter() {
        final AdapterDropDown<String> mResidentAdapter = new AdapterDropDown<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(mResidentTypeList)),
                SPINNER_RESIDENT
        );
        mResidentAdapter.setDropDownViewResource(R.layout.spinner_layout);
        mSpResident.setAdapter(mResidentAdapter);
    }

    private void setEducationAdapter() {
        final AdapterDropDown<String> mResidentAdapter = new AdapterDropDown<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(mEducationList)),
                SPINNER_RESIDENT
        );
        mResidentAdapter.setDropDownViewResource(R.layout.spinner_layout);
        mSpEducation.setAdapter(mResidentAdapter);
    }

    private void onSexSelected(int indexId) {
        mItemPerson.sex = ((RadioButton) findViewById(indexId)).getText().toString();

        if (!mSexAutoCheck) {
            mItemPerson.relationToHoh = null;
            mItemPerson.marital = null;
        } else {
            mSexAutoCheck = false;
            mSpRelation.setEnabled(true);
            return;
        }

        if (0 == getSexIndex(mItemPerson.sex) || 2 == getSexIndex(mItemPerson.sex)) {
            mItemPerson.hb = null;
            mEtHb.setText("");
            hideHb();

        } else if (1 == getSexIndex(mItemPerson.sex)) {
            if (MIN_AGE_FOR_HB <= mItemPerson.ageYear && 1 == getSexIndex(mItemPerson.sex)) {
                showHb();
            }
        }

        mSpMarital.setAdapter(getMaritalAdapter(getSexIndex(mItemPerson.sex)));
        setRelationAdapter();
        mSpRelation.setEnabled(true);
    }

    private void setFamilyIdAdapter() {
//        final AdapterDropDown<String> mFamilyIdAdapter = new AdapterDropDown<>(
//                this,
//                R.layout.spinner_layout,
//                new ArrayList<>(Arrays.asList(mFamilyIds)),
//                SPINNER_FAMILY_ID
//        );
//        etFamilyId.setAdapter(mFamilyIdAdapter);
    }

    private double findBmi() {
        try {
            float height = Float.parseFloat(mEtHeight.getText().toString());
            float weight = Float.parseFloat(mEtWeight.getText().toString());
            float bmi    = 0.0f;

            // Find the height in metre
            if (mItemPerson.heightUnit.equals(mHeightUnitList[1])) { // cm
                height = height / 100.0f;
                bmi = weight / (height * height);
                double bmiRoundOff = Math.round(bmi * 100.0) / 100.0;
                mEtBmi.setText("" + bmiRoundOff);
                return bmiRoundOff;

            } else if (mItemPerson.heightUnit.equals(mHeightUnitList[2])) { // inch
                height = height * 0.0254f;
                bmi = weight / (height * height);
                double bmiRoundOff = Math.round(bmi * 100.0) / 100.0;
                mEtBmi.setText("" + bmiRoundOff);

                return bmiRoundOff;
            }
        } catch (Exception e) {

        }

        return 0.0d;
    }

    private void onAgeSet() {
        if (null == mAgeTime) {
            return;
        }

        mItemPerson.ageYear = 0;

        if (mAgeTime.equals(mAgeTimeSpans[1])) {
            try {
                mItemPerson.ageYear = Integer.parseInt(mEtPersonAge.getText().toString());
            } catch (Exception e) {

            }
        }

        clearAnswers();

        if (MIN_AGE_FOR_QUESTIONNAIRE < mItemPerson.ageYear) {
            showQuestions();
        } else {
            hideQuestions();
        }

        if (MIN_AGE_FOR_MEASUREMENTS <= mItemPerson.ageYear) {
            showMeasurements();
        } else {
            hideMeasurements();
        }


        if (3 <= mItemPerson.ageYear && 6 >= mItemPerson.ageYear) {
            // Anganawadi
            mTvSchoolTitle.setVisibility(View.VISIBLE);
            mTvSchoolTitle.setText(getString(R.string.ques_anganwadi));
            mRgSchool.setVisibility(View.VISIBLE);
        } else if (7 <= mItemPerson.ageYear && 16 >= mItemPerson.ageYear) {
            // School
            mTvSchoolTitle.setVisibility(View.VISIBLE);
            mTvSchoolTitle.setText(getString(R.string.ques_school));
            mRgSchool.setVisibility(View.VISIBLE);
        } else {
            // Hide
            mTvSchoolTitle.setVisibility(View.GONE);
            mRgSchool.setVisibility(View.GONE);
        }

        if (10 < mItemPerson.ageYear) {
            // Show habits
            mTvHabitTitle.setVisibility(View.VISIBLE);
            findViewById(R.id.ll_habits).setVisibility(View.VISIBLE);
            // Show exercise
            mTvExerciseTitle.setVisibility(View.VISIBLE);
            mRgExercise.setVisibility(View.VISIBLE);
        } else {
            // Hide
            mTvHabitTitle.setVisibility(View.GONE);
            findViewById(R.id.ll_habits).setVisibility(View.GONE);
            mTvExerciseTitle.setVisibility(View.GONE);
            mRgExercise.setVisibility(View.GONE);
        }

        if (MIN_AGE_FOR_HB <= mItemPerson.ageYear && 1 == getSexIndex(mItemPerson.sex)) {
            showHb();
        } else {
            mItemPerson.hb = null;
            hideHb();
        }

        Log.d(TAG, "onAgeSet: " + mItemPerson.ageYear);
    }

    private void showMeasurements() {
        findViewById(R.id.tv_bp_title).setVisibility(View.VISIBLE);
        findViewById(R.id.ll_bp_value).setVisibility(View.VISIBLE);

        findViewById(R.id.tv_rbs_allow_title).setVisibility(View.VISIBLE);
        findViewById(R.id.rg_rbs_allow).setVisibility(View.VISIBLE);

        findViewById(R.id.tv_rbs_title).setVisibility(View.VISIBLE);
        findViewById(R.id.ll_rbs_value).setVisibility(View.VISIBLE);
    }

    private void hideMeasurements() {
        mItemPerson.bpSystolic = null;
        mItemPerson.bpDiastolic = null;
        mItemPerson.rbsAllow = null;
        mItemPerson.rbs = null;

        findViewById(R.id.tv_bp_title).setVisibility(View.GONE);
        findViewById(R.id.ll_bp_value).setVisibility(View.GONE);

        findViewById(R.id.tv_rbs_allow_title).setVisibility(View.GONE);
        findViewById(R.id.rg_rbs_allow).setVisibility(View.GONE);

        findViewById(R.id.tv_rbs_title).setVisibility(View.GONE);
        findViewById(R.id.ll_rbs_value).setVisibility(View.GONE);
    }

    private void showQuestions() {
        findViewById(R.id.ll_questions).setVisibility(View.VISIBLE);
    }

    private void hideQuestions() {
        mItemPerson.marital = null;
        mItemPerson.identityType = null;
        mItemPerson.literacy = null;
        mItemPerson.occupationType = null;
        mItemPerson.education = null;
        mItemPerson.income = 0;
        mItemPerson.incomeType = null;

        setRelationAdapter();
        mSpMarital.setAdapter(getMaritalAdapter(getSexIndex(mItemPerson.sex)));
        setIdentityAdapter();
        setLiteracyAdapter();
        setOccupationAdapter();

        mEtIncome.setText("");
        setIncomeTypeAdapter();

        findViewById(R.id.tv_occupation_type).setVisibility(View.GONE);
        findViewById(R.id.rg_occupation_type).setVisibility(View.GONE);
        findViewById(R.id.tv_occupation_type).setVisibility(View.GONE);
        findViewById(R.id.rg_occupation_type).setVisibility(View.GONE);

        mEtIdentityNo.setText("");

        findViewById(R.id.ll_questions).setVisibility(View.GONE);
    }

    private void setAgeTimeAdapter() {
        mAgeTimeSpans = mDb.getAgeTimes();

        final AdapterDropDown<String> adapter = new AdapterDropDown<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(mAgeTimeSpans)),
                SPINNER_AGE_TIME
        );
        adapter.setDropDownViewResource(R.layout.spinner_layout);

        mSpAgeTime.setAdapter(adapter);
    }

    private SpinnerAdapter getMaritalAdapter(int sex) {
        mMaritalList = mDb.getMaritalList(sex);

        final AdapterDropDown<String> maritalAdapter = new AdapterDropDown<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(mMaritalList)),
                SPINNER_MARITAL
        );
        maritalAdapter.setDropDownViewResource(R.layout.spinner_layout);

        return maritalAdapter;
    }

    private SpinnerAdapter setRelationAdapter(boolean isEmpty) {
        final ArrayAdapter<String> adapter
                = new ArrayAdapter<>(ActivityPersonEntry.this,
                R.layout.spinner_layout, new String[]{mItemPerson.relationToHoh});

        adapter.setDropDownViewResource(R.layout.spinner_layout);

        return adapter;
    }

    private void savePersonDetails() {
        readValues();

        if (!isAllFieldsEntered()) {
            return;
        }

        if (!isValidMeasurements()) {
            return;
        }

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setMessage(getString(R.string.message_confirm_save));
        builder.setPositiveButton(getString(R.string.btn_save), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (LAUNCH_TYPE_EDIT == mLaunchType) {
                    String id = mDb.updatePerson(mItemPerson);
                    Toasty.success(ActivityPersonEntry.this, getString(R.string.msg_update_success)
                            + "\nPerson ID: " + id, Toast.LENGTH_LONG).show();
                } else {
                    String id = mDb.addPerson(mItemPerson);
                    Toasty.success(ActivityPersonEntry.this, getString(R.string.msg_save_success)
                            + "\nPerson ID: " + id, Toast.LENGTH_LONG).show();
                }

                finish();
            }
        });
        builder.setNegativeButton(getString(R.string.btn_cancel), null);
        builder.show();
    }

    private void readValues() {
        mItemPerson.name = mEtPersonName.getText().toString();
        mItemPerson.dob = mEtDob.getText().toString();

        try {
            mItemPerson.ageYear = 0;
            mItemPerson.ageMonth = 0;
            mItemPerson.ageDay = 0;

            if (mAgeTime.equals(mAgeTimeSpans[1])) {
                mItemPerson.ageYear = Integer.parseInt(mEtPersonAge.getText().toString());
            } else if (mAgeTime.equals(mAgeTimeSpans[2])) {
                mItemPerson.ageMonth = Integer.parseInt(mEtPersonAge.getText().toString());
            } else if (mAgeTime.equals(mAgeTimeSpans[3])) {
                mItemPerson.ageDay = Integer.parseInt(mEtPersonAge.getText().toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mItemPerson.income = Integer.parseInt(mEtIncome.getText().toString());
        } catch (Exception e) {
            mItemPerson.income = -1;
            e.printStackTrace();
        }

        if (null != mItemPerson.identityType &&
                mItemPerson.identityType.equals(getString(R.string.text_other))) {
            mItemPerson.identityType = mEtIdentityTypeOther.getText().toString();
        }

        mItemPerson.identityNo = mEtIdentityNo.getText().toString();

        int educationIndex = getItemWithOthersIndex(mItemPerson.education, mEducationList);

        if (educationIndex == mEducationList.length - 1 || educationIndex == -1) {
            mItemPerson.education = mEtEducationOther.getText().toString();
        }

        int occupationIndex = getItemWithOthersIndex(mItemPerson.occupation, mOccupationList);

        if (occupationIndex == mOccupationList.length - 1 || occupationIndex == -1) {
            mItemPerson.occupation = mEtOccupationOther.getText().toString();
        }

        // Check if treatment questions are answered
        if (mCbDiseaseOther.isChecked()) {
            // Update
            mItemPerson.diseaseList.get(getOtherDiseasePosition()).disease = mEtDiseaseOther.getText().toString();
        }

        mItemPerson.disabilityList = new ArrayList<>(4);
        mDisabilityOther = null;
        if (mCbLocomotor.isChecked()) {
            mItemPerson.disabilityList.add(getString(R.string.cb_locomotor));
        }
        if (mCbMentalRetardation.isChecked()) {
            mItemPerson.disabilityList.add(getString(R.string.cb_mental_retardation));
        }
        if (mCbMentalIllness.isChecked()) {
            mItemPerson.disabilityList.add(getString(R.string.cb_mental_illness));
        }
        if (mCbHearing.isChecked()) {
            mItemPerson.disabilityList.add(getString(R.string.cb_hearing));
        }
        if (mCbLeprosy.isChecked()) {
            mItemPerson.disabilityList.add(getString(R.string.cb_leprosy));
        }
        if (mCbBlindness.isChecked()) {
            mItemPerson.disabilityList.add(getString(R.string.cb_blindness));
        }
        if (mCbLowVision.isChecked()) {
            mItemPerson.disabilityList.add(getString(R.string.cb_low_vision));
        }
        if (mCbDisabilityOther.isChecked()) {
            mItemPerson.disabilityList.add(mEtDisabilityOther.getText().toString());
            mDisabilityOther = mEtDisabilityOther.getText().toString();
        }

        mItemPerson.languagesString = "";
        if (cbUrdu.isChecked()) {
            if (mItemPerson.languagesString.isEmpty()) {
                mItemPerson.languagesString = mItemPerson.languagesString + "Urdu";
            } else {
                mItemPerson.languagesString = mItemPerson.languagesString + ";" + "Urdu";
            }
        }

        if (cbTamil.isChecked()) {
            if (mItemPerson.languagesString.isEmpty()) {
                mItemPerson.languagesString = mItemPerson.languagesString + "Tamil";
            } else {
                mItemPerson.languagesString = mItemPerson.languagesString + ";" + "Tamil";
            }
        }

        if (cbKannada.isChecked()) {
            if (mItemPerson.languagesString.isEmpty()) {
                mItemPerson.languagesString = mItemPerson.languagesString + "Kannada";
            } else {
                mItemPerson.languagesString = mItemPerson.languagesString + ";" + "Kannada";
            }
        }

        if (cbEnglish.isChecked()) {
            if (mItemPerson.languagesString.isEmpty()) {
                mItemPerson.languagesString = mItemPerson.languagesString + "English";
            } else {
                mItemPerson.languagesString = mItemPerson.languagesString + ";" + "English";
            }
        }

        if (cbLangOther.isChecked()) {
            if (mItemPerson.languagesString.isEmpty()) {
                mItemPerson.languagesString = mItemPerson.languagesString + etLangOther.getText().toString();
            } else {
                mItemPerson.languagesString = mItemPerson.languagesString + ";" + etLangOther.getText().toString();
            }
        }

        mItemPerson.ailment = mEtAilment.getText().toString();

        mItemPerson.height = mEtHeight.getText().toString();
        mItemPerson.weight = mEtWeight.getText().toString();
        mItemPerson.bpSystolic = mEtBpSystolic.getText().toString();
        mItemPerson.bpDiastolic = mEtBpDiastolic.getText().toString();
        mItemPerson.rbs = mEtRbs.getText().toString();
        mItemPerson.hb = mEtHb.getText().toString();
    }

    private boolean isAllFieldsEntered() {
        if (mItemPerson.name.equals("")) {
            Toasty.warning(this, getString(R.string.error_name), Toast.LENGTH_LONG).show();
            return false;

        } else if ((0 == mItemPerson.ageYear && 0 == mItemPerson.ageMonth && 0 == mItemPerson.ageDay)
                || (200 < mItemPerson.ageYear)) {
            Toasty.warning(this, getString(R.string.error_age), Toast.LENGTH_LONG).show();
            return false;

        } else if (null == mItemPerson.sex) {
            Toasty.warning(this, getString(R.string.error_sex), Toast.LENGTH_LONG).show();
            return false;

        } else if (null == mItemPerson.relationToHoh) {
            Toasty.warning(this, getString(R.string.error_relation), Toast.LENGTH_LONG).show();
            return false;

        } else if (null == mItemPerson.resident) {
            Toasty.warning(this, getString(R.string.error_resident), Toast.LENGTH_LONG).show();
            return false;

        } else if (mItemPerson.ageYear > MIN_AGE_FOR_QUESTIONNAIRE && null == mItemPerson.marital) {
            Toasty.warning(this, getString(R.string.error_marital), Toast.LENGTH_LONG).show();
            return false;

        } else if (mItemPerson.ageYear > MIN_AGE_FOR_QUESTIONNAIRE && null == mItemPerson.literacy) {
            Toasty.warning(this, getString(R.string.error_literacy), Toast.LENGTH_LONG).show();
            return false;

        } else if (mItemPerson.ageYear > MIN_AGE_FOR_QUESTIONNAIRE && mItemPerson.education.equals("")) {
            Toasty.warning(this, getString(R.string.error_education), Toast.LENGTH_LONG).show();
            return false;

        } else if (mItemPerson.ageYear > MIN_AGE_FOR_QUESTIONNAIRE && (null == mItemPerson.occupation || mItemPerson.occupation.isEmpty())) {
            Toasty.warning(this, getString(R.string.error_occupation), Toast.LENGTH_LONG).show();
            return false;

        } else if (mItemPerson.ageYear > MIN_AGE_FOR_QUESTIONNAIRE
                && isRegular(mItemPerson.occupation) && null == mItemPerson.occupationType) {
            Toasty.warning(this, getString(R.string.error_occupation_regular), Toast.LENGTH_LONG).show();
            return false;

        } else if (mItemPerson.ageYear > MIN_AGE_FOR_QUESTIONNAIRE
                && hasIncome(mItemPerson.occupation) && mItemPerson.income < 0) {
            Toasty.warning(this, getString(R.string.error_income), Toast.LENGTH_LONG).show();
            return false;

        } else if (mItemPerson.ageYear > MIN_AGE_FOR_QUESTIONNAIRE && null == mItemPerson.identityType) {
            Toasty.warning(this, getString(R.string.error_indentity_type), Toast.LENGTH_LONG).show();
            return false;

        } else if (mItemPerson.ageYear > MIN_AGE_FOR_QUESTIONNAIRE && mItemPerson.identityType.equals("")) {
            Toasty.warning(this, getString(R.string.error_indentity_type), Toast.LENGTH_LONG).show();
            return false;

        } else if (mItemPerson.ageYear > MIN_AGE_FOR_QUESTIONNAIRE && mItemPerson.identityNo.equals("")
                && !mItemPerson.identityType.equals(getString(R.string.none))) {
            Toasty.warning(this, getString(R.string.error_identity_number), Toast.LENGTH_LONG).show();
            return false;

        } else if ((3 <= mItemPerson.ageYear && 16 >= mItemPerson.ageYear) && null == mItemPerson.school) { // School
            Toasty.warning(this, getString(R.string.error_school), Toast.LENGTH_LONG).show();
            return false;

        } else if (mItemPerson.ageYear > 10
                && (null == mItemPerson.smoking || null == mItemPerson.tobacco || null == mItemPerson.drinking)) { // Habits
            Toasty.warning(this, getString(R.string.error_habits), Toast.LENGTH_LONG).show();
            return false;

        } else if (mItemPerson.ageYear > 10 && null == mItemPerson.exercise) {
            Toasty.warning(this, getString(R.string.error_exercise), Toast.LENGTH_LONG).show();
            return false;

        } else if (!isDiseasesFilled()) { // Treatment not answered
            Toasty.warning(this, getString(R.string.error_disease), Toast.LENGTH_LONG).show();
            return false;

        } else if (null != mDisabilityOther && mDisabilityOther.equals("")) { // Disease other
            Toasty.warning(this, getString(R.string.error_disability), Toast.LENGTH_LONG).show();
            return false;

//        } else if (30 <= mItemPerson.ageYear && null == mItemPerson.rbsAllow) {
//            Toasty.warning(this, getString(R.string.error_rbs_allow), Toast.LENGTH_LONG).show();
//            return false;
//
//        } else if (12 <= mItemPerson.ageYear && 1 == getSexIndex(mItemPerson.sex) && null == mItemPerson.hbAllow) {
//            Toasty.warning(this, getString(R.string.error_hb_allow), Toast.LENGTH_LONG).show();
//            return false;

        } else {
            return true;
        }
    }

    private boolean isValidMeasurements() {
        float height = -1;
        float weight = -1;

        int bpDiastolic = -1;
        int bpSystolic  = -1;
        int rbs         = -1;
        int hb          = -1;

        try {
            height = Integer.valueOf(mItemPerson.height);
            if (-1 != height && height < MIN_HEIGHT || height > MAX_HEIGHT) {
                Toasty.warning(this, getString(R.string.error_height), Toast.LENGTH_LONG).show();
                return false;
            } else if (null == mItemPerson.heightUnit) {
                Toasty.warning(this, getString(R.string.error_height_unit), Toast.LENGTH_LONG).show();
                return false;
            }
        } catch (Exception e) {
        }

        try {
            weight = Integer.valueOf(mItemPerson.weight);
            if (-1 != height && weight < MIN_WEIGHT || weight > MAX_WEIGHT) {
                Toasty.warning(this, getString(R.string.error_weight), Toast.LENGTH_LONG).show();
                return false;
            }
        } catch (Exception e) {
        }

        if (findBmi() > 50) {
            Toasty.warning(this, getString(R.string.error_height_weight), Toast.LENGTH_LONG).show();
            return false;
        }

        try {
            bpSystolic = Integer.valueOf(mItemPerson.bpSystolic);
        } catch (Exception e) {
        }

        try {
            bpDiastolic = Integer.valueOf(mItemPerson.bpDiastolic);
        } catch (Exception e) {
        }

        if ((-1 == bpSystolic && -1 != bpDiastolic) || (-1 != bpSystolic && -1 == bpDiastolic)) {
            Toasty.warning(this, getString(R.string.error_bp), Toast.LENGTH_LONG).show();
            return false;
        } else if (30 <= mItemPerson.ageYear && -1 != bpSystolic && -1 != bpDiastolic
                && (bpDiastolic < MIN_BP_DIASTOLIC || bpDiastolic > MAX_BP_DIASTOLIC ||
                bpSystolic < MIN_BP_SYSTOLIC || bpSystolic > MAX_BP_SYSTOLIC)) {
            Toasty.warning(this, getString(R.string.error_bp), Toast.LENGTH_LONG).show();
            return false;
        }

        try {
            rbs = Integer.valueOf(mItemPerson.rbs);

            if (null != mItemPerson.rbsAllow && mItemPerson.rbsAllow.equals("Yes")
                    && 30 <= mItemPerson.ageYear && (rbs < MIN_RBS || rbs > MAX_RBS)) {
                Toasty.warning(this, getString(R.string.error_rbs), Toast.LENGTH_LONG).show();
                return false;
            }
        } catch (Exception e) {
            if (null != mItemPerson.rbsAllow && mItemPerson.rbsAllow.equals("Yes")) {
                Toasty.warning(this, getString(R.string.error_rbs), Toast.LENGTH_LONG).show();
                return false;
            }
        }

        try {
            hb = Integer.valueOf(mItemPerson.hb);

            if (null != mItemPerson.hbAllow && mItemPerson.hbAllow.equals("Yes")
                    && 12 <= mItemPerson.ageYear && 1 == getSexIndex(mItemPerson.sex)
                    && (hb < MIN_HB || hb > MAX_HB)) {
                Toasty.warning(this, getString(R.string.error_hb), Toast.LENGTH_LONG).show();
                return false;
            }
        } catch (Exception e) {
            if (null != mItemPerson.hbAllow && mItemPerson.hbAllow.equals("Yes")) {
                Toasty.warning(this, getString(R.string.error_hb), Toast.LENGTH_LONG).show();
                return false;
            }
        }

        return true;
    }

    private boolean isDiseasesFilled() {
        for (int i = 0; i < mItemPerson.diseaseList.size(); ++i) {
            ItemDisease itemDisease = mItemPerson.diseaseList.get(i);

            if (itemDisease.disease.equals("") || null == itemDisease.treatment) {
                return false;
            }
        }

        return true;
    }


    private void onDobSet(int year, int monthOfYear, int dayOfMonth) {
        if (isFutureDate(dayOfMonth, monthOfYear, year)) {
            Toasty.warning(ActivityPersonEntry.this, getString(R.string.msg_cant_select_future_date), Toast.LENGTH_SHORT).show();
            return;
        }

        mItemPerson.dob = getFormattedDate(dayOfMonth, monthOfYear + 1, year);
        mEtDob.setText(mItemPerson.dob);

        long daysDifference = getDifferenceInDays(dayOfMonth, monthOfYear, year);

        if (daysDifference >= 365) {
            mItemPerson.ageYear = (int) (daysDifference / 365);
            mSpAgeTime.setSelection(1);
            mEtPersonAge.setText("" + mItemPerson.ageYear);
        } else if (daysDifference >= 30) {
            mItemPerson.ageMonth = (int) (daysDifference / 30);
            mSpAgeTime.setSelection(2);
            mEtPersonAge.setText("" + mItemPerson.ageMonth);
        } else {
            mItemPerson.ageDay = (int) daysDifference;

            if (0 == mItemPerson.ageDay) {
                mItemPerson.ageDay = 1;
            }

            mSpAgeTime.setSelection(3);
            mEtPersonAge.setText("" + mItemPerson.ageDay);
        }

        mEtPersonAge.setEnabled(false);
        mSpAgeTime.setEnabled(false);
    }

    private void setRgEnabled(RadioGroup radioGroup, boolean enable) {
        for (int i = 0; i < radioGroup.getChildCount(); i++) {
            radioGroup.getChildAt(i).setEnabled(enable);
        }
    }

    private int getSexIndex(String sex) {
        return getRadioIndex(sex, getResources().getStringArray(R.array.sex_types));
    }

    private class BmiTextWatcher implements TextWatcher {
        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start,
                                      int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start,
                                  int before, int count) {
            double bmi = findBmi();

            try {
                mEtBmiUnit.setTextColor(Color.WHITE);

                if (19 <= bmi && bmi <= 23) {
                    mEtBmiUnit.setBackgroundColor(LColors.GREEN);
                    return;
                } else if ((23 < bmi && bmi <= 25) || (19 > bmi && 0 < bmi)) {
                    mEtBmiUnit.setBackgroundColor(LColors.ORANGE);
                    return;
                } else if (25 < bmi) {
                    mEtBmiUnit.setBackgroundColor(LColors.RED);
                    return;
                }
            } catch (Exception e) {

            }

            mEtBmiUnit.setTextColor(Color.BLACK);
            mEtBmiUnit.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private class BpSystolicWatcher implements TextWatcher {
        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start,
                                      int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start,
                                  int before, int count) {
            onBpChanged();
        }
    }

    private class BpDiastolicWatcher implements TextWatcher {
        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start,
                                      int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start,
                                  int before, int count) {
            onBpChanged();
        }
    }

    private void onBpChanged() {
        try {
            int bpSystolic  = Integer.parseInt(mEtBpSystolic.getText().toString());
            int bpDiastolic = Integer.parseInt(mEtBpDiastolic.getText().toString());

            mEtBpUnit.setTextColor(Color.WHITE);

            if (90 <= bpSystolic && 120 >= bpSystolic && 60 <= bpDiastolic && 80 >= bpDiastolic) {
                mEtBpUnit.setBackgroundColor(LColors.GREEN);
                return;
            } else if (120 <= bpSystolic && 140 >= bpSystolic && 80 <= bpDiastolic && 90 >= bpDiastolic) {
                mEtBpUnit.setBackgroundColor(LColors.YELLOW);
                return;
            } else if (140 <= bpSystolic && 160 >= bpSystolic && 90 <= bpDiastolic && 100 >= bpDiastolic) {
                mEtBpUnit.setBackgroundColor(LColors.ORANGE);
                return;
            } else if (160 <= bpSystolic && 180 >= bpSystolic && 100 <= bpDiastolic && 110 >= bpDiastolic) {
                mEtBpUnit.setBackgroundColor(LColors.DARK_ORANGE);
                return;
            } else if (180 < bpSystolic || 110 < bpDiastolic) {
                mEtBpUnit.setBackgroundColor(LColors.RED);
                return;
            }
        } catch (Exception e) {

        }

        mEtBpUnit.setTextColor(Color.BLACK);
        mEtBpUnit.setBackgroundColor(Color.TRANSPARENT);
    }

    private class RbsTextWatcher implements TextWatcher {
        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start,
                                      int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start,
                                  int before, int count) {
            int rbs = 0;

            try {
                rbs = Integer.parseInt(s.toString());

                mEtRbsUnit.setTextColor(Color.WHITE);

                if (70 <= rbs && rbs <= 140) {
                    mEtRbsUnit.setBackgroundColor(LColors.GREEN);
                    return;
                } else if (140 < rbs && rbs <= 200) {
                    mEtRbsUnit.setBackgroundColor(LColors.ORANGE);
                    return;
                } else if (200 < rbs) {
                    mEtRbsUnit.setBackgroundColor(LColors.RED);
                    return;
                }
            } catch (Exception e) {

            }

            mEtRbsUnit.setTextColor(Color.BLACK);
            mEtRbsUnit.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private class HbTextWatcher implements TextWatcher {
        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start,
                                      int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start,
                                  int before, int count) {

            int hb = 0;

            try {
                hb = Integer.parseInt(s.toString());

                mEtHbUnit.setTextColor(Color.WHITE);

                if (12 <= hb) {
                    mEtHbUnit.setBackgroundColor(LColors.GREEN);
                    return;
                } else if (8 <= hb && 12 > hb) {
                    mEtHbUnit.setBackgroundColor(LColors.ORANGE);
                    return;
                } else if (hb < 8) {
                    mEtHbUnit.setBackgroundColor(LColors.RED);
                    return;
                }
            } catch (Exception e) {

            }

            mEtHbUnit.setTextColor(Color.BLACK);
            mEtHbUnit.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private class AgeTextWatcher implements TextWatcher {
        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start,
                                      int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start,
                                  int before, int count) {
            onAgeSet();
        }
    }

    private void showHideMeasurements() {
        if (mLlMeasurements.isShown()) {
            mLlMeasurements.setVisibility(View.GONE);
            mIvShowHideMeasurements.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
        } else {
            mLlMeasurements.setVisibility(View.VISIBLE);
            mIvShowHideMeasurements.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
        }
    }
}
