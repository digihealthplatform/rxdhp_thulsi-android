package com.dhp.hins.view.main.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.dhp.hins.utils.LUtils;
import com.dhp.hins.view.main.home.ActivityAdminHome;
import com.dhp.hins.view.main.home.ActivityNurseHome;

import static com.dhp.hins.utils.LConstants.KEY_PERSON_LOGGED_IN;
import static com.dhp.hins.utils.LConstants.LOGGED_IN_ADMIN;
import static com.dhp.hins.utils.LConstants.LOGGED_IN_NURSE;

/**
 * Created by Prabhat on 14-Apr-17.
 */

public class ActivityLoginCheck extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Intent intent;

        if (LUtils.getStringPref(KEY_PERSON_LOGGED_IN, "").equals(LOGGED_IN_ADMIN)) {
            intent = new Intent(this, ActivityAdminHome.class);
        } else if (LUtils.getStringPref(KEY_PERSON_LOGGED_IN, "").equals(LOGGED_IN_NURSE)) {
            intent = new Intent(this, ActivityNurseHome.class);
        } else {
            intent = new Intent(this, ActivityLogin.class);
        }
        
        startActivity(intent);
        
        finish();
    }
}