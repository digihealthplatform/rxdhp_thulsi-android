package com.dhp.hins.view.person;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.dhp.hins.R;
import com.dhp.hins.adapter.AdapterPerson;
import com.dhp.hins.utils.LUtils;
import com.dhp.hins.view.household.ActivityHouseholdSearch;

/**
 * Created by Prabhat on 14-Apr-17.
 */

public class ActivityPerson extends AppCompatActivity {

    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_person);

        LUtils.initToolbar(this, true, getString(R.string.title_person));
        initUi();
    }

    private void initUi() {
        findViewById(R.id.fab_add_new_person).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ActivityPerson.this, ActivityPersonEntry.class);
//                intent.putExtra()
                startActivity(intent);
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_person_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume() {
        super.onResume();

        AdapterPerson adapterPerson = new AdapterPerson(this);

        mRecyclerView.setAdapter(adapterPerson);

        if (0 == adapterPerson.getItemCount()) {
            findViewById(R.id.tv_empty).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.tv_empty).setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_family_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
            }
            break;

            case R.id.action_search:
                Intent intent = new Intent(this, ActivityPersonSearch.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
