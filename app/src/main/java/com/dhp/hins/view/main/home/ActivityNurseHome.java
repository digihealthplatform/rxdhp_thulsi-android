package com.dhp.hins.view.main.home;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.facebook.stetho.Stetho;
import com.dhp.hins.BuildConfig;
import com.dhp.hins.R;
import com.dhp.hins.utils.LUtils;
import com.dhp.hins.view.family.ActivityFamily;
import com.dhp.hins.view.household.ActivityHousehold;
import com.dhp.hins.view.main.login.ActivityLogin;
import com.dhp.hins.view.person.ActivityPerson;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.Calendar;

import es.dmoral.toasty.Toasty;

import static com.dhp.hins.storage.LDatabase.DATABASE_NAME;
import static com.dhp.hins.utils.LConstants.CODE_WRITE_PERMISSION;
import static com.dhp.hins.utils.LConstants.KEY_INSTALLATION_DATE;
import static com.dhp.hins.utils.LConstants.KEY_NURSE_NAME;
import static com.dhp.hins.utils.LConstants.KEY_NURSE_USER_NAME;
import static com.dhp.hins.utils.LConstants.KEY_PERSON_LOGGED_IN;

/**
 * Created by Prabhat on 14-Apr-17.
 */

public class ActivityNurseHome extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_nurse_home);
        LUtils.initToolbar(this, false, getString(R.string.app_name));

        initUi();

        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(
                                Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(
                                Stetho.defaultInspectorModulesProvider(this))
                        .build());
    }

    private void initUi() {
        findViewById(R.id.btn_household).setOnClickListener(this);
        findViewById(R.id.btn_family).setOnClickListener(this);
        findViewById(R.id.btn_individual).setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_nurse, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_export_data: {
                exportDataWithPermissionCheck();
//                LDbTransfer.appendRows(this);
            }
            break;

            case R.id.action_about: {
                AlertDialog.Builder builder;

                builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

                builder.setTitle(getString(R.string.title_about));
                builder.setMessage(getString(R.string.msg_application_version) + " " + BuildConfig.VERSION_NAME
                        + "\n" + getString(R.string.msg_installation_date) + " " + LUtils.getStringPref(KEY_INSTALLATION_DATE, ""));
                builder.setPositiveButton(getString(R.string.btn_ok), null);
                builder.show();
            }
            break;

            case R.id.action_my_account: {
                AlertDialog.Builder builder;

                builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

                builder.setTitle(getString(R.string.title_my_account));
                builder.setMessage(getString(R.string.msg_name) + " " + LUtils.getStringPref(KEY_NURSE_NAME, "")
                        + "\n" + getString(R.string.msg_user_name) + " " + LUtils.getStringPref(KEY_NURSE_USER_NAME, ""));
                builder.setPositiveButton(getString(R.string.btn_ok), null);
                builder.show();
            }
            break;

            case R.id.action_logout: {
                AlertDialog.Builder builder;

                builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

                builder.setMessage(getString(R.string.msg_logout));
                builder.setPositiveButton(getString(R.string.btn_logout), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LUtils.putStringPref(KEY_PERSON_LOGGED_IN, "");
                        finish();
                        startActivity(new Intent(ActivityNurseHome.this, ActivityLogin.class));
                    }
                });
                builder.setNegativeButton(getString(R.string.btn_cancel), null);
                builder.show();
            }
            break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void exportDataWithPermissionCheck() {

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (PackageManager.PERMISSION_GRANTED != permissionCheck) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CODE_WRITE_PERMISSION);
        } else {
            exportData();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case CODE_WRITE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    exportData();
                } else {
                    Toasty.error(this, getString(R.string.message_permission_denied_write), Toast.LENGTH_SHORT).show();
                }
            }
            break;
        }
    }

    private void exportData() {

        try {
            File outputPath = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.export_backup_path));

            if (!outputPath.exists()) {
                outputPath.mkdirs();
            }

            Calendar calendar = Calendar.getInstance();

            if (outputPath.canWrite()) {

                String backupDBPath = getString(R.string.prefix_export_backup) + "_"
                        + LUtils.getFileName(calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DATE),
                        calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE))
                        + ".db";

                File currentDB = getDatabasePath(DATABASE_NAME);
                File backupDB  = new File(outputPath, backupDBPath);

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();

                Toasty.success(this, getString(R.string.message_export_success), Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toasty.error(this, getString(R.string.message_export_error), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_household: {
                startActivity(new Intent(this, ActivityHousehold.class));
            }
            break;

            case R.id.btn_family: {
                startActivity(new Intent(this, ActivityFamily.class));
            }
            break;

            case R.id.btn_individual: {
                startActivity(new Intent(this, ActivityPerson.class));
            }
            break;
        }
    }
}
