package com.dhp.hins.item;

/**
 * Created by Prabhat on 17-Apr-17.
 */

public class ItemHouseholdMin {
    public final long householdIdKey;

    public final String houseNo;
    public final String locality;

    public ItemHouseholdMin(long householdIdKey, String houseNo, String locality) {
        this.householdIdKey = householdIdKey;
        this.houseNo = houseNo;
        this.locality = locality;
    }
}
