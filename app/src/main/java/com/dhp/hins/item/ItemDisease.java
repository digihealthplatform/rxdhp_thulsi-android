package com.dhp.hins.item;

/**
 * Created by Prabhat on 01-Aug-17.
 */

public class ItemDisease {
    public String disease;
    public String treatment;

    public ItemDisease(String disease, String treatment) {
        this.disease = disease;
        this.treatment = treatment;
    }
}
