package com.dhp.hins.item;

import java.util.ArrayList;

/**
 * Created by Prabhat on 16-Apr-17.
 */

public class ItemFamily {
    public long familyIdKey;

    public String familyId;
    public String houseNumber;

    public String rationCard;
    public String rationCardType;
    public String insurance;
    public String insuranceType;
    public String mealTimings;
    public String cookingFuel;
    public String treatmentPlace;
    public String recentDeath;

    public ArrayList<ItemDeathInfo> deathInfoList;

    public ItemFamily() {

    }

    public ItemFamily(long familyIdKey, String familyId, String houseNumber,
                      String rationCard, String rationCardType, String insurance, String insuranceType,
                      String mealTimings, String cookingFuel, String treatmentPlace,
                      String recentDeath, ArrayList<ItemDeathInfo> deathInfoList) {
        this.familyIdKey = familyIdKey;
        this.familyId = familyId;
        this.houseNumber = houseNumber;
        this.rationCard = rationCard;
        this.rationCardType = rationCardType;
        this.insurance = insurance;
        this.insuranceType = insuranceType;
        this.mealTimings = mealTimings;
        this.cookingFuel = cookingFuel;
        this.treatmentPlace = treatmentPlace;
        this.recentDeath = recentDeath;
        this.deathInfoList = deathInfoList;
    }
}
