package com.dhp.hins.item;

/**
 * Created by Prabhat on 17-Apr-17.
 */

public class ItemPersonMin {

    public final long personIdKey;
    
    public final String personId;
    public final String name;

    public ItemPersonMin(long personIdKey, String personId, String name) {
        this.personIdKey = personIdKey;
        this.personId = personId;

        this.name = name;
    }
}
