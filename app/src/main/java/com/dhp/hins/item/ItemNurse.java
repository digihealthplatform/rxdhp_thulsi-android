package com.dhp.hins.item;

/**
 * Created by Prabhat on 15-Apr-17.
 */

public class ItemNurse {

    public final long nurseIdKey;
    public String nurseName;
    public String nurseUserName;
    public String nursePassword;

    public ItemNurse(long nurseIdKey, String nurseName, String nurseUserName, String nursePassword) {
        this.nurseIdKey = nurseIdKey;
        this.nurseName = nurseName;
        this.nurseUserName = nurseUserName;
        this.nursePassword = nursePassword;
    }
}