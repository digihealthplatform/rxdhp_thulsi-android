package com.dhp.hins.item;

/**
 * Created by Prabhat on 15-Apr-17.
 */

public class ItemLocality {

    public final long   localityId;
    public       String locality;

    public ItemLocality(long localityId, String locality) {
        this.localityId = localityId;
        this.locality = locality;
    }
}
