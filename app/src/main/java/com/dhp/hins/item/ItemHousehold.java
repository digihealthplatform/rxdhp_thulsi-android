package com.dhp.hins.item;

/**
 * Created by Prabhat on 15-Apr-17.
 */

public class ItemHousehold {
    public int  totalRooms;
    public int  totalMembers;
    public long householdIdKey;

    public String houseNo1;
    public String houseNo2;
    public String houseStatus;
    public String streetName;
    public String landmark;
    public String houseLocality;
    public String houseType;
    public String houseOwnership;
    public String houseRespondent;
    public String houseHead;
    public String houseHeadSpouse;
    
    public String campId;
    public String surveyPerson;
    public String surveyDate;

    public ItemHousehold() {

    }

    public ItemHousehold(long householdIdKey,
                         String houseNo1, String houseNo2, String houseStatus,
                         String streetName, String landmark, String locality,
                         String houseType, String houseOwnership, int totalRooms,
                         String respondent, String houseHead, String houseHeadSpouse,
                         int totalMembers,
                         String campId, String surveyPerson, String surveyDate) {
        this.householdIdKey = householdIdKey;
        this.houseNo1 = houseNo1;
        this.houseNo2 = houseNo2;
        this.houseStatus = houseStatus;
        this.houseLocality = locality;
        this.streetName = streetName;
        this.landmark = landmark;
        this.houseType = houseType;
        this.houseOwnership = houseOwnership;
        this.totalRooms = totalRooms;
        this.houseRespondent = respondent;
        this.houseHead = houseHead;
        this.houseHeadSpouse = houseHeadSpouse;
        this.totalMembers = totalMembers;

        this.campId = campId;
        this.surveyPerson = surveyPerson;
        this.surveyDate = surveyDate;
    }
}
