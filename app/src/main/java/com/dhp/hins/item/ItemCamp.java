package com.dhp.hins.item;

/**
 * Created by Prabhat on 25-Apr-17.
 */

public class ItemCamp {

    public final long campIdKey;
    public final String campId;
    public final String campTitle;
    public final String startDate;
    public final String endDate;

    public ItemCamp(long campIdKey, String campId, String campTitle, String startDate, String endDate) {
        this.campIdKey = campIdKey;
        this.campId = campId;
        this.campTitle = campTitle;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}