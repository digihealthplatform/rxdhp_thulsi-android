package com.dhp.hins.item;

public class ItemDeathInfo {
    public int age;

    public String name;
    public String relation;
    public String deathCause;
    public String ageUnit;

    public ItemDeathInfo() {

    }

    public ItemDeathInfo(int age, String ageUnit, String name, String relation, String deathCause) {
        this.age = age;
        this.ageUnit = ageUnit;
        this.name = name;
        this.relation = relation;
        this.deathCause = deathCause;
    }
}
