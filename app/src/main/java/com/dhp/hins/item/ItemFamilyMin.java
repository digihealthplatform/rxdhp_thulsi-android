package com.dhp.hins.item;

/**
 * Created by Prabhat on 17-Apr-17.
 */

public class ItemFamilyMin {

    public final long familyIdKey;
    public final String familyId;
    public final String householdId;

    public ItemFamilyMin(long familyIdKey, String familyId, String householdId) {
        this.familyIdKey = familyIdKey;
        this.familyId = familyId;
        this.householdId = householdId;
    }
}
