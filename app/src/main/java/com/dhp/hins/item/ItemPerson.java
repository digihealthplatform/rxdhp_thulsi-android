package com.dhp.hins.item;

import java.util.ArrayList;

/**
 * Created by Prabhat on 17-Apr-17.
 */

public class ItemPerson {
    public int ageYear;
    public int ageMonth;
    public int ageDay;
    public int income;

    public long personIdKey;

    public String personId;
    public String familyId;
    public String name;
    public String relationToHoh;
    public String sex;
    public String dob;
    public String resident;
    public String marital;
    public String education;
    public String literacy;
    public String occupation;
    public String occupationType;
    public String identityType;
    public String identityNo;
    public String incomeType;
    public String school;
    public String smoking;
    public String tobacco;
    public String drinking;
    public String exercise;
    public String height;
    public String heightUnit;
    public String weight;
    public String weightUnit;
    public String bpSystolic;
    public String bpDiastolic;
    public String rbsAllow;
    public String rbs;
    public String hbAllow;
    public String hb;
    public String sick;
    public String ailment;

    public ArrayList<ItemDisease> diseaseList;
    public ArrayList<String> disabilityList;
    public String[] languages;
    public String languagesString;

    public ItemPerson() {

    }

    public ItemPerson(long personIdKey, String personId, String familyId, String name, String sex,
                      String dob, int ageYear, int ageMonth, int ageDay, String relationToHoh,
                      String resident, String languages, String marital, String education, String literacy,
                      String occupation, String occupationType,
                      int income, String incomeType,
                      String identityType, String identityNo,
                      String school, String smoking, String tobacco, String drinking, String exercise,
                      String height, String heightUnit, String weight, String weightUnit,
                      String bpSystolic, String bpDiastolic, String rbsAllow, String rbs, String hbAllow, String hb,
                      String sick, String ailment,
                      ArrayList<ItemDisease> diseaseList, ArrayList<String> disabilityList) {
        this.personIdKey = personIdKey;
        this.personId = personId;
        this.familyId = familyId;
        this.name = name;
        this.relationToHoh = relationToHoh;
        this.sex = sex;
        this.dob = dob;
        this.ageYear = ageYear;
        this.ageMonth = ageMonth;
        this.ageDay = ageDay;
        this.resident = resident;

        if (null != languages) {
            this.languages = languages.split(";");
            this.languagesString = languages;
        }

        this.marital = marital;
        this.education = education;
        this.literacy = literacy;
        this.occupation = occupation;
        this.occupationType = occupationType;
        this.income = income;
        this.incomeType = incomeType;
        this.identityType = identityType;
        this.identityNo = identityNo;
        this.school = school;
        this.smoking = smoking;
        this.tobacco = tobacco;
        this.drinking = drinking;
        this.exercise = exercise;

        this.diseaseList = diseaseList;
        this.disabilityList = disabilityList;

        this.height = height;
        this.heightUnit = heightUnit;
        this.weight = weight;
        this.weightUnit = weightUnit;
        this.bpSystolic = bpSystolic;
        this.bpDiastolic = bpDiastolic;
        this.rbsAllow = rbsAllow;
        this.rbs = rbs;
        this.hb = hb;
        this.hbAllow = hbAllow;
        this.sick = sick;
        this.ailment = ailment;
    }
}
