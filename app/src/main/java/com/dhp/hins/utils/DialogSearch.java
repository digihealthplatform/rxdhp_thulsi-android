package com.dhp.hins.utils;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.dhp.hins.R;
import com.dhp.hins.adapter.AdapterDialogSearch;
import com.dhp.hins.storage.LDatabase;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Prabhat on 27-Jul-17.
 */

public class DialogSearch implements DialogSearchCallbacks {
    private DialogSearchCallbacks mCallbacks;
    private Activity mActivity;
    private AlertDialog mDialog;
    private InputMethodManager mInputMethodManager;
    private EditText mEtRelation;
    private ArrayList<String> items;

    public DialogSearch(Activity activity, DialogSearchCallbacks callback) {
        mCallbacks = callback;
        mActivity = activity;
    }

    public DialogSearch withItems(String[] items) {
        this.items = new ArrayList(Arrays.asList(items));
        return this;
    }

    public void showDialog() {
        mInputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
//        mInputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        final View dialogView = mActivity.getLayoutInflater().inflate(R.layout.dialog_relation_type, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(mActivity, R.style.AlertDialogLight);
        builder.setView(dialogView);

        mDialog = builder.create();
        mDialog.setCanceledOnTouchOutside(false);
//        mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        final AdapterDialogSearch adapter = new AdapterDialogSearch(this, mActivity,
                items);

        RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.rv_relation_type);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(adapter);

        mEtRelation = (EditText) dialogView.findViewById(R.id.et_relation_type);
        mEtRelation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<String> items = new ArrayList<String>(0);

                for (String item : DialogSearch.this.items) {
                    if (item.toLowerCase().contains(s.toString().toLowerCase())) {
                        items.add(item);
                    }
                }

                adapter.setNewList(items);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mEtRelation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                mEtRelation.post(new Runnable() {
                    @Override
                    public void run() {
                        InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(mEtRelation, InputMethodManager.SHOW_IMPLICIT);
                    }
                });
            }
        });
        mEtRelation.requestFocus();

        mDialog.show();
    }

    public void onSelected(String relation) {
        mInputMethodManager.hideSoftInputFromWindow(mEtRelation.getWindowToken(), 0);
        mDialog.dismiss();
        mCallbacks.onSelected(relation);
    }
}