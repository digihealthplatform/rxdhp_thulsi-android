package com.dhp.hins.utils;

/**
 * Created by Prabhat on 27-Jul-17.
 */

public interface DialogSearchCallbacks {
    void onSelected(String value);
}