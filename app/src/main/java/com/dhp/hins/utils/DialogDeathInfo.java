package com.dhp.hins.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.dhp.hins.R;
import com.dhp.hins.adapter.AdapterDropDown;
import com.dhp.hins.item.ItemDeathInfo;
import com.dhp.hins.storage.LDatabase;

import java.util.ArrayList;
import java.util.Arrays;

import es.dmoral.toasty.Toasty;

import static android.view.MotionEvent.ACTION_UP;
import static com.dhp.hins.utils.LApplication.getAppContext;
import static com.dhp.hins.utils.LConstants.SPINNER_AGE_TIME;

/**
 * Created by Prabhat on 27-Jul-17.
 */

public class DialogDeathInfo {
    private int age;

    private boolean prePopulated;

    private String ageUnit;
    private String name;
    private String relation;
    private String deathCause;

    private String[] deathCauses;
    private String[] ageUnits;

    private Spinner spRelation;

    private Spinner spDeathCause;
    private Spinner spAgeUnit;

    private Activity mActivity;
    private AlertDialog mDialog;
    private EditText mEtRelation;
    private DialogDeathInfoCallbacks mCallbacks;

    public DialogDeathInfo(Activity activity, DialogDeathInfoCallbacks callbacks, ItemDeathInfo item) {
        mActivity = activity;
        mCallbacks = callbacks;

        age = item.age;
        ageUnit = item.ageUnit;
        name = item.name;
        relation = item.relation;
        deathCause = item.deathCause;
    }

    public void showDialog(final int position) {
        final View dialogView = mActivity.getLayoutInflater().inflate(R.layout.dialog_death_info, null);

        final EditText etName = (EditText) dialogView.findViewById(R.id.et_death_name);
        final EditText etAge = (EditText) dialogView.findViewById(R.id.et_death_age);
        final EditText etDeathCauseOther = (EditText) dialogView.findViewById(R.id.et_death_cause_other);

        if (null != name) {
            etName.setText(name);
            etName.setSelection(etName.getText().length());
        }

        if (0 < age) {
            etAge.setText("" + age);
        }

        spAgeUnit = (Spinner) dialogView.findViewById(R.id.sp_age_unit);
        setAgeUnitAdapter();

        if (null != ageUnit) {
            switch (ageUnit) {
                case "Year":
                    spAgeUnit.setSelection(1);
                    break;
                case "Month":
                    spAgeUnit.setSelection(2);
                    break;
                case "Day":
                    spAgeUnit.setSelection(3);
                    break;
                default:
                    spAgeUnit.setSelection(1);
                    break;
            }
        } else {
            spAgeUnit.setSelection(1);
        }

        spAgeUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    return;
                }
                ageUnit = ageUnits[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spDeathCause = (Spinner) dialogView.findViewById(R.id.sp_death_cause);
        spDeathCause.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    return;
                }

                if (!prePopulated) {
                    deathCause = deathCauses[position];
                } else {
                    prePopulated = false;
                    return;
                }

                // If Other
                if (deathCause.equals(getAppContext().getString(R.string.text_other))) {
                    etDeathCauseOther.setVisibility(View.VISIBLE);
                } else {
                    etDeathCauseOther.setText("");
                    etDeathCauseOther.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        setDeathCauseAdapter();

        if (null != deathCause) {
            int deathCauseIndex = LUtils.getItemWithOthersIndex(deathCause, deathCauses);
            if (deathCauses.length - 1 == deathCauseIndex) {
                etDeathCauseOther.setText(deathCause);
                prePopulated = true;
            }
            spDeathCause.setSelection(deathCauseIndex);
        }

        final LDatabase database = LDatabase.getInstance(mActivity);

        spRelation = (Spinner) dialogView.findViewById(R.id.sp_relation_to_hh);
        spRelation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (ACTION_UP == motionEvent.getAction()) {
                    // Show dialog

                    new DialogSearch(mActivity, new DialogSearchCallbacks() {
                        @Override
                        public void onSelected(String relation) {
                            DialogDeathInfo.this.relation = relation;
                            setRelationAdapter();
                        }
                    }).withItems(database.getRelationTypes(2)).showDialog(); // Gender = Other
                }
                return true;
            }
        });
        setRelationAdapter();

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(mActivity, R.style.AlertDialogLight);
        builder.setView(dialogView);
        builder.setTitle(getAppContext().getString(R.string.title_death_info));
        builder.setPositiveButton(mActivity.getString(R.string.button_save), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(mActivity.getString(R.string.button_cancel), null);

        mDialog = builder.create();
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.show();
        mDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    age = Integer.parseInt(etAge.getText().toString());
                } catch (Exception e) {

                }

                name = etName.getText().toString();

                if (null != deathCause && deathCause.equals(getAppContext().getString(R.string.text_other))) {
                    deathCause = etDeathCauseOther.getText().toString();
                }

                // Check the fields
                if (name.equals("")) {
                    Toasty.warning(mActivity, getAppContext().getString(R.string.warning_fill_name),
                            Toast.LENGTH_LONG).show();

                } else if (1 > age) {
                    Toasty.warning(mActivity, getAppContext().getString(R.string.warning_fill_age),
                            Toast.LENGTH_LONG).show();

                } else if (null == relation || relation.equals("")) {
                    Toasty.warning(mActivity, getAppContext().getString(R.string.warning_select_relation),
                            Toast.LENGTH_LONG).show();

                } else if (null == deathCause || deathCause.equals("")) {
                    Toasty.warning(mActivity, getAppContext().getString(R.string.warning_select_death_cause),
                            Toast.LENGTH_LONG).show();

                } else {
                    ItemDeathInfo item = new ItemDeathInfo(age, ageUnit, name, relation, deathCause);
                    mCallbacks.onDeathInfoChanged(item, position);
                    mDialog.dismiss();
                }
            }
        });
    }

    private void setRelationAdapter() {
        ArrayAdapter<String> adapter;
        if (null == relation) {
            adapter = new ArrayAdapter<>(mActivity,
                    R.layout.spinner_layout, new String[]{getAppContext().getString(R.string.text_select)});
        } else {
            adapter = new ArrayAdapter<>(mActivity,
                    R.layout.spinner_layout, new String[]{relation});
        }
        spRelation.setAdapter(adapter);
    }

    private void setDeathCauseAdapter() {
        deathCauses = LDatabase.getInstance(mActivity).getDeathCauses();
        final AdapterDropDown<String> adapter = new AdapterDropDown<>(
                mActivity,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(deathCauses)),
                SPINNER_AGE_TIME
        );
        adapter.setDropDownViewResource(R.layout.spinner_layout);
        spDeathCause.setAdapter(adapter);
    }

    private void setAgeUnitAdapter() {
        ageUnits = new String[]{"Select", "Year", "Month", "Day"};
        final AdapterDropDown<String> adapter = new AdapterDropDown<>(
                mActivity,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(ageUnits)),
                SPINNER_AGE_TIME
        );
        adapter.setDropDownViewResource(R.layout.spinner_layout);
        spAgeUnit.setAdapter(adapter);
    }
}
