package com.dhp.hins.utils;

import android.support.annotation.ColorInt;

/**
 * Created by Prabhat on 01-Aug-17.
 */

public class LColors {
    @ColorInt
    public static final int GREEN  = 0XFF8CC63E;
    @ColorInt
    public static final int YELLOW  = 0XFFFFE222;
    @ColorInt
    public static final int ORANGE = 0XFFFD8F0C;
    @ColorInt
    public static final int DARK_ORANGE = 0XFFFD700C;
    @ColorInt
    public static final int RED    = 0XFFE3520F;
}
