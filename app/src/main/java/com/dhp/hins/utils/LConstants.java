package com.dhp.hins.utils;

/**
 * Created by Prabhat on 14-Apr-17.
 */

public class LConstants {

    public static final int LAUNCH_TYPE_ADD  = 1;
    public static final int LAUNCH_TYPE_EDIT = 2;

    public static final int CODE_WRITE_PERMISSION = 46;
    public static final int CODE_READ_PERMISSION  = 93;
    public static final int CODE_PICK_DB          = 29;

    public static final String KEY_IS_FIRST_TIME     = "key_is_first_time";
    public static final String KEY_INSTALLATION_DATE = "key_installation_date";

    public static final String KEY_DEVICE_ID_DEFAULT = "D_";
    public static final String KEY_CAMP_ID_DEFAULT   = "C_";

    public static final String KEY_LAUNCH_TYPE  = "key_launch_type";
    public static final String KEY_HOUSEHOLD_ID = "key_household_id";
    public static final String KEY_FAMILY_ID    = "key_family_id";
    public static final String KEY_PERSON_ID    = "key_person_id";

    public static final String KEY_RECENT_HOUSE_NUMBER = "key_recent_household_id";
    public static final String KEY_RECENT_FAMILY_ID    = "key_recent_family_id";
    public static final String KEY_RECENT_PERSON_ID    = "key_recent_person_id";

    public static final String KEY_PERSON_LOGGED_IN = "key_person_logged_in";
    public static final String LOGGED_IN_ADMIN      = "logged_in_admin";
    public static final String LOGGED_IN_NURSE      = "logged_in_nurse";

    public static final String KEY_DEVICE_ID = "key_device_id";

    public static final String KEY_CAMP_ID    = "key_camp_id";
    public static final String KEY_CAMP_TITLE = "key_camp_title";
    public static final String KEY_START_DATE = "key_start_date";
    public static final String KEY_END_DATE   = "key_end_date";

    public static final String KEY_NURSE_NAME      = "key_nurse_name";
    public static final String KEY_NURSE_USER_NAME = "key_nurse_id";

    public static final String ADMIN_ID  = "admin";
    public static final String ADMIN_PWD = "dhp123";

    public static final int SPINNER_HOUSE_TYPE   = 1;
    public static final int SPINNER_LOCALITY     = 2;
    public static final int SPINNER_INSURANCE    = 3;
    public static final int SPINNER_TREATMENT    = 4;
    public static final int SPINNER_CAUSES       = 5;
    public static final int SPINNER_HOUSEHOLD_ID = 6;

    public static final int SPINNER_RELATION    = 6;
    public static final int SPINNER_RESIDENT    = 7;
    public static final int SPINNER_MARITAL     = 8;
    public static final int SPINNER_LITERACY    = 9;
    public static final int SPINNER_OCCUPATION  = 10;
    public static final int SPINNER_IDENTITY    = 11;
    public static final int SPINNER_HEIGHT_UNIT = 12;
    public static final int SPINNER_FAMILY_ID   = 13;
    public static final int SPINNER_RATION_TYPE = 14;
    public static final int SPINNER_AGE_TIME    = 15;

    public static final String DEFAULT_WEIGHT_UNIT = "kg";
    public static final String DEFAULT_INCOME_UNIT = "Rs";
    public static final String DEFAULT_BP_UNIT     = "mmHg";
    public static final String DEFAULT_RBS_UNIT    = "mg/dL";
    public static final String DEFAULT_HB_UNIT     = "g/dL";

    public static final int MIN_AGE                   = 0;
    public static final int MAX_AGE                   = 200;
    public static final int MIN_AGE_FOR_QUESTIONNAIRE = 5;
    public static final int MIN_AGE_FOR_MEASUREMENTS  = 30;
    public static final int MIN_AGE_FOR_HB            = 12;

    public static final float MIN_HEIGHT = 0.0f;
    public static final float MAX_HEIGHT = 250.0f;

    public static final float MIN_WEIGHT = 0.0f;
    public static final float MAX_WEIGHT = 200.0f;

    public static final float MIN_BP_SYSTOLIC = 50.0f;
    public static final float MAX_BP_SYSTOLIC = 299.0f;

    public static final float MIN_BP_DIASTOLIC = 30.0f;
    public static final float MAX_BP_DIASTOLIC = 170.0f;

    public static final float MIN_RBS = 0.0f;
    public static final float MAX_RBS = 600.0f;

    public static final int MIN_HB = 4;
    public static final int MAX_HB = 14;

    public static final String DHP_HB_APP = "com.example.youtestcal";


    public static final String temp = "12/5-Noojan W/o Chand Pasha\n" +
            "13/5-Mehavtaj W/o Harif\n" +
            "14/5-Simran W/o Imran\n" +
            "15/5-Sayeeda W/o Abdulla\n" +
            "16/5-Shanu W/o Siraj\n" +
            "17/5-Dilsha W/o Sayeed muktiyav\n" +
            "18/5-Isha W/o Chand\n" +
            "19/5-Kudusiabanu W/o Naseerpasha\n" +
            "20/5-Shabana W/o Zaheruddin\n" +
            "21/5-Anitha W/o Venkatesh\n" +
            "22/5-Sultana W/o Afzal\n" +
            "23/5-Reshma W/o Usman\n" +
            "25/5-Noorjah W/o Md Siraj\n" +
            "26/5-Ahamadi W/o Zakir\n" +
            "27/5-Yasmeen W/o Zabi\n" +
            "28/5-Zabeen W/o Suban\n" +
            "29/5-Saleem\n" +
            "30/5-Mustari\n" +
            "31/5-Nazima\n" +
            "32/5-Sumarah W/o Navshad\n" +
            "33/5-Shama W/o Ameerjan\n" +
            "34/5-Shahanaz W/o Chanu\n" +
            "35/5-Hajira W/o Firoz\n" +
            "38/5-Waseem\n" +
            "39/5-Salhabi W/o Sakeen\n" +
            "40/5-Shama W/o Azam\n" +
            "41/5-Salhabi W/o Alimbai\n" +
            "45/5-Sallu W/o Mubarak\n" +
            "48/5-Shaheen W/o Abdul Kalim\n" +
            "53/5-Naseena\n" +
            "57/5-Fayaz\n" +
            "6/6-Yasmeen W/o Yaseen\n" +
            "7/6-Thangamani W/o Sethu\n" +
            "8/6-Dilshad W/o Noorulla\n" +
            "9/6-Shahanaz W/o Shafi\n" +
            "10/6-Hasna W/o Munir pasha\n" +
            "11/6-Ruhi D/o Munir pasha\n" +
            "12/6-Asma W/o Nayeen Mohammed\n" +
            "15/6-Rasheeda W/o Aiyaz Pasha\n" +
            "18/6-Aheeda W/o House\n" +
            "20/6-Archana W/o Saravana\n" +
            "22/6-Sultana W/o Amjad\n" +
            "23/6-Arbeen W/o Bhakash\n" +
            "24/6-Hasmatunnisa W/o Chand pasha\n" +
            "25/6-Sulthana W/o Amjad\n" +
            "26/6-Naseema W/o Chand Pasha\n" +
            "28/6-Shaheentaj W/o Faizal\n" +
            "31/6-Fathima W/o Md Waji kausar\n" +
            "32/6-Shasunnisa W/o Riyazuddin\n" +
            "33/6-Shakeen W/o Mehaboob\n" +
            "34/6-Sabana W/o Iqbal\n" +
            "35/6-Shakila W/o Iqbal\n" +
            "36/6-Jamila W/o Azaar\n" +
            "37/6-Tairataj W/o Tabrez pasha\n" +
            "38/6-Shairabanu W/o Md Mustafa\n" +
            "39/6-Nargiskathun W/o Md Hasif\n" +
            "40/6-Farida W/o Riaz\n" +
            "41/6-Tasin W/o Taniver\n" +
            "42/6-Suman W/o Jeviya\n" +
            "43/6-Yasmintaj W/o Sayeed Alim\n" +
            "45/6-Taj W/o Sana ulla\n" +
            "46/6-Mubarak\n" +
            "47/6-Mubeena Begum W/o Syeed Faruk\n" +
            "48/6-Amam W/o Rahath\n" +
            "49/6-Nasima W/o Irasha Sidiq\n" +
            "50/6-Nwashad W/o Sadiya\n" +
            "51/6-Alamaz W/o Ramath\n" +
            "52/6-Dilasad W/o Anwar\n" +
            "53/6-Sainha W/o Kalim\n" +
            "54/6-Amrin W/o Md Alli\n" +
            "55/6-Rieana W/o Iliaz\n" +
            "56/6-Nasreen W/o Ali Bakash\n" +
            "58/6-Mamataj\n" +
            "59/6-Vaccant\n" +
            "60/6-Seriean W/o Kalim\n" +
            "61/6-Sahbu W/o Babu\n" +
            "64/6-Farthunisha W/o Md Safiulla\n" +
            "65/6-Reshama W/o Abdul Bashir\n" +
            "5/7-Tairabegum W/o \n" +
            "7/7-Zazima \n" +
            "10/7-Parejhon\n" +
            "12/7-Lakshmi\n" +
            "21/7-Reshma\n" +
            "25/7-Kamalamma\n" +
            "29/7-Fathima\n" +
            "31/7-Faraq\n" +
            "36/7-Locked\n" +
            "37/7-Tashim\n" +
            "38/7-Locked\n" +
            "39/7-Fathima\n" +
            "43/7-Locked\n" +
            "44/7-Waieda\n" +
            "48/7-Locked\n" +
            "49/7-Locked\n" +
            "52/7-Locked\n" +
            "53/7-Zabeen Taj\n" +
            "54/7-Locked\n" +
            "55/7-Locked\n" +
            "56/7-Dilsad\n" +
            "57/7-Locked\n" +
            "58/7-Mamataj\n" +
            "59/7-Waieda\n" +
            "60/7-Amereen\n" +
            "62/7-Zinath\n" +
            "63/7-Reshma\n" +
            "64/7-Kironisa\n" +
            "66/7-Vacant\n" +
            "67/7-Zabeena\n" +
            "70/7-Muniyamma\n" +
            "71/7-Marimuthu\n" +
            "72/7-Valley\n" +
            "76/7-Anganwadi (Modi road 15th cross)\n" +
            "77/7-Shieen taj\n" +
            "80/7-Tashina\n" +
            "81/7-Locked\n" +
            "82/7-Locked\n" +
            "83/7-Nasreen\n" +
            "85/7-Locked\n" +
            "87/7-Shabanna\n" +
            "93/7-Rubeena\n" +
            "94/7-Locked\n" +
            "96/7-Locked\n" +
            "98/7-Aseen Taj (TB Patient)\n" +
            "101/7-Reshma\n" +
            "102/7-Dilsad\n" +
            "103/7-Gulazar Begum\n" +
            "104/7-Prahana Begum\n" +
            "1/8-Kuttiyamm\n" +
            "2/8-Manjula\n" +
            "3/8-Shahera Banu\n" +
            "4/8-Maheshwari\n" +
            "5/8-Anitha\n" +
            "6/8-Amudha\n" +
            "10/8-Sujatha\n" +
            "12/8-Sumathi\n" +
            "17/8-Devi\n" +
            "18/8-Vacant\n" +
            "21/8-Shardha\n" +
            "23/8-Vacant\n" +
            "24/8-Vacant\n" +
            "28/8-Sanjamma\n" +
            "29/8-Deepa\n" +
            "33/8-Vacant\n" +
            "34/8-Nashima\n" +
            "37/8-Shashikala\n" +
            "38/8-Pushpa\n" +
            "45/8-Jereena\n" +
            "46/8-Krusieda\n" +
            "47/8-Vacant\n" +
            "49/8-Rani\n" +
            "50/8-Banu\n" +
            "56/8-Nalina\n" +
            "57/8-Ilasudhana\n" +
            "58/8-locked\n" +
            "60/8-Vacant\n" +
            "62/8-Shataj\n" +
            "63/8-Sheera\n" +
            "64/8-Nowsad Banu\n" +
            "67/8-Abibunisa\n" +
            "68/8-Gwrimma";

}