package com.dhp.hins.utils;

import com.dhp.hins.item.ItemDeathInfo;

/**
 * Created by Prabhat on 29-Jul-17.
 */

public interface DialogDeathInfoCallbacks {
    void onDeathInfoChanged(ItemDeathInfo item, int position);
}