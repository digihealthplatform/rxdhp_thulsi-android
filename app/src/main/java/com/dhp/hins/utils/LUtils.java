package com.dhp.hins.utils;

import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;

import com.dhp.hins.R;
import com.dhp.hins.item.ItemNurse;
import com.dhp.hins.storage.LDatabase;

import java.util.ArrayList;
import java.util.Calendar;

import static com.dhp.hins.utils.LApplication.getAppContext;
import static com.dhp.hins.utils.LConstants.KEY_NURSE_NAME;

/**
 * Created by Prabhat on 14-Apr-17.
 */

public class LUtils {

    public static void initToolbar(AppCompatActivity activity, boolean setHomeUpEnabled, String title) {
        Toolbar toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);

        if (setHomeUpEnabled) {
            activity.getSupportActionBar().setHomeButtonEnabled(true);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        activity.getSupportActionBar().setTitle(title);
    }

    public static void putIntPref(String prefKey, int value) {
        SharedPreferences.Editor editor = LApplication.getSharedPreferences().edit();
        editor.putInt(prefKey, value);
        editor.apply();
    }

    public static void putBooleanPref(String prefKey, boolean value) {
        SharedPreferences.Editor editor = LApplication.getSharedPreferences().edit();
        editor.putBoolean(prefKey, value);
        editor.apply();
    }

    public static void putStringPref(String prefKey, String value) {
        SharedPreferences.Editor editor = LApplication.getSharedPreferences().edit();
        editor.putString(prefKey, value);
        editor.apply();
    }

    public static int getIntPref(String prefKey, int defaultValue) {
        return LApplication.getSharedPreferences().getInt(prefKey, defaultValue);
    }

    public static boolean getBooleanPref(String prefKey, boolean defaultValue) {
        return LApplication.getSharedPreferences().getBoolean(prefKey, defaultValue);
    }

    public static String getStringPref(String prefKey, String defaultValue) {
        return LApplication.getSharedPreferences().getString(prefKey, defaultValue);
    }

    public static boolean checkAdminLogin(String adminUserName, String adminPassword) {
        if (adminUserName.equals(LConstants.ADMIN_ID) && adminPassword.equals(LConstants.ADMIN_PWD)) {
            return true;
        }

        return false;
    }

    public static boolean checkNurseLogin(String nurseUserName, String nursePassword) {
        LDatabase db = LDatabase.getInstance(getAppContext());

        ArrayList<ItemNurse> itemNurses = db.getNurseList();

        for (ItemNurse itemNurse : itemNurses) {
            if (nurseUserName.equals(itemNurse.nurseUserName) && nursePassword.equals(itemNurse.nursePassword)) {

                LUtils.putStringPref(KEY_NURSE_NAME, itemNurse.nurseName);

                return true;
            }
        }

        return false;
    }

    public static String getFormattedDate() {
        Calendar calendar = Calendar.getInstance();

        String formattedDate = "";

        if (calendar.get(Calendar.DATE) < 10) {
            formattedDate = "0" + calendar.get(Calendar.DATE);
        } else {
            formattedDate = "" + calendar.get(Calendar.DATE);
        }

        formattedDate = formattedDate + "/";

        if (calendar.get(Calendar.MONTH) < 9) {
            formattedDate = formattedDate + "0" + (calendar.get(Calendar.MONTH) + 1);
        } else {
            formattedDate = formattedDate + "" + (calendar.get(Calendar.MONTH) + 1);
        }

        formattedDate = formattedDate + "/" + calendar.get(Calendar.YEAR);

        return formattedDate;
    }

    public static long getDifferenceInDays(int date, int month, int year) {
        Calendar currentDay = Calendar.getInstance();
        Calendar birthDay = Calendar.getInstance();
        birthDay.set(Calendar.YEAR, year);
        birthDay.set(Calendar.MONTH, month);
        birthDay.set(Calendar.DATE, date);
        birthDay.set(Calendar.HOUR_OF_DAY, 0);
        birthDay.set(Calendar.MINUTE, 0);
        birthDay.set(Calendar.SECOND, 0);
        birthDay.set(Calendar.MILLISECOND, 0);

        long difference = (long) (currentDay.getTimeInMillis() - birthDay.getTimeInMillis()) / (1000 * 60 * 60 * 24);

        return difference;
    }

    public static String getFormattedDate(int date, int month, int year) {

        String formattedDate = "";

        if (date < 10) {
            formattedDate = "0" + date;
        } else {
            formattedDate = "" + date;
        }

        formattedDate = formattedDate + "/";

        if (month < 10) {
            formattedDate = formattedDate + "0" + month;
        } else {
            formattedDate = formattedDate + "" + month;
        }

        formattedDate = formattedDate + "/" + year;

        return formattedDate;
    }


    public static String getFileName(int year, int month, int date, int hour, int min) {
        String fileName;

        ++month;

        String am_pm;
        String sDate = "" + date;
        String sMonth = "" + month;

        if (hour < 1) {
            hour = 12;
            am_pm = "am";
        } else if (hour < 12) {
            am_pm = "am";
        } else if (hour == 12) {
            hour = 12;
            am_pm = "pm";
        } else {
            hour = hour % 12;
            am_pm = "pm";
        }

        String minute = Integer.toString(min);
        String hourOfDay = Integer.toString(hour);

        if (min < 10) {
            minute = "0" + minute;
        }

        if (hour < 10) {
            hourOfDay = "0" + hourOfDay;
        }

        if (date < 10) {
            sDate = "0" + date;
        }

        if (month < 10) {
            sMonth = "0" + month;
        }

        fileName = sDate + "-" + sMonth + "-" + year + "_" + hourOfDay + "-" + minute + am_pm;

        return fileName;
    }

    public static int getYesNoIndex(String yesNo) {
        // TODO: 26-Jul-17, Add it to strings

        if (null == yesNo) {
            return -1;
        }

        if (yesNo.equals("Yes")) {
            return 0;
        } else if (yesNo.equals("No")) {
            return 1;
        } else if (yesNo.equals("Don't know")) {
            return 2;
        }

        return -1;
    }

    public static String toYesNo(int index) {
        if (0 == index) {
            return "Yes";
        } else if (1 == index) {
            return "No";
        } else if (2 == index) {
            return "Don't know";
        }

        return "";
    }

    public static int getSpinnerIndex(String item, String[] items) {
        if (null == items) {
            return 0;
        }

        for (int i = 0; i < items.length; i++) {
            if (items[i].equals(item)) {
                return i;
            }
        }
        return 0;
    }

    public static int getContainsSpinnerIndex(String item, String[] items) {
        if (null == items) {
            return 0;
        }

        for (int i = 0; i < items.length; i++) {
            if (items[i].contains(item)) {
                return i;
            }
        }
        return 0;
    }

    public static int getRadioIndex(String item, String[] items) {
        if (null == items) {
            return -1;
        }

        for (int i = 0; i < items.length; i++) {
            if (items[i].equals(item)) {
                return i;
            }
        }
        return -1;
    }

    public static int getRadioWithOthersIndex(String item, String[] itemWithOthers) {
        if (null == itemWithOthers || null == item || item.equals("")) {
            return -1;
        }

        for (int i = 0; i < itemWithOthers.length; i++) {
            if (itemWithOthers[i].equals(item)) {
                return i;
            }
        }

        return itemWithOthers.length - 1;
    }

    public static long getTimeInMillis(int date, int month, int year) {
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.DATE, date);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTimeInMillis();
    }

    public static int getItemWithOthersIndex(String item, String[] itemWithOthers) {
        if (null == item || item.equals("")) {
            return -1;
        }

        for (int i = 0; i < itemWithOthers.length; ++i) {
            if (item.equals(itemWithOthers[i])) {
                return i;
            }
        }

        return itemWithOthers.length - 1;
    }


    public static int getSexIndex(String key) {
        if (key.equals(getAppContext().getString(R.string.text_male))) {
            return 0;
        } else if (key.equals(getAppContext().getString(R.string.text_female))) {
            return 1;
        } else if (key.equals(getAppContext().getString(R.string.text_sex_other))) {
            return 2;
        }

        return -1;
    }

    public static boolean isRegular(String occupation) {
        if (null == occupation) {
            return false;
        }

        if (occupation.equals(getAppContext().getString(R.string.text_retired))
                || occupation.equals(getAppContext().getString(R.string.text_unemployed))
                || occupation.equals(getAppContext().getString(R.string.text_house_wife))
                || occupation.equals(getAppContext().getString(R.string.text_old_age))
                || occupation.equals(getAppContext().getString(R.string.text_none))) {

            return false;
        }
//        if (occupation.equals(getAppContext().getString(R.string.text_domestic_work))
//                || occupation.equals(getAppContext().getString(R.string.text_manual_labour))
//                || occupation.equals(getAppContext().getString(R.string.text_semi_skilled))
//                || occupation.equals(getAppContext().getString(R.string.text_skilled))
//                || occupation.equals(getAppContext().getString(R.string.text_small_business))
//                || occupation.equals(getAppContext().getString(R.string.text_govt_employee))
//                || occupation.equals(getAppContext().getString(R.string.text_student))
//                || occupation.equals(getAppContext().getString(R.string.text_pre_school))) {
//            return true;
//        }

        return true;
    }

    public static boolean hasIncome(String occupation) {
        if (null == occupation) {
            return false;
        }

        if (occupation.equals(getAppContext().getString(R.string.text_retired))
                || occupation.equals(getAppContext().getString(R.string.text_unemployed))
                || occupation.equals(getAppContext().getString(R.string.text_house_wife))
                || occupation.equals(getAppContext().getString(R.string.text_old_age))
                || occupation.equals(getAppContext().getString(R.string.text_pre_school))
                || occupation.equals(getAppContext().getString(R.string.text_student))
                || occupation.equals(getAppContext().getString(R.string.text_none))) {

            return false;
        }

//        if (occupation.equals(getAppContext().getString(R.string.text_domestic_work))
//                || occupation.equals(getAppContext().getString(R.string.text_retired))
//                || occupation.equals(getAppContext().getString(R.string.text_manual_labour))
//                || occupation.equals(getAppContext().getString(R.string.text_semi_skilled))
//                || occupation.equals(getAppContext().getString(R.string.text_skilled))
//                || occupation.equals(getAppContext().getString(R.string.text_small_business))
//                || occupation.equals(getAppContext().getString(R.string.text_govt_employee))) {
//            return true;
//        }

        return true;
    }

    public static boolean isFutureDate(int dayOfMonth, int monthOfYear, int year) {
        Calendar calendar = Calendar.getInstance();

        long currentDateMilli = getTimeInMillis(calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
        long setDateMilli = getTimeInMillis(dayOfMonth, monthOfYear, year);

        if (currentDateMilli < setDateMilli) {
            return true;
        }

        return false;
    }

    public static CharSequence fromHtml(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(source);
        }
    }
}
