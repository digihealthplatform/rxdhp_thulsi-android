package com.dhp.hins.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.dhp.hins.BuildConfig;

import static com.dhp.hins.utils.LConstants.KEY_INSTALLATION_DATE;
import static com.dhp.hins.utils.LConstants.KEY_IS_FIRST_TIME;
import static com.dhp.hins.utils.LUtils.getFormattedDate;

/**
 * Created by Prabhat on 14-Apr-17.
 */

public class LApplication extends Application {

    private static final String TAG = LApplication.class.getSimpleName();

    private static LApplication mInstance;
    private static SharedPreferences sharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        sharedPreferences = getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE);
        
        if (LUtils.getBooleanPref(KEY_IS_FIRST_TIME, true)) {
            // Save installation date
            
            LUtils.putStringPref(KEY_INSTALLATION_DATE, getFormattedDate());
            
            LUtils.getBooleanPref(KEY_IS_FIRST_TIME, false);
        }
    }

    public static Context getAppContext() {
        return mInstance.getApplicationContext();
    }
    
    public static SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }
}
