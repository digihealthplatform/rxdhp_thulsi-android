package com.dhp.hins.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.dhp.hins.R;
import com.dhp.hins.item.ItemCamp;
import com.dhp.hins.item.ItemDeathInfo;
import com.dhp.hins.item.ItemDisease;
import com.dhp.hins.item.ItemFamily;
import com.dhp.hins.item.ItemFamilyMin;
import com.dhp.hins.item.ItemHousehold;
import com.dhp.hins.item.ItemHouseholdMin;
import com.dhp.hins.item.ItemLocality;
import com.dhp.hins.item.ItemNurse;
import com.dhp.hins.item.ItemPerson;
import com.dhp.hins.item.ItemPersonMin;
import com.dhp.hins.utils.LUtils;

import java.util.ArrayList;

import static com.dhp.hins.utils.LConstants.KEY_NURSE_NAME;
import static com.dhp.hins.utils.LConstants.KEY_RECENT_FAMILY_ID;
import static com.dhp.hins.utils.LConstants.KEY_RECENT_HOUSE_NUMBER;

/**
 * Created by Prabhat on 14-Apr-17.
 */

public class LDatabase extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;

    private static final String TAG = LDatabase.class.getSimpleName();

    public static final String DATABASE_NAME = "THULSI_Database";

    public static final String TABLE_CAMP     = "table_camp";
    public static final String TABLE_LOCALITY = "table_locality";
    public static final String TABLE_NURSE    = "table_nurse";

    public static final String TABLE_HOUSEHOLD = "table_household";

    public static final String TABLE_FAMILY            = "table_family";
    public static final String TABLE_FAMILY_DEATH_INFO = "table_family_death_info";

    public static final String TABLE_PERSON            = "table_individual";
    public static final String TABLE_PERSON_DISEASE    = "table_individual_disease";
    public static final String TABLE_PERSON_DISABILITY = "table_individual_disability";

    public static final String CAMP_KEY        = "camp_key";
    public static final String CAMP_ID         = "camp_id";
    public static final String CAMP_TITLE      = "camp_title";
    public static final String CAMP_START_DATE = "camp_start_date";
    public static final String CAMP_END_DATE   = "camp_end_date";

    public static final String LOCALITY_KEY = "locality_key";
    public static final String LOCALITY     = "houseLocality";

    public static final String NURSE_KEY       = "nurse_key";
    public static final String NURSE_NAME      = "nurse_name";
    public static final String NURSE_USER_NAME = "nurse_user_name";
    public static final String NURSE_PASSWORD  = "nurse_password";

    public static final String HOUSEHOLD_KEY       = "household_key";
    public static final String SURVEY_PERSON       = "survey_person";
    public static final String SURVEY_DATE         = "survey_date";
    public static final String HOUSE_NUMBER        = "house_number";
    public static final String HOUSE_LOCALITY      = "house_locality";
    public static final String HOUSE_STATUS        = "house_status";
    public static final String HOUSE_STREET_NAME   = "street_name";
    public static final String HOUSE_LANDMARK      = "landmark";
    public static final String HOUSE_TYPE          = "house_type";
    public static final String HOUSE_OWNERSHIP     = "ownership";
    public static final String HOUSE_TOTAL_ROOMS   = "total_rooms";
    public static final String HOUSE_RESPONDENT    = "respondent_name";
    public static final String HOUSE_HEAD          = "house_head";
    public static final String HOUSE_HEAD_SPOUSE   = "house_head_spouse";
    public static final String HOUSE_TOTAL_MEMBERS = "house_total_members";

    public static final String FAMILY_KEY              = "family_key";
    public static final String FAMILY_ID               = "family_id";
    public static final String FAMILY_RATION_CARD      = "ration_card";
    public static final String FAMILY_RATION_CARD_TYPE = "ration_card_type";
    public static final String FAMILY_INSURANCE        = "insurance";
    public static final String FAMILY_INSURANCE_TYPE   = "insurance_type";
    public static final String FAMILY_MEAL_TIMES       = "meal_times";
    public static final String FAMILY_COOKING_FUEL     = "cooking_fuel";
    public static final String FAMILY_TREATMENT_PLACE  = "treatment_place";
    public static final String FAMILY_RECENT_DEATH     = "recent_death";

    public static final String FAMILY_DEATH_INFO_KEY = "family_death_info_key";
    public static final String FAMILY_DEATH_NAME     = "name";
    public static final String FAMILY_DEATH_AGE      = "age";
    public static final String FAMILY_DEATH_AGE_UNIT = "age_unit";
    public static final String FAMILY_DEATH_RELATION = "relation_to_hoh";
    public static final String FAMILY_DEATH_CAUSE    = "death_cause";

    public static final String PERSON_KEY               = "individual_key";
    public static final String PERSON_ID                = "individual_id";
    public static final String PERSON_NAME              = "name";
    public static final String PERSON_RELATION_TO_HH    = "relation_to_hh";
    public static final String PERSON_SEX               = "sex";
    public static final String PERSON_DOB               = "dob";
    public static final String PERSON_AGE_YEAR          = "age_year";
    public static final String PERSON_AGE_MONTH         = "age_month";
    public static final String PERSON_AGE_DAY           = "age_day";
    public static final String PERSON_RESIDENT_STATUS   = "resident_status";
    public static final String PERSON_LANGUAGES         = "languages_spoken";
    public static final String PERSON_MARITAL_STATUS    = "marital_status";
    public static final String PERSON_HIGHEST_EDUCATION = "highest_education";
    public static final String PERSON_LITERACY          = "literacy";
    public static final String PERSON_OCCUPATION        = "occupation";
    public static final String PERSON_OCCUPATION_TYPE   = "occupation_type";
    public static final String PERSON_INCOME            = "income";
    public static final String PERSON_INCOME_TYPE       = "income_type";
    public static final String PERSON_IDENTITY_TYPE     = "identity_type";
    public static final String PERSON_IDENTITY_NUMBER   = "identity_number";
    public static final String PERSON_SCHOOL            = "anganwadi_school";
    public static final String PERSON_SMOKING           = "smoking";
    public static final String PERSON_TOBACCO           = "tobacco";
    public static final String PERSON_DRINKING          = "drinking";
    public static final String PERSON_EXERCISE          = "exercise";

    public static final String PERSON_HEIGHT       = "height";
    public static final String PERSON_HEIGHT_UNIT  = "height_unit";
    public static final String PERSON_WEIGHT       = "weight";
    public static final String PERSON_WEIGHT_UNIT  = "weight_unit";
    public static final String PERSON_BMI          = "bmi";
    public static final String PERSON_BP_SYSTOLIC  = "bp_systolic";
    public static final String PERSON_BP_DIASTOLIC = "bp_diastolic";
    public static final String PERSON_RBS_ALLOW    = "rbs_allow";
    public static final String PERSON_RBS          = "rbs";
    public static final String PERSON_HB_ALLOW     = "hb_allow";
    public static final String PERSON_HB           = "hb";
    public static final String PERSON_SICK         = "sick";
    public static final String PERSON_AILMENT      = "ailment";

    public static final String PERSON_DISEASE_KEY       = "disease_key";
    public static final String PERSON_DISEASE           = "disease";
    public static final String PERSON_DISEASE_TREATMENT = "on_treatment";

    public static final String PERSON_DISABILITY_KEY = "disability_key";
    public static final String PERSON_DISABILITY     = "disability";

    public static final String INACTIVE        = "inactive";
    public static final String INACTIVE_REASON = "inactive_reason";

    private static LDatabase mInstance = null;
    private Context mContext;

    public static LDatabase getInstance(Context context) {
        if (null == mInstance) {
            mInstance = new LDatabase(context);
        }

        return mInstance;
    }

    private LDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_CAMP + "("
                + CAMP_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + CAMP_ID + " TEXT, "
                + CAMP_TITLE + " TEXT, "
                + CAMP_START_DATE + " TEXT, "
                + CAMP_END_DATE + " TEXT, "
                + LOCALITY + " TEXT )";
        db.execSQL(CREATE_TABLE);

        CREATE_TABLE = "CREATE TABLE " + TABLE_LOCALITY + "("
                + LOCALITY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + LOCALITY + " TEXT )";
        db.execSQL(CREATE_TABLE);

        CREATE_TABLE = "CREATE TABLE " + TABLE_NURSE + "("
                + NURSE_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + NURSE_NAME + " TEXT, "
                + NURSE_USER_NAME + " TEXT, "
                + NURSE_PASSWORD + " TEXT )";
        db.execSQL(CREATE_TABLE);

        CREATE_TABLE = "CREATE TABLE " + TABLE_HOUSEHOLD + "("
                + HOUSEHOLD_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + HOUSE_NUMBER + " TEXT, "
                + HOUSE_STATUS + " TEXT, "
                + HOUSE_STREET_NAME + " TEXT, "
                + HOUSE_LANDMARK + " TEXT, "
                + HOUSE_LOCALITY + " TEXT, "
                + HOUSE_TYPE + " TEXT, "
                + HOUSE_OWNERSHIP + " TEXT, "
                + HOUSE_TOTAL_ROOMS + " INTEGER, "
                + HOUSE_RESPONDENT + " TEXT, "
                + HOUSE_HEAD + " TEXT, "
                + HOUSE_HEAD_SPOUSE + " TEXT, "
                + HOUSE_TOTAL_MEMBERS + " INTEGER, "
                + INACTIVE + " TEXT, "
                + INACTIVE_REASON + " TEXT, "
                + CAMP_ID + " TEXT, "
                + SURVEY_PERSON + " TEXT, "
                + SURVEY_DATE + " TEXT )";
        db.execSQL(CREATE_TABLE);

        CREATE_TABLE = "CREATE TABLE " + TABLE_FAMILY + "("
                + FAMILY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + FAMILY_ID + " TEXT, "
                + HOUSE_NUMBER + " TEXT, "
                + FAMILY_RATION_CARD + " TEXT, "
                + FAMILY_RATION_CARD_TYPE + " TEXT, "
                + FAMILY_INSURANCE + " TEXT, "
                + FAMILY_INSURANCE_TYPE + " TEXT, "
                + FAMILY_MEAL_TIMES + " TEXT, "
                + FAMILY_COOKING_FUEL + " TEXT, "
                + FAMILY_TREATMENT_PLACE + " TEXT, "
                + FAMILY_RECENT_DEATH + " TEXT, "
                + INACTIVE + " TEXT, "
                + INACTIVE_REASON + " TEXT, "
                + CAMP_ID + " TEXT, "
                + SURVEY_PERSON + " TEXT, "
                + SURVEY_DATE + " TEXT )";
        db.execSQL(CREATE_TABLE);

        CREATE_TABLE = "CREATE TABLE " + TABLE_FAMILY_DEATH_INFO + "("
                + FAMILY_DEATH_INFO_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + FAMILY_ID + " INTEGER, "
                + FAMILY_DEATH_NAME + " TEXT, "
                + FAMILY_DEATH_AGE + " INTEGER, "
                + FAMILY_DEATH_AGE_UNIT + " TEXT, "
                + FAMILY_DEATH_RELATION + " TEXT, "
                + FAMILY_DEATH_CAUSE + " TEXT )";
        db.execSQL(CREATE_TABLE);

        CREATE_TABLE = "CREATE TABLE " + TABLE_PERSON + "("
                + PERSON_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + PERSON_ID + " TEXT, "
                + FAMILY_ID + " TEXT, "
                + PERSON_NAME + " TEXT, "
                + PERSON_SEX + " TEXT, "
                + PERSON_DOB + " TEXT, "
                + PERSON_AGE_YEAR + " INTEGER, "
                + PERSON_AGE_MONTH + " INTEGER, "
                + PERSON_AGE_DAY + " INTEGER, "
                + PERSON_RELATION_TO_HH + " TEXT, "
                + PERSON_RESIDENT_STATUS + " TEXT, "
                + PERSON_LANGUAGES + " TEXT, "
                + PERSON_MARITAL_STATUS + " TEXT, "
                + PERSON_HIGHEST_EDUCATION + " TEXT, "
                + PERSON_LITERACY + " TEXT, "
                + PERSON_OCCUPATION + " TEXT, "
                + PERSON_OCCUPATION_TYPE + " TEXT, "
                + PERSON_INCOME + " INTEGER, "
                + PERSON_INCOME_TYPE + " TEXT, "
                + PERSON_IDENTITY_TYPE + " TEXT, "
                + PERSON_IDENTITY_NUMBER + " TEXT, "
                + PERSON_SCHOOL + " TEXT, "
                + PERSON_SMOKING + " TEXT, "
                + PERSON_TOBACCO + " TEXT, "
                + PERSON_DRINKING + " TEXT, "
                + PERSON_EXERCISE + " TEXT, "
                + PERSON_HEIGHT + " INTEGER, "
                + PERSON_HEIGHT_UNIT + " TEXT, "
                + PERSON_WEIGHT + " INTEGER, "
                + PERSON_WEIGHT_UNIT + " TEXT, "
                + PERSON_BMI + " TEXT, "
                + PERSON_BP_SYSTOLIC + " INTEGER, "
                + PERSON_BP_DIASTOLIC + " INTEGER, "
                + PERSON_RBS_ALLOW + " TEXT, "
                + PERSON_RBS + " INTEGER, "
                + PERSON_HB_ALLOW + " TEXT, "
                + PERSON_HB + " INTEGER, "
                + PERSON_SICK + " TEXT, "
                + PERSON_AILMENT + " TEXT, "
                + INACTIVE + " TEXT,"
                + INACTIVE_REASON + " TEXT, "
                + CAMP_ID + " TEXT, "
                + SURVEY_PERSON + " TEXT, "
                + SURVEY_DATE + " TEXT )";
        db.execSQL(CREATE_TABLE);

        CREATE_TABLE = "CREATE TABLE " + TABLE_PERSON_DISEASE + "("
                + PERSON_DISEASE_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + PERSON_ID + " INTEGER, "
                + PERSON_DISEASE + " TEXT, "
                + PERSON_DISEASE_TREATMENT + " TEXT )";
        db.execSQL(CREATE_TABLE);

        CREATE_TABLE = "CREATE TABLE " + TABLE_PERSON_DISABILITY + "("
                + PERSON_DISABILITY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + PERSON_ID + " INTEGER, "
                + PERSON_DISABILITY + " TEXT )";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }

    public ItemNurse addNurse(String nurseName, String nurseUserName, String nursePassword) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(NURSE_NAME, nurseName);
        contentValues.put(NURSE_USER_NAME, nurseUserName);
        contentValues.put(NURSE_PASSWORD, nursePassword);

        long id = db.insert(TABLE_NURSE, null, contentValues);

        return new ItemNurse(id, nurseName, nurseUserName, nursePassword);
    }

    public void updateNurse(ItemNurse itemNurse) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(NURSE_NAME, itemNurse.nurseName);
        contentValues.put(NURSE_USER_NAME, itemNurse.nurseUserName);
        contentValues.put(NURSE_PASSWORD, itemNurse.nursePassword);

        db.update(TABLE_NURSE, contentValues, NURSE_KEY + " = " + itemNurse.nurseIdKey, null);
    }

    public ArrayList<ItemNurse> getNurseList() {
        ArrayList<ItemNurse> itemNurses = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NURSE + " ORDER BY " + NURSE_KEY + " ASC ", null);

        if (cursor.moveToFirst()) {
            do {
                itemNurses.add(new ItemNurse(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3)
                ));

            } while (cursor.moveToNext());
        }

        cursor.close();

        return itemNurses;
    }

    public void removeNurse(long nurseIdKey) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NURSE, NURSE_KEY + " = '" + nurseIdKey + "'", null);
    }

    public String addHousehold(ItemHousehold household) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(HOUSE_NUMBER, household.houseNo1 + "/" + household.houseNo2);
        contentValues.put(HOUSE_STATUS, household.houseStatus);
        contentValues.put(HOUSE_STREET_NAME, household.streetName);
        contentValues.put(HOUSE_LANDMARK, household.landmark);
        contentValues.put(HOUSE_LOCALITY, household.houseLocality);
        contentValues.put(HOUSE_TYPE, household.houseType);
        contentValues.put(HOUSE_OWNERSHIP, household.houseOwnership);

        if (household.totalRooms > 0) {
            contentValues.put(HOUSE_TOTAL_ROOMS, household.totalRooms);
        }

        contentValues.put(HOUSE_RESPONDENT, household.houseRespondent);
        contentValues.put(HOUSE_HEAD, household.houseHead);
        contentValues.put(HOUSE_HEAD_SPOUSE, household.houseHeadSpouse);

        if (household.totalMembers > 0) {
            contentValues.put(HOUSE_TOTAL_MEMBERS, household.totalMembers);
        }

        contentValues.put(CAMP_ID, getCampDetails().campId);
        contentValues.put(SURVEY_PERSON, LUtils.getStringPref(KEY_NURSE_NAME, ""));
        contentValues.put(SURVEY_DATE, LUtils.getFormattedDate());
//        contentValues.put(SURVEY_DATE, LUtils.getStringPref("s_date", ""));

        long id = db.insert(TABLE_HOUSEHOLD, null, contentValues);

        LUtils.putStringPref(KEY_RECENT_HOUSE_NUMBER, household.houseNo1 + "/" + household.houseNo2);

        return household.houseNo1 + "/" + household.houseNo2;
    }

    public String updateHousehold(ItemHousehold household) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(HOUSE_NUMBER, household.houseNo1 + "/" + household.houseNo2);
        contentValues.put(HOUSE_STATUS, household.houseStatus);
        contentValues.put(HOUSE_STREET_NAME, household.streetName);
        contentValues.put(HOUSE_LANDMARK, household.landmark);
        contentValues.put(HOUSE_LOCALITY, household.houseLocality);
        contentValues.put(HOUSE_TYPE, household.houseType);
        contentValues.put(HOUSE_OWNERSHIP, household.houseOwnership);
        contentValues.put(HOUSE_TOTAL_ROOMS, household.totalRooms);
        contentValues.put(HOUSE_RESPONDENT, household.houseRespondent);
        contentValues.put(HOUSE_HEAD, household.houseHead);
        contentValues.put(HOUSE_HEAD_SPOUSE, household.houseHeadSpouse);
        contentValues.put(HOUSE_TOTAL_MEMBERS, household.totalMembers);

        contentValues.put(CAMP_ID, getCampDetails().campId);
        contentValues.put(SURVEY_PERSON, LUtils.getStringPref(KEY_NURSE_NAME, ""));
        contentValues.put(SURVEY_DATE, LUtils.getFormattedDate());

        db.update(TABLE_HOUSEHOLD, contentValues, HOUSEHOLD_KEY + " = " + household.householdIdKey, null);

        return household.houseNo1 + "/" + household.houseNo2;
    }

    public ArrayList<ItemHouseholdMin> getHouseholdList() {
        return getHouseholdList("");
    }

    public ArrayList<ItemHouseholdMin> getHouseholdList(String searchText) {
        ArrayList<ItemHouseholdMin> itemHouseholds = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor;

        if (searchText.equals("")) {
            cursor = db.rawQuery("SELECT * ,"
                    + " RTRIM(house_number, '/') AS first, "
                    + " REPLACE(house_number, house_number, SUBSTR(house_number , instr(house_number, '/') + 1)) AS second "
                    + " FROM " + TABLE_HOUSEHOLD
                    + "  ORDER BY " + " CAST(second AS INTEGER) , CAST(first AS INTEGER) ASC", null);
        } else {
            cursor = db.rawQuery("SELECT * ,"
                    + " RTRIM(house_number, '/') AS first, "
                    + " REPLACE(house_number, house_number, SUBSTR(house_number , instr(house_number, '/') + 1)) AS second "
                    + " FROM " + TABLE_HOUSEHOLD
                    + " WHERE " + HOUSE_NUMBER + " LIKE '" + searchText + "%'"
                    + "  ORDER BY " + " CAST(second AS INTEGER) , CAST(first AS INTEGER) ASC", null);
        }

        if (cursor.moveToFirst()) {
            do {
                itemHouseholds.add(new ItemHouseholdMin(
                        cursor.getLong(0),
                        cursor.getString(1),
                        cursor.getString(5)
                ));

            } while (cursor.moveToNext());
        }

        cursor.close();

        return itemHouseholds;
    }

    public ItemHousehold getHouseholdDetails(long householdId) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_HOUSEHOLD + " WHERE " + HOUSEHOLD_KEY + " = " + householdId, null);

        if (cursor.getCount() == 0) {
            return null;
        }

        cursor.moveToFirst();

        ItemHousehold itemHousehold = new ItemHousehold(
                cursor.getLong(0),
                cursor.getString(1).split("/")[0],
                cursor.getString(1).split("/")[1],
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getString(5),
                cursor.getString(6),
                cursor.getString(7),
                cursor.getInt(8),
                cursor.getString(9),
                cursor.getString(10),
                cursor.getString(11),
                cursor.getInt(12),

                // 13 Active
                // 14 Reason

                cursor.getString(15),
                cursor.getString(16),
                cursor.getString(17));

        cursor.close();

        return itemHousehold;
    }

    public void removeHousehold(long householdIdKey) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_HOUSEHOLD, HOUSEHOLD_KEY + " = '" + householdIdKey + "'", null);
    }

    public String addFamily(ItemFamily family) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(HOUSE_NUMBER, family.houseNumber);
        contentValues.put(FAMILY_ID, family.familyId);
        contentValues.put(FAMILY_RATION_CARD, family.rationCard);
        contentValues.put(FAMILY_RATION_CARD_TYPE, family.rationCardType);
        contentValues.put(FAMILY_INSURANCE, family.insurance);
        contentValues.put(FAMILY_INSURANCE_TYPE, family.insuranceType);
        contentValues.put(FAMILY_MEAL_TIMES, family.mealTimings);
        contentValues.put(FAMILY_COOKING_FUEL, family.cookingFuel);
        contentValues.put(FAMILY_TREATMENT_PLACE, family.treatmentPlace);
        contentValues.put(FAMILY_RECENT_DEATH, family.recentDeath);

        contentValues.put(CAMP_ID, getCampDetails().campId);
        contentValues.put(SURVEY_PERSON, LUtils.getStringPref(KEY_NURSE_NAME, ""));
        contentValues.put(SURVEY_DATE, LUtils.getFormattedDate());
//        contentValues.put(SURVEY_DATE, LUtils.getStringPref("s_date", ""));

        String familyId = family.houseNumber + "/F" + (getFamilyCount(family.houseNumber) + 1);

        long id = db.insert(TABLE_FAMILY, null, contentValues);

        LUtils.putStringPref(KEY_RECENT_FAMILY_ID, familyId);

        String strSQL = "UPDATE " + TABLE_FAMILY + " SET " + FAMILY_ID + " = '" + familyId + "' WHERE " + FAMILY_KEY + " = " + id;

        db.execSQL(strSQL);

        if (null == family.deathInfoList) {
            return familyId;
        }
//         Save in Family recentDeath info
        for (ItemDeathInfo deathInfo : family.deathInfoList) {
            contentValues = new ContentValues();
            contentValues.put(FAMILY_ID, id);
            contentValues.put(FAMILY_DEATH_AGE, deathInfo.age);
            contentValues.put(FAMILY_DEATH_AGE_UNIT, deathInfo.ageUnit);
            contentValues.put(FAMILY_DEATH_NAME, deathInfo.name);
            contentValues.put(FAMILY_DEATH_RELATION, deathInfo.relation);
            contentValues.put(FAMILY_DEATH_CAUSE, deathInfo.deathCause);

            db.insert(TABLE_FAMILY_DEATH_INFO, null, contentValues);
        }

        return familyId;
    }

    private int getFamilyCount(String houseNumber) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_FAMILY + " WHERE " + HOUSE_NUMBER + " LIKE '" + houseNumber + "'", null);

        if (cursor.moveToFirst()) {

            Log.d(TAG, "getFamilyCount: " + cursor.getCount());
            return cursor.getCount();
        }

        cursor.close();

        return 0;
    }

    public ArrayList<ItemFamilyMin> getFamilyList() {
        return getFamilyList("");
    }

    public ArrayList<ItemFamilyMin> getFamilyList(String searchText) {
        ArrayList<ItemFamilyMin> itemFamilies = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor;

        if (searchText.equals("")) {
            cursor = db.rawQuery("SELECT * ,"
                    + " RTRIM(family_id, '/') AS first, "
                    + " REPLACE(family_id, family_id, SUBSTR(family_id , instr(family_id, '/')+1)) AS second "
                    + " FROM " + TABLE_FAMILY
                    + " ORDER BY " + " CAST(second AS INTEGER) , CAST(first AS INTEGER) ASC", null);
        } else {
            cursor = db.rawQuery("SELECT * ,"
                    + " RTRIM(family_id, '/') AS first, "
                    + " REPLACE(family_id, family_id, SUBSTR(family_id , instr(family_id, '/')+1)) AS second "
                    + " FROM " + TABLE_FAMILY
                    + " WHERE " + FAMILY_ID + " LIKE '" + searchText + "%'"
                    + " ORDER BY " + " CAST(second AS INTEGER) , CAST(first AS INTEGER) ASC", null);
        }

        if (cursor.moveToFirst()) {
            do {
                itemFamilies.add(new ItemFamilyMin(
                        cursor.getLong(0),
                        cursor.getString(1),
                        cursor.getString(2)));

            } while (cursor.moveToNext());
        }

        cursor.close();

        return itemFamilies;
    }

    public ItemFamily getFamilyDetails(long familyIdKey) {

        SQLiteDatabase db = getReadableDatabase();

        ItemFamily itemFamily = null;

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_FAMILY + " WHERE " + FAMILY_KEY + " = " + familyIdKey, null);

        if (cursor.moveToFirst()) {

            ArrayList<ItemDeathInfo> deathInfoList = new ArrayList<>();

            Cursor tempCursor = db.rawQuery("SELECT * FROM " + TABLE_FAMILY_DEATH_INFO + " WHERE "
                    + FAMILY_ID + " = " + cursor.getLong(0), null);

            if (tempCursor.moveToFirst()) {
                do {
                    deathInfoList.add(new ItemDeathInfo(tempCursor.getInt(3),
                            tempCursor.getString(4),
                            tempCursor.getString(2),
                            tempCursor.getString(5),
                            tempCursor.getString(6)));
                } while (tempCursor.moveToNext());
            }

            tempCursor.close();

            itemFamily = new ItemFamily(
                    cursor.getLong(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getString(6),
                    cursor.getString(7),
                    cursor.getString(8),
                    cursor.getString(9),
                    cursor.getString(10),
                    deathInfoList);
        }

        cursor.close();

        return itemFamily;
    }

    public String updateFamily(ItemFamily family) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        ItemFamily itemFamily = getFamilyDetails(family.familyIdKey);

        if (!itemFamily.houseNumber.equals(family.houseNumber)) {
            family.familyId = family.houseNumber + "/F" + (getFamilyCount(family.houseNumber) + 1);
            contentValues.put(HOUSE_NUMBER, family.houseNumber);
        }

        contentValues.put(FAMILY_ID, family.familyId);

        contentValues.put(FAMILY_RATION_CARD, family.rationCard);
        contentValues.put(FAMILY_RATION_CARD_TYPE, family.rationCardType);
        contentValues.put(FAMILY_INSURANCE, family.insurance);
        contentValues.put(FAMILY_INSURANCE_TYPE, family.insuranceType);
        contentValues.put(FAMILY_MEAL_TIMES, family.mealTimings);
        contentValues.put(FAMILY_COOKING_FUEL, family.cookingFuel);
        contentValues.put(FAMILY_TREATMENT_PLACE, family.treatmentPlace);
        contentValues.put(FAMILY_RECENT_DEATH, family.recentDeath);

        db.update(TABLE_FAMILY, contentValues, FAMILY_KEY + " = " + family.familyIdKey, null);

        // Remove old problems and add new
        db.delete(TABLE_FAMILY_DEATH_INFO, FAMILY_ID + " = " + family.familyIdKey, null);

        if (null == family.deathInfoList) {
            return family.familyId;
        }

        // Save in Family recentDeath info
        for (ItemDeathInfo deathInfo : family.deathInfoList) {
            contentValues = new ContentValues();
            contentValues.put(FAMILY_ID, family.familyIdKey);
            contentValues.put(FAMILY_DEATH_AGE, deathInfo.age);
            contentValues.put(FAMILY_DEATH_AGE_UNIT, deathInfo.ageUnit);
            contentValues.put(FAMILY_DEATH_NAME, deathInfo.name);
            contentValues.put(FAMILY_DEATH_RELATION, deathInfo.relation);
            contentValues.put(FAMILY_DEATH_CAUSE, deathInfo.deathCause);

            db.insert(TABLE_FAMILY_DEATH_INFO, null, contentValues);
        }

        return family.familyId;
    }

    public String addPerson(ItemPerson person) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(FAMILY_ID, person.familyId);
        contentValues.put(PERSON_NAME, person.name);
        contentValues.put(PERSON_RELATION_TO_HH, person.relationToHoh);
        contentValues.put(PERSON_DOB, person.dob);
        contentValues.put(PERSON_AGE_YEAR, person.ageYear);
        contentValues.put(PERSON_AGE_MONTH, person.ageMonth);
        contentValues.put(PERSON_AGE_DAY, person.ageDay);
        contentValues.put(PERSON_SEX, person.sex);
        contentValues.put(PERSON_RESIDENT_STATUS, person.resident);
        contentValues.put(PERSON_LANGUAGES, person.languagesString);
        contentValues.put(PERSON_MARITAL_STATUS, person.marital);
        contentValues.put(PERSON_HIGHEST_EDUCATION, person.education);
        contentValues.put(PERSON_LITERACY, person.literacy);
        contentValues.put(PERSON_OCCUPATION, person.occupation);
        contentValues.put(PERSON_OCCUPATION_TYPE, person.occupationType);
        contentValues.put(PERSON_IDENTITY_TYPE, person.identityType);
        contentValues.put(PERSON_IDENTITY_NUMBER, person.identityNo);

        if (person.income > 0) {
            contentValues.put(PERSON_INCOME, person.income);
            contentValues.put(PERSON_INCOME_TYPE, person.incomeType);
        }

        contentValues.put(PERSON_SCHOOL, person.school);
        contentValues.put(PERSON_SMOKING, person.smoking);
        contentValues.put(PERSON_TOBACCO, person.tobacco);
        contentValues.put(PERSON_DRINKING, person.drinking);
        contentValues.put(PERSON_EXERCISE, person.exercise);

        contentValues.put(PERSON_HEIGHT, person.height);
        contentValues.put(PERSON_HEIGHT_UNIT, person.heightUnit);
        contentValues.put(PERSON_WEIGHT, person.weight);
        contentValues.put(PERSON_WEIGHT_UNIT, "kg");

        contentValues.put(PERSON_BMI, findBmi(person.height, person.heightUnit, person.weight));

        contentValues.put(PERSON_BP_DIASTOLIC, person.bpDiastolic);
        contentValues.put(PERSON_BP_SYSTOLIC, person.bpSystolic);
        contentValues.put(PERSON_RBS_ALLOW, person.rbsAllow);
        contentValues.put(PERSON_RBS, person.rbs);
        contentValues.put(PERSON_HB_ALLOW, person.hbAllow);
        contentValues.put(PERSON_HB, person.hb);
        contentValues.put(PERSON_SICK, person.sick);
        contentValues.put(PERSON_AILMENT, person.ailment);

        contentValues.put(CAMP_ID, getCampDetails().campId);
        contentValues.put(SURVEY_PERSON, LUtils.getStringPref(KEY_NURSE_NAME, ""));
        contentValues.put(SURVEY_DATE, LUtils.getFormattedDate());
//        contentValues.put(SURVEY_DATE, LUtils.getStringPref("s_date", ""));

        String personId = person.familyId + "/P" + (getPersonCount(person.familyId) + 1);

        long id = db.insert(TABLE_PERSON, null, contentValues);

        String strSQL = "UPDATE " + TABLE_PERSON + " SET " + PERSON_ID + " = '" + personId + "' WHERE " + PERSON_KEY + " = " + id;

        db.execSQL(strSQL);

        if (null == person.diseaseList) {
            return personId;
        }
        for (ItemDisease itemDisease : person.diseaseList) {
            contentValues = new ContentValues();
            contentValues.put(PERSON_ID, id);
            contentValues.put(PERSON_DISEASE, itemDisease.disease);
            contentValues.put(PERSON_DISEASE_TREATMENT, itemDisease.treatment);

            db.insert(TABLE_PERSON_DISEASE, null, contentValues);
        }

        if (null == person.disabilityList) {
            return personId;
        }

        for (String disability : person.disabilityList) {
            contentValues = new ContentValues();
            contentValues.put(PERSON_ID, id);
            contentValues.put(PERSON_DISABILITY, disability);

            db.insert(TABLE_PERSON_DISABILITY, null, contentValues);
        }

        return personId;
    }

    private int getPersonCount(String familyId) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PERSON + " WHERE " + FAMILY_ID + " LIKE '" + familyId + "'", null);

        if (cursor.moveToFirst()) {
            Log.d(TAG, "getPersonCount: " + cursor.getCount());
            return cursor.getCount();
        }

        cursor.close();

        return 0;
    }

    public ArrayList<ItemPersonMin> getPersonList() {
        return getPersonListSearch("");
    }

    public ArrayList<ItemPersonMin> getPersonListSearch(String searchText) {
        ArrayList<ItemPersonMin> itemPersons = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor;

        if (searchText.equals("")) {
            cursor = db.rawQuery("SELECT * ,"
                    + " RTRIM(individual_id, '/') AS first, "
                    + " REPLACE(individual_id, individual_id, SUBSTR(individual_id , instr(individual_id, '/')+1)) AS second "
                    + " FROM " + TABLE_PERSON
                    + "  ORDER BY " + " CAST(second AS INTEGER) , CAST(first AS INTEGER) ASC", null);
        } else {
            cursor = db.rawQuery("SELECT * ,"
                    + " RTRIM(individual_id, '/') AS first, "
                    + " REPLACE(individual_id, individual_id, SUBSTR(individual_id , instr(individual_id, '/')+1)) AS second "
                    + " FROM " + TABLE_PERSON
                    + " WHERE " + PERSON_ID + " LIKE '" + searchText + "%'"
                    + "  ORDER BY " + " CAST(second AS INTEGER) , CAST(first AS INTEGER) ASC", null);
        }

        if (cursor.moveToFirst()) {
            do {
                itemPersons.add(new ItemPersonMin(
                        cursor.getLong(0),
                        cursor.getString(1),
                        cursor.getString(3)));

            } while (cursor.moveToNext());
        }

        cursor.close();

        return itemPersons;
    }

    public ArrayList<ItemPerson> getPersonList(String familyId) {
        ArrayList<ItemPerson> itemPersons = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PERSON + " WHERE " + FAMILY_ID + " = '" + familyId + "' ORDER BY " + PERSON_KEY + " DESC", null);

        if (cursor.moveToFirst()) {
            do {
                ItemPerson itemPerson = getPersonDetails(cursor.getLong(0));
                itemPersons.add(itemPerson);

            } while (cursor.moveToNext());
        }

        cursor.close();

        return itemPersons;
    }

    public ItemPerson getPersonDetails(long personIdKey) {
        SQLiteDatabase db = getReadableDatabase();

        ItemPerson itemPerson = null;

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PERSON + " WHERE " + PERSON_KEY + " = " + personIdKey, null);

        if (cursor.moveToFirst()) {
            ArrayList<ItemDisease> itemDiseaseList    = new ArrayList<>(4);
            ArrayList<String>      itemDisabilityList = new ArrayList<>(4);

            Cursor tempCursor = db.rawQuery("SELECT * FROM " + TABLE_PERSON_DISEASE + " WHERE "
                    + PERSON_ID + " = " + cursor.getLong(0), null);

            if (tempCursor.moveToFirst()) {
                do {
                    itemDiseaseList.add(new ItemDisease(tempCursor.getString(2),
                            tempCursor.getString(3)));
                } while (tempCursor.moveToNext());
            }

            tempCursor.close();

            tempCursor = db.rawQuery("SELECT * FROM " + TABLE_PERSON_DISABILITY + " WHERE "
                    + PERSON_ID + " = " + cursor.getLong(0), null);

            if (tempCursor.moveToFirst()) {
                do {
                    itemDisabilityList.add(tempCursor.getString(2));
                } while (tempCursor.moveToNext());
            }

            tempCursor.close();

            itemPerson = new ItemPerson(
                    cursor.getLong(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getInt(6),
                    cursor.getInt(7),
                    cursor.getInt(8),
                    cursor.getString(9),
                    cursor.getString(10),
                    cursor.getString(11),
                    cursor.getString(12),
                    cursor.getString(13),
                    cursor.getString(14),
                    cursor.getString(15),
                    cursor.getString(16),
                    cursor.getInt(17),
                    cursor.getString(18),
                    cursor.getString(19),
                    cursor.getString(20),
                    cursor.getString(21),
                    cursor.getString(22),
                    cursor.getString(23),
                    cursor.getString(24),
                    cursor.getString(25),
                    cursor.getString(26),
                    cursor.getString(27),
                    cursor.getString(28),
                    cursor.getString(30),
                    cursor.getString(31),
                    cursor.getString(32),
                    cursor.getString(33),
                    cursor.getString(34),
                    cursor.getString(35),
                    cursor.getString(36),
                    cursor.getString(37),
                    cursor.getString(38),
                    itemDiseaseList,
                    itemDisabilityList);
        }

        cursor.close();

        return itemPerson;
    }

    public String updatePerson(ItemPerson person) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(FAMILY_ID, person.familyId);
        contentValues.put(PERSON_NAME, person.name);
        contentValues.put(PERSON_RELATION_TO_HH, person.relationToHoh);
        contentValues.put(PERSON_DOB, person.dob);
        contentValues.put(PERSON_AGE_YEAR, person.ageYear);
        contentValues.put(PERSON_AGE_MONTH, person.ageMonth);
        contentValues.put(PERSON_AGE_DAY, person.ageDay);
        contentValues.put(PERSON_SEX, person.sex);
        contentValues.put(PERSON_RESIDENT_STATUS, person.resident);
        contentValues.put(PERSON_LANGUAGES, person.languagesString);
        contentValues.put(PERSON_MARITAL_STATUS, person.marital);
        contentValues.put(PERSON_HIGHEST_EDUCATION, person.education);
        contentValues.put(PERSON_LITERACY, person.literacy);
        contentValues.put(PERSON_OCCUPATION, person.occupation);
        contentValues.put(PERSON_OCCUPATION_TYPE, person.occupationType);
        contentValues.put(PERSON_IDENTITY_TYPE, person.identityType);
        contentValues.put(PERSON_IDENTITY_NUMBER, person.identityNo);
        contentValues.put(PERSON_INCOME, person.income);
        contentValues.put(PERSON_INCOME_TYPE, person.incomeType);
        contentValues.put(PERSON_SCHOOL, person.school);
        contentValues.put(PERSON_SMOKING, person.smoking);
        contentValues.put(PERSON_TOBACCO, person.tobacco);
        contentValues.put(PERSON_DRINKING, person.drinking);
        contentValues.put(PERSON_EXERCISE, person.exercise);

        contentValues.put(PERSON_HEIGHT, person.height);
        contentValues.put(PERSON_HEIGHT_UNIT, person.heightUnit);
        contentValues.put(PERSON_WEIGHT, person.weight);
        contentValues.put(PERSON_WEIGHT_UNIT, "kg");
        contentValues.put(PERSON_BP_DIASTOLIC, person.bpDiastolic);
        contentValues.put(PERSON_BP_SYSTOLIC, person.bpSystolic);
        contentValues.put(PERSON_RBS_ALLOW, person.rbsAllow);
        contentValues.put(PERSON_RBS, person.rbs);
        contentValues.put(PERSON_HB_ALLOW, person.hbAllow);
        contentValues.put(PERSON_HB, person.hb);
        contentValues.put(PERSON_SICK, person.sick);
        contentValues.put(PERSON_AILMENT, person.ailment);

        contentValues.put(CAMP_ID, getCampDetails().campId);
        contentValues.put(SURVEY_PERSON, LUtils.getStringPref(KEY_NURSE_NAME, ""));
        contentValues.put(SURVEY_DATE, LUtils.getFormattedDate());

        ItemPerson itemPerson = getPersonDetails(person.personIdKey);

        if (!itemPerson.familyId.equals(person.familyId)) {
            itemPerson.personId = person.familyId + "/P" + (getPersonCount(person.familyId) + 1);
            contentValues.put(FAMILY_ID, person.familyId);
        }

        contentValues.put(PERSON_ID, itemPerson.personId);

        db.update(TABLE_PERSON, contentValues, PERSON_KEY + " = " + person.personIdKey, null);

        // Remove old problems and add new
        db.delete(TABLE_PERSON_DISEASE, PERSON_ID + " = " + person.personIdKey, null);

        if (null == person.diseaseList) {
            return itemPerson.personId;
        }

        for (ItemDisease itemDisease : person.diseaseList) {
            contentValues = new ContentValues();
            contentValues.put(PERSON_ID, person.personIdKey);
            contentValues.put(PERSON_DISEASE, itemDisease.disease);
            contentValues.put(PERSON_DISEASE_TREATMENT, itemDisease.treatment);

            db.insert(TABLE_PERSON_DISEASE, null, contentValues);
        }

        // Remove old problems and add new
        db.delete(TABLE_PERSON_DISABILITY, PERSON_ID + " = " + person.personIdKey, null);

        if (null == person.disabilityList) {
            return itemPerson.personId;
        }
        for (String disability : person.disabilityList) {
            contentValues = new ContentValues();
            contentValues.put(PERSON_ID, person.personIdKey);
            contentValues.put(PERSON_DISABILITY, disability);

            db.insert(TABLE_PERSON_DISABILITY, null, contentValues);
        }

        return itemPerson.personId;
    }

    public void saveCampDetails(String campId, String campTitle, String campStartDate, String campEndDate) {
        SQLiteDatabase db = this.getWritableDatabase();

        ItemCamp itemCamp = getCampDetails();

        ContentValues contentValues = new ContentValues();

        contentValues.put(CAMP_ID, campId);
        contentValues.put(CAMP_TITLE, campTitle);
        contentValues.put(CAMP_START_DATE, campStartDate);
        contentValues.put(CAMP_END_DATE, campEndDate);

        if (null == itemCamp) {
            db.insert(TABLE_CAMP, null, contentValues);
        } else {
            db.update(TABLE_CAMP, contentValues, CAMP_KEY + " = " + itemCamp.campIdKey, null);
        }
    }

    public ItemCamp getCampDetails() {

        SQLiteDatabase db = getReadableDatabase();

        ItemCamp itemCamp = null;

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_CAMP + " ORDER BY " + CAMP_KEY + " ASC ", null);

        if (cursor.moveToFirst()) {
            itemCamp = new ItemCamp(cursor.getLong(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4));
        }

        cursor.close();

        return itemCamp;
    }

    public ItemLocality addLocality(String locality) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(LOCALITY, locality);

        long id = db.insert(TABLE_LOCALITY, null, contentValues);

        return new ItemLocality(id, locality);
    }

    public void updateLocality(ItemLocality itemLocality) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(LOCALITY, itemLocality.locality);

        db.update(TABLE_LOCALITY, contentValues, LOCALITY_KEY + " = " + itemLocality.localityId, null);
    }

    public ArrayList<ItemLocality> getLocalityList() {
        ArrayList<ItemLocality> itemLocalities = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_LOCALITY + " ORDER BY " + LOCALITY_KEY + " ASC ", null);

        if (cursor.moveToFirst()) {
            do {
                itemLocalities.add(new ItemLocality(
                        cursor.getInt(0),
                        cursor.getString(1)
                ));

            } while (cursor.moveToNext());
        }

        cursor.close();

        return itemLocalities;
    }

    public void removeLocality(long localityId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_LOCALITY, LOCALITY_KEY + " = '" + localityId + "'", null);
    }

    public String[] getLocalities() {
        ArrayList<ItemLocality> itemLocalities = getLocalityList();

        String[] localities = new String[itemLocalities.size() + 1];
        localities[0] = mContext.getString(R.string.text_select);

        for (int i = 0; i < itemLocalities.size(); ++i) {
            localities[i + 1] = itemLocalities.get(i).locality;
        }

        return localities;
    }

    public String[] getHouseTypes() {
        return mContext.getResources().getStringArray(R.array.house_types);
    }

    public String[] getHouseNumbers() {

        String[] householdIds = null;

        SQLiteDatabase db = getReadableDatabase();

//        Cursor cursor = db.rawQuery("SELECT " + HOUSE_NUMBER + " FROM "
//                + TABLE_HOUSEHOLD + " ORDER BY " + HOUSE_NUMBER + " DESC", null);

        Cursor cursor = db.rawQuery("SELECT house_number ,"
                + " RTRIM(house_number, '/') AS first, "
                + " REPLACE(house_number, house_number, SUBSTR(house_number , instr(house_number, '/')+1)) AS second "
                + " FROM " + TABLE_HOUSEHOLD
                + " ORDER BY " + " CAST(second AS INTEGER) , CAST(first AS INTEGER) ASC", null);

        if (cursor.moveToFirst()) {
//            householdIds = new String[cursor.getCount() + 1];
//            householdIds[0] = mContext.getString(R.string.text_select);

            householdIds = new String[cursor.getCount()];

            int i = 0;

            do {
                householdIds[i++] = cursor.getString(0);
            } while (cursor.moveToNext());
        } else {
            householdIds = new String[0];
//            householdIds[0] = mContext.getString(R.string.text_select);
        }

        cursor.close();

        return householdIds;
    }

    public String[] getFamilyIds() {

        String[] familyIds = null;

        SQLiteDatabase db = getReadableDatabase();

//        Cursor cursor = db.rawQuery("SELECT " + FAMILY_ID + " FROM " + TABLE_FAMILY + " ORDER BY " + FAMILY_KEY + " DESC", null);
        Cursor cursor = db.rawQuery("SELECT family_id ,"
                + " RTRIM(family_id, '/') AS first, "
                + " REPLACE(family_id, family_id, SUBSTR(family_id , instr(family_id, '/')+1)) AS second "
                + " FROM " + TABLE_FAMILY
                + " ORDER BY " + " CAST(second AS INTEGER) , CAST(first AS INTEGER) ASC", null);

        if (cursor.moveToFirst()) {
//            familyIds = new String[cursor.getCount() + 1];
//            familyIds[0] = mContext.getString(R.string.text_select);
            familyIds = new String[cursor.getCount()];

            int i = 0;

            do {
                familyIds[i++] = cursor.getString(0);
            } while (cursor.moveToNext());
        } else {
            familyIds = new String[0];
//            familyIds[0] = mContext.getString(R.string.text_select);
        }

        cursor.close();

        return familyIds;
    }

    public String[] getRationTypes() {
        return mContext.getResources().getStringArray(R.array.ration_types);
    }

    public String[] getInsuranceTypes() {
        return mContext.getResources().getStringArray(R.array.insurance_types);
    }

    public String[] getTreatmentPlaces() {
        return mContext.getResources().getStringArray(R.array.treatment_places);
    }

    public String[] getDeathCauses() {
        return mContext.getResources().getStringArray(R.array.death_causes);
    }

    public String[] getAgeTimes() {
        return mContext.getResources().getStringArray(R.array.age_time);
    }

    public String[] getHouseStatuses() {
        return mContext.getResources().getStringArray(R.array.house_status);
    }

    public String[] getHouseTypeRent() {
        return mContext.getResources().getStringArray(R.array.house_ownership_types);
    }

    public String[] getRelationTypes(int sex) {
        if (0 == sex) {
            return mContext.getResources().getStringArray(R.array.relation_to_hh_male);
        } else if (1 == sex) {
            return mContext.getResources().getStringArray(R.array.relation_to_hh_female);
        } else {
            return mContext.getResources().getStringArray(R.array.relation_to_hh_other);
        }
    }

    public String[] getResidentTypeList() {
        return mContext.getResources().getStringArray(R.array.resident_status);
    }

    public String[] getEducationList() {
        return mContext.getResources().getStringArray(R.array.education);
    }

    public String[] getMaritalList(int sex) {
        if (0 == sex) {
            return mContext.getResources().getStringArray(R.array.marital_status_male);
        } else if (1 == sex) {
            return mContext.getResources().getStringArray(R.array.marital_status_female);
        } else {
            return mContext.getResources().getStringArray(R.array.marital_status_other);
        }
    }

    public String[] getLiteracyList() {
        return mContext.getResources().getStringArray(R.array.literacy);
    }

    public String[] getOccupationList() {
        return mContext.getResources().getStringArray(R.array.occupation);
    }

    public String[] getOccupationTypeList() {
        return mContext.getResources().getStringArray(R.array.occupation_type);
    }

    public String[] getIncomeTypeList() {
        return mContext.getResources().getStringArray(R.array.income_types);
    }

    public String[] getIdentityList() {
        return mContext.getResources().getStringArray(R.array.identity_types);
    }

    public String[] getTreatmentStatusList() {
        return mContext.getResources().getStringArray(R.array.yes_no_dont_know);
    }

    public String[] getHeightUnitList() {
        return mContext.getResources().getStringArray(R.array.height_unit_types);
    }

    public boolean localityExists(String locality) {

        String[] localities = getLocalities();

        for (String localityItem : localities) {

            if (localityItem.equals(locality)) {
                return true;
            }
        }

        return false;
    }

    public boolean nurseExists(String nurseUserName) {

        for (ItemNurse itemNurse : getNurseList()) {
            if (itemNurse.nurseUserName.equals(nurseUserName)) {
                return true;
            }
        }

        return false;
    }

    public int getTotalPersonInFamily(String familyId) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PERSON + " WHERE " + FAMILY_ID + " = '" + familyId + "'", null);

        if (cursor.moveToFirst()) {
            return cursor.getCount();
        }

        cursor.close();

        return 0;
    }

    public String getHouseNumber(String householdId, SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("SELECT " + HOUSE_NUMBER + " FROM "
                + TABLE_HOUSEHOLD + " WHERE " + "household_id" + " = '" + householdId + "'", null);

        if (cursor.moveToFirst()) {
            return cursor.getString(0);
        }

        cursor.close();

        return "";
//        Cursor cursor = db.rawQuery("SELECT * FROM "
//                + TABLE_HOUSEHOLD + " WHERE " + "household_id" + " = '" + householdId + "'", null);
//
//        if (cursor.moveToFirst()) {
//            return cursor.getString(2) + "/" + cursor.getString(3);
//        }
//
//        cursor.close();
//
//        return "";
    }

    private double findBmi(String sHeight, String heightUnit, String sWeight) {
        double bmiRoundOff = 0;

        try {
            float height = Float.parseFloat(sHeight);
            float weight = Float.parseFloat(sWeight);

            float bmi = 0.0f;

            // Find height in metre

            if (heightUnit.equals(getHeightUnitList()[1])) { // cm
                height = height / 100.0f;
                bmi = weight / (height * height);

                bmiRoundOff = Math.round(bmi * 100.0) / 100.0;

            } else if (heightUnit.equals(getHeightUnitList()[2])) { // inch
                height = height * 0.0254f;
                bmi = weight / (height * height);

                bmiRoundOff = Math.round(bmi * 100.0) / 100.0;
            }
        } catch (Exception e) {

        }

        return bmiRoundOff;
    }

    public boolean isHouseNoExists(String houseNo) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_HOUSEHOLD + " WHERE " + HOUSE_NUMBER + " = '" + houseNo + "'", null);

        if (cursor.moveToFirst()) {
            if (cursor.getCount() > 0) {
                return true;
            }
        }

        cursor.close();

        return false;
    }

    public int getTotalSurveyDone(String houseNo) {
        int            totalSurveyDone = 0;
        SQLiteDatabase db              = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PERSON + " WHERE "
                + PERSON_ID + " LIKE '" + houseNo + "%'", null);

        if (cursor.moveToFirst()) {
            totalSurveyDone = cursor.getCount();
        }

        cursor.close();

        return totalSurveyDone;
    }

    public void setActiveHousehold(long householdIdKey, String activeStatus, String inactiveReason) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(INACTIVE, activeStatus);
        contentValues.put(INACTIVE_REASON, inactiveReason);

        db.update(TABLE_HOUSEHOLD, contentValues, HOUSEHOLD_KEY + " = " + householdIdKey, null);
    }

    public boolean isHouseholdActive(long householdIdKey) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT " + INACTIVE + " FROM " + TABLE_HOUSEHOLD + " WHERE " + HOUSEHOLD_KEY + " = " + householdIdKey, null);

        boolean active = true;

        if (cursor.moveToFirst()) {
            String inactive = cursor.getString(0);

            if (null != inactive && inactive.equals("Yes")) {
                active = false;
            }
        }

        cursor.close();
        return active;
    }

    public String getHouseInactiveReason(long householdIdKey) {
        SQLiteDatabase db             = getReadableDatabase();
        String         inactiveReason = "";

        Cursor cursor = db.rawQuery("SELECT " + INACTIVE_REASON + " FROM " + TABLE_HOUSEHOLD + " WHERE " + HOUSEHOLD_KEY + " = " + householdIdKey, null);

        if (cursor.moveToFirst()) {
            inactiveReason = cursor.getString(0);
        }

        cursor.close();

        return inactiveReason;
    }

    public void setActiveFamily(long familyIdKey, String activeStatus, String inactiveReason) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(INACTIVE, activeStatus);
        contentValues.put(INACTIVE_REASON, inactiveReason);

        db.update(TABLE_FAMILY, contentValues, FAMILY_KEY + " = " + familyIdKey, null);
    }

    public boolean isFamilyActive(long familyIdKey) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT " + INACTIVE + " FROM " + TABLE_FAMILY + " WHERE " + FAMILY_KEY + " = " + familyIdKey, null);

        boolean active = true;

        if (cursor.moveToFirst()) {
            String inactive = cursor.getString(0);

            if (null != inactive && inactive.equals("Yes")) {
                active = false;
            }
        }

        cursor.close();
        return active;
    }

    public String getFamilyInactiveReason(long familyIdKey) {
        SQLiteDatabase db             = getReadableDatabase();
        String         inactiveReason = "";

        Cursor cursor = db.rawQuery("SELECT " + INACTIVE_REASON + " FROM " + TABLE_FAMILY + " WHERE " + FAMILY_KEY + " = " + familyIdKey, null);

        if (cursor.moveToFirst()) {
            inactiveReason = cursor.getString(0);
        }

        cursor.close();

        return inactiveReason;
    }

    public void setActivePerson(long personIdKey, String activeStatus, String inactiveReason) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(INACTIVE, activeStatus);
        contentValues.put(INACTIVE_REASON, inactiveReason);

        db.update(TABLE_PERSON, contentValues, PERSON_KEY + " = " + personIdKey, null);
    }

    public boolean isPersonActive(long personIdKey) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT " + INACTIVE + " FROM " + TABLE_PERSON + " WHERE " + PERSON_KEY + " = " + personIdKey, null);

        boolean active = true;

        if (cursor.moveToFirst()) {
            String inactive = cursor.getString(0);

            if (null != inactive && inactive.equals("Yes")) {
                active = false;
            }
        }

        cursor.close();
        return active;
    }

    public String getPersonInactiveReason(long personIdKey) {
        SQLiteDatabase db             = getReadableDatabase();
        String         inactiveReason = "";

        Cursor cursor = db.rawQuery("SELECT " + INACTIVE_REASON + " FROM " + TABLE_PERSON + " WHERE " + PERSON_KEY + " = " + personIdKey, null);

        if (cursor.moveToFirst()) {
            inactiveReason = cursor.getString(0);
        }

        cursor.close();

        return inactiveReason;
    }
}
