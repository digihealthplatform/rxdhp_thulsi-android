package com.dhp.hins.storage;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.dhp.hins.item.ItemFamily;
import com.dhp.hins.item.ItemHousehold;
import com.dhp.hins.item.ItemPerson;
import com.dhp.hins.utils.LConstants;
import com.dhp.hins.utils.LUtils;

import static com.dhp.hins.storage.LDatabase.TABLE_CAMP;
import static com.dhp.hins.storage.LDatabase.TABLE_FAMILY;
import static com.dhp.hins.storage.LDatabase.TABLE_HOUSEHOLD;
import static com.dhp.hins.storage.LDatabase.TABLE_LOCALITY;
import static com.dhp.hins.storage.LDatabase.TABLE_NURSE;
import static com.dhp.hins.storage.LDatabase.TABLE_PERSON;
import static com.dhp.hins.utils.LConstants.KEY_NURSE_NAME;
import static com.dhp.hins.utils.LUtils.toYesNo;

/**
 * Created by Prabhat on 02-Aug-17.
 */

public class LDbTransfer {
    private static final String TAG = "LDbTransfer";

    public static void appendRows(Context context) {
        LDatabase db = LDatabase.getInstance(context);

        String[] rows = LConstants.temp.split("\n");

        Log.d(TAG, "appendRows: " + rows.length);

        for (String row : rows) {
            try {
                String houseNo  = row.split("-")[0];
                String allNames = row.split("-")[1];

                String firstName = allNames.split("W/o")[0];

                String secondName = null;

                try {
                    secondName = allNames.split("W/o")[1];
                } catch (Exception e) {

                }

                ItemHousehold itemHousehold = new ItemHousehold();
                itemHousehold.houseNo1 = houseNo.split("/")[0];
                itemHousehold.houseNo2 = houseNo.split("/")[1];
                itemHousehold.houseLocality = "Indirapuram";

                if (firstName.equals("Locked") || firstName.equals("Vacant")) {
                    itemHousehold.houseStatus = firstName;
                } else {
//                    itemHousehold.houseStatus = "Open";
                    itemHousehold.houseHead = secondName;
                    itemHousehold.houseHeadSpouse = firstName;
                }

                Log.d(TAG, "appendRows: houseNo " + itemHousehold.houseNo1 + "/" + itemHousehold.houseNo2
                        + ", houseStatus " + itemHousehold.houseStatus
                        + ", houseHead " + itemHousehold.houseHead
                        + ", houseHeadSpouse " + itemHousehold.houseHeadSpouse);


                if (!db.isHouseNoExists(itemHousehold.houseNo1 + "/" + itemHousehold.houseNo2)) {
                    db.addHousehold(itemHousehold);
                }

            } catch (Exception e) {

            }
        }
    }


    public static void transfer(SQLiteDatabase oldDb, Context context) {
        LDatabase newDb = LDatabase.getInstance(context);

        // Camp details
        try {
            Cursor cursor = oldDb.rawQuery("SELECT * FROM " + TABLE_CAMP, null);

            if (cursor.moveToFirst()) {
                newDb.saveCampDetails(cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4)
                );
            }
            cursor.close();

        } catch (SQLException e) {

        }

        // Localities
        try {
            Cursor cursor = oldDb.rawQuery("SELECT * FROM " + TABLE_LOCALITY, null);

            if (cursor.moveToFirst()) {
                do {
                    newDb.addLocality(cursor.getString(1));
                } while (cursor.moveToNext());
            }
            cursor.close();

        } catch (SQLException e) {

        }

        // Health workers
        try {
            Cursor cursor = oldDb.rawQuery("SELECT * FROM " + TABLE_NURSE, null);

            if (cursor.moveToFirst()) {
                do {
                    newDb.addNurse(cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3));
                } while (cursor.moveToNext());
            }
            cursor.close();

        } catch (SQLException e) {

        }

        // Add household
        try {
            Cursor cursor = oldDb.rawQuery("SELECT * FROM " + TABLE_HOUSEHOLD, null);

            if (cursor.moveToFirst()) {
                do {
                    if (!cursor.getString(4).equals("Indirapuram")) {
                        continue;
                    }
                    ItemHousehold itemHousehold = new ItemHousehold();

                    itemHousehold.houseNo1 = cursor.getString(2).split("/")[0];

                    try {
                        itemHousehold.houseNo2 = cursor.getString(2).split("/")[1];
                    } catch (Exception e) {
                        itemHousehold.houseNo2 = cursor.getString(3);
                    }

                    itemHousehold.houseLocality = cursor.getString(4);
                    itemHousehold.houseType = cursor.getString(5);
                    itemHousehold.totalRooms = cursor.getInt(6);
                    itemHousehold.houseHead = cursor.getString(7);
                    itemHousehold.houseHeadSpouse = cursor.getString(8);

                    itemHousehold.campId = cursor.getString(9);
                    itemHousehold.surveyPerson = cursor.getString(10);
                    itemHousehold.surveyDate = cursor.getString(11);

                    LUtils.putStringPref("s_date", cursor.getString(11));

                    LUtils.putStringPref(KEY_NURSE_NAME, itemHousehold.surveyPerson);

                    newDb.addHousehold(itemHousehold);
                } while (cursor.moveToNext());
            }
            cursor.close();

        } catch (SQLException e) {

        }

        // Add family
        try {
            Cursor cursor  = oldDb.rawQuery("SELECT * FROM " + TABLE_FAMILY, null);
            Cursor cursor1 = oldDb.rawQuery("SELECT * FROM " + TABLE_HOUSEHOLD, null);

            if (cursor.moveToFirst()) {
                do {
                    if (!exists(cursor.getString(1), cursor1)) {
                        continue;
                    }

                    ItemFamily itemFamily = new ItemFamily();
                    itemFamily.familyId = cursor.getString(1);

                    itemFamily.houseNumber = newDb.getHouseNumber(cursor.getString(2), oldDb);

                    itemFamily.rationCard = toYesNo(cursor.getInt(4));
                    itemFamily.rationCardType = cursor.getString(3);
                    itemFamily.insurance = toYesNo(cursor.getInt(5));
                    itemFamily.insuranceType = cursor.getString(6);
                    itemFamily.treatmentPlace = cursor.getString(10);

                    LUtils.putStringPref("s_date", cursor.getString(16));

                    newDb.addFamily(itemFamily);
                } while (cursor.moveToNext());
            }
            cursor1.close();
            cursor.close();

        } catch (SQLException e) {

        }

        // Individual
        try {
            Cursor cursor1 = oldDb.rawQuery("SELECT * FROM " + TABLE_HOUSEHOLD, null);
            Cursor cursor  = oldDb.rawQuery("SELECT * FROM " + TABLE_PERSON, null);

            if (cursor.moveToFirst()) {
                do {
                    if (!exists(cursor.getString(1), cursor1)) {
                        continue;
                    }
                    ItemPerson person = new ItemPerson();

                    String householdId = cursor.getString(2).split("/")[0]
                            + "/" + cursor.getString(2).split("/")[1];

                    person.familyId = newDb.getHouseNumber(householdId, oldDb) + "/" + cursor.getString(2).split("/")[2];
                    person.name = cursor.getString(3);
                    person.sex = cursor.getString(4);
                    person.dob = cursor.getString(5);
                    person.ageYear = cursor.getInt(6);
                    person.ageMonth = cursor.getInt(7);
                    person.ageDay = cursor.getInt(8);
                    person.relationToHoh = cursor.getString(9);
                    person.resident = cursor.getString(10);
                    person.marital = cursor.getString(11);
                    person.education = cursor.getString(12);
                    person.literacy = cursor.getString(13);
                    person.occupation = cursor.getString(14);
                    person.occupationType = cursor.getString(15);
                    person.income = cursor.getInt(16);
                    person.identityType = cursor.getString(17);
                    person.identityNo = cursor.getString(18);
                    person.height = cursor.getString(19);
                    person.heightUnit = cursor.getString(20);
                    person.weight = cursor.getString(21);
                    person.weightUnit = cursor.getString(22);
                    person.bpSystolic = cursor.getString(23);
                    person.bpDiastolic = cursor.getString(24);
                    person.rbs = cursor.getString(25);
                    person.hb = cursor.getString(26);

                    LUtils.putStringPref("s_date", cursor.getString(29));
                    newDb.addPerson(person);
                } while (cursor.moveToNext());
            }
            cursor.close();

        } catch (SQLException e) {

        }

        ((Activity) context).recreate();
    }

    private static boolean exists(String familyId, Cursor cursor) {
        String familyIdStripped = familyId.split("/")[0] + "/" + familyId.split("/")[1];

        if (cursor.moveToFirst()) {
            do {
                if (familyIdStripped.equals(cursor.getString(1)) && cursor.getString(4).equals("Indirapuram")) {
                    return true;
                }
            }

            while (cursor.moveToNext());
        }

        return false;
    }
}
