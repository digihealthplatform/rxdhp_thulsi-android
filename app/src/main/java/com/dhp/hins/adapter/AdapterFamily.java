package com.dhp.hins.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dhp.hins.R;
import com.dhp.hins.item.ItemFamilyMin;
import com.dhp.hins.storage.LDatabase;
import com.dhp.hins.view.family.ActivityFamilyDetails;
import com.dhp.hins.view.family.ActivityFamilyEntry;

import java.util.ArrayList;

import static com.dhp.hins.utils.LConstants.KEY_FAMILY_ID;
import static com.dhp.hins.utils.LConstants.KEY_LAUNCH_TYPE;
import static com.dhp.hins.utils.LConstants.LAUNCH_TYPE_EDIT;

/**
 * Created by Prabhat on 16-Apr-17.
 */

public class AdapterFamily extends RecyclerView.Adapter<AdapterFamily.ViewHolder> {

    private ArrayList<ItemFamilyMin> mItems;

    private final Context mContext;
    private final LayoutInflater mLayoutInflater;
    private final LDatabase mDb;

    public AdapterFamily(Context context) {
        mContext = context;
        mDb = LDatabase.getInstance(context);
        mItems = mDb.getFamilyList();

        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public AdapterFamily.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_family, parent, false);
        return new AdapterFamily.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterFamily.ViewHolder holder, int position) {
        ItemFamilyMin itemFamily = mItems.get(position);

        holder.tvFamilyId.setText(itemFamily.familyId);
    }

    @Override
    public int getItemCount() {
        if (null != mItems) {
            return mItems.size();
        }

        return 0;
    }

    public void addItem(ItemFamilyMin itemFamily) {
        mItems.add(itemFamily);
        notifyItemInserted(mItems.size() - 1);
    }

    public void clear() {
        mItems.clear();
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
//        mDb.removeHousehold(mItems.get(position).familyIdKey);
//        mItems.remove(position);
//        notifyItemRemoved(position);
//        Toasty.success(mContext, mContext.getString(R.string.msg_remove_success), Toast.LENGTH_SHORT).show();
    }

    public void onSearchTextChanged(String searchText) {
        mItems = mDb.getFamilyList(searchText);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener {

        private TextView tvFamilyId;

        ViewHolder(View itemView) {
            super(itemView);

            tvFamilyId = (TextView) itemView.findViewById(R.id.tv_family_id);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(mContext, ActivityFamilyDetails.class);
            intent.putExtra(KEY_FAMILY_ID, mItems.get(getAdapterPosition()).familyIdKey);
            mContext.startActivity(intent);
        }

        @Override
        public boolean onLongClick(View view) {
            if (!mDb.isFamilyActive(mItems.get(getAdapterPosition()).familyIdKey)) {
                return true;
            }

            String options_array[] = {mContext.getString(R.string.option_edit)};

            AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialogLight);

            builder.setItems(options_array, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    switch (which) {
                        case 0: {
                            Intent intent = new Intent(mContext, ActivityFamilyEntry.class);
                            intent.putExtra(KEY_LAUNCH_TYPE, LAUNCH_TYPE_EDIT);
                            intent.putExtra(KEY_FAMILY_ID, mItems.get(getAdapterPosition()).familyIdKey);
                            mContext.startActivity(intent);
                        }
                        break;
                    }
                }
            });

            builder.show();

            return true;
        }
    }
}