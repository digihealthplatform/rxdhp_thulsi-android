package com.dhp.hins.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.hins.R;
import com.dhp.hins.item.ItemLocality;
import com.dhp.hins.storage.LDatabase;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

/**
 * Created by Prabhat on 15-Apr-17.
 */

public class AdapterLocality extends RecyclerView.Adapter<AdapterLocality.ViewHolder> {

    private ArrayList<ItemLocality> mItems;

    private final Context        mContext;
    private final LayoutInflater mLayoutInflater;
    private final LDatabase      mDb;

    public AdapterLocality(Context context, ArrayList<ItemLocality> items) {
        mContext = context;
        mItems = items;
        mDb = LDatabase.getInstance(context);

        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public AdapterLocality.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_locality, parent, false);
        return new AdapterLocality.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterLocality.ViewHolder holder, int position) {
        holder.tvLocality.setText(mItems.get(position).locality);
    }

    @Override
    public int getItemCount() {
        if (null != mItems) {
            return mItems.size();
        }

        return 0;
    }

    public void addItem(ItemLocality itemLocality) {
        mItems.add(itemLocality);
        notifyItemInserted(mItems.size() - 1);
    }

    public void clear() {
        mItems.clear();
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        mDb.removeLocality(mItems.get(position).localityId);
        mItems.remove(position);
        notifyItemRemoved(position);
        Toasty.success(mContext, mContext.getString(R.string.msg_remove_success), Toast.LENGTH_SHORT).show();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        private TextView tvLocality;

        ViewHolder(View itemView) {
            super(itemView);

            tvLocality = (TextView) itemView.findViewById(R.id.tv_locality);

            itemView.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {

            String options_array[] = {mContext.getString(R.string.option_edit), mContext.getString(R.string.option_remove)};

            AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialogLight);;

            builder.setItems(options_array, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    switch (which) {
                        case 0: {
                            updateLocality(mItems.get(getAdapterPosition()));
                        }
                        break;
                        
                        case 1: {
                            AlertDialog.Builder builder;

                            builder = new AlertDialog.Builder(mContext, R.style.AlertDialogLight);

                            builder.setMessage(mContext.getString(R.string.message_remove_locality));
                            builder.setPositiveButton(mContext.getString(R.string.btn_remove), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    removeItem(getAdapterPosition());
                                }
                            });
                            builder.setNegativeButton(mContext.getString(R.string.btn_cancel), null);
                            builder.show();
                        }
                        break;
                    }
                }
            });

            builder.show();

            return true;
        }
    }
    
    private void updateLocality(final ItemLocality itemLocality) {
        View dialogView = LayoutInflater.from(mContext).inflate(R.layout.dialog_add_locality, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(mContext, R.style.AlertDialogLight);

        builder.setView(dialogView);

        builder.setPositiveButton(mContext.getString(R.string.btn_save), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(mContext.getString(R.string.btn_cancel), null);

        final AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);

        final EditText etLocality = (EditText) dialogView.findViewById(R.id.et_locality);
        etLocality.setText(itemLocality.locality);
        etLocality.setSelection(etLocality.getText().length());

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String locality = etLocality.getText().toString();

                if (!locality.equals("")) {

                    if (locality.equals(itemLocality.locality)) {
                        dialog.dismiss();
                        return;
                    }
                    
                    if (mDb.localityExists(locality)) {
                        Toasty.warning(mContext, mContext.getString(R.string.msg_locality_exists), Toast.LENGTH_SHORT).show();

                        return;
                    }
                    
                    itemLocality.locality = locality;
                    
                    mDb.updateLocality(itemLocality);
                    
                    mItems = mDb.getLocalityList();
                    
                    notifyDataSetChanged();

                    dialog.dismiss();
                    Toasty.success(mContext, mContext.getString(R.string.msg_add_success), Toast.LENGTH_SHORT).show();
                } else {
                    Toasty.warning(mContext, mContext.getString(R.string.msg_fill_all_fields), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}