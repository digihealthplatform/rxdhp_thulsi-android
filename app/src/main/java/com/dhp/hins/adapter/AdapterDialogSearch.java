package com.dhp.hins.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dhp.hins.R;
import com.dhp.hins.utils.DialogSearchCallbacks;

import java.util.ArrayList;

/**
 * Created by Prabhat on 27-Jul-17.
 */

public class AdapterDialogSearch extends RecyclerView.Adapter<AdapterDialogSearch.ViewHolder> {
    private final DialogSearchCallbacks mCallback;
    private ArrayList<String> mItems;

    private final LayoutInflater mLayoutInflater;

    public AdapterDialogSearch(DialogSearchCallbacks callbacks, Context context, ArrayList<String> items) {
        mCallback = callbacks;
        mItems = items;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public AdapterDialogSearch.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_relation, parent, false);
        return new AdapterDialogSearch.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterDialogSearch.ViewHolder holder, int position) {
        holder.tvRelation.setText(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        if (null != mItems) {
            return mItems.size();
        }

        return 0;
    }

    public void setNewList(ArrayList<String> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvRelation;

        ViewHolder(View itemView) {
            super(itemView);
            tvRelation = (TextView) itemView.findViewById(R.id.tv_relation);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mCallback.onSelected(mItems.get(getAdapterPosition()));
        }
    }
}