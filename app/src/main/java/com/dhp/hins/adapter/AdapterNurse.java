package com.dhp.hins.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.hins.R;
import com.dhp.hins.item.ItemNurse;
import com.dhp.hins.storage.LDatabase;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

/**
 * Created by Prabhat on 25-Jan-17.
 */

public class AdapterNurse extends RecyclerView.Adapter<AdapterNurse.ViewHolder> {

    private ArrayList<ItemNurse> mItems;

    private final Context        mContext;
    private final LayoutInflater mLayoutInflater;
    private final LDatabase      mDb;

    public AdapterNurse(Context context, ArrayList<ItemNurse> items) {
        mContext = context;
        mItems = items;
        mDb = LDatabase.getInstance(context);
        
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public AdapterNurse.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_nurse, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvNurseName.setText(mItems.get(position).nurseName);
    }

    @Override
    public int getItemCount() {
        if (null != mItems) {
            return mItems.size();
        }
        
        return 0;
    }

    public void addItem(ItemNurse itemNurse) {
        mItems.add(itemNurse);
        notifyItemInserted(mItems.size() - 1);
    }

    public void clear() {
        mItems.clear();
        notifyDataSetChanged();
    }
    
    public void removeItem(int position) {
        mDb.removeNurse(mItems.get(position).nurseIdKey);
        mItems.remove(position);
        notifyItemRemoved(position);
        Toasty.success(mContext, mContext.getString(R.string.msg_remove_success), Toast.LENGTH_SHORT).show();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        private TextView tvNurseName;

        ViewHolder(View itemView) {
            super(itemView);

            tvNurseName = (TextView) itemView.findViewById(R.id.tv_nurse_name);

            itemView.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {

            String options_array[] = {mContext.getString(R.string.option_edit), mContext.getString(R.string.option_remove)};

            AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialogLight);;

            builder.setItems(options_array, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    switch (which) {
                        case 0: {
                            updateNurse(mItems.get(getAdapterPosition()));
                        }
                        break;
                        
                        case 1: {
                            AlertDialog.Builder builder;

                            builder = new AlertDialog.Builder(mContext, R.style.AlertDialogLight);

                            builder.setMessage(mContext.getString(R.string.message_remove_nurse));
                            builder.setPositiveButton(mContext.getString(R.string.btn_remove), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    removeItem(getAdapterPosition());
                                }
                            });
                            builder.setNegativeButton(mContext.getString(R.string.btn_cancel), null);
                            builder.show();
                        }
                        break;
                    }
                }
            });

            builder.show();

            return true;
        }
    }
    
    private void updateNurse(final ItemNurse itemNurse) {
        View dialogView = LayoutInflater.from(mContext).inflate(R.layout.dialog_add_nurse, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(mContext, R.style.AlertDialogLight);

        builder.setView(dialogView);

        builder.setPositiveButton(mContext.getString(R.string.btn_save), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(mContext.getString(R.string.btn_cancel), null);

        final AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);

        final EditText etNurseName = (EditText) dialogView.findViewById(R.id.et_nurse_name);
        etNurseName.setText(itemNurse.nurseName);
        etNurseName.setSelection(etNurseName.getText().length());
        
        final EditText etNurseUserName = (EditText) dialogView.findViewById(R.id.et_nurse_user_name);
        etNurseUserName.setText(itemNurse.nurseUserName);
        
        final EditText etNursePassword   = (EditText) dialogView.findViewById(R.id.et_nurse_password);
        etNursePassword.setText(itemNurse.nursePassword);

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nurseName     = etNurseName.getText().toString();
                String nursePassword = etNursePassword.getText().toString();
                String nurseUserName = etNurseUserName.getText().toString();

                if (!nurseName.equals("") && !nursePassword.equals("") && !nurseUserName.equals("")) {

                    if (itemNurse.nurseName.equals(nurseName) &&
                            itemNurse.nurseUserName.equals(nurseUserName) &&
                            itemNurse.nursePassword.equals(nursePassword)) {
                        dialog.dismiss();
                        return;
                    }
                    
                    if (mDb.nurseExists(nurseUserName) && !itemNurse.nurseUserName.equals(nurseUserName)) {
                        Toasty.warning(mContext, mContext.getString(R.string.msg_nurse_exits), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    
                    itemNurse.nurseName = nurseName;
                    itemNurse.nurseUserName = nurseUserName;
                    itemNurse.nursePassword = nursePassword;

                    mDb.updateNurse(itemNurse);

                    mItems = mDb.getNurseList();
                    
                    notifyDataSetChanged();
                    
                    dialog.dismiss();
                    Toasty.success(mContext, mContext.getString(R.string.msg_add_success), Toast.LENGTH_SHORT).show();

                } else {
                    Toasty.warning(mContext, mContext.getString(R.string.msg_fill_all_fields), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}