package com.dhp.hins.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.dhp.hins.R;
import com.dhp.hins.item.ItemDeathInfo;
import com.dhp.hins.utils.DialogDeathInfo;
import com.dhp.hins.utils.DialogDeathInfoCallbacks;

import java.util.ArrayList;

import static android.view.MotionEvent.ACTION_UP;
import static com.dhp.hins.utils.LApplication.getAppContext;

/**
 * Created by Prabhat on 29-Jul-17.
 */

public class AdapterDeathInfo extends RecyclerView.Adapter<AdapterDeathInfo.ViewHolder> {
    private final Context                  mContext;
    private       ArrayList<ItemDeathInfo> mItems;
    private final LayoutInflater           mLayoutInflater;

    public AdapterDeathInfo(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    public void setItems(ArrayList<ItemDeathInfo> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    public ArrayList<ItemDeathInfo> getItems() {
        return mItems;
    }

    @Override
    public AdapterDeathInfo.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_death_info, parent, false);
        return new AdapterDeathInfo.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterDeathInfo.ViewHolder holder, int position) {
        String spinnerText = mItems.get(position).name;

        if (null == spinnerText || spinnerText.equals("")) {
            spinnerText = getAppContext().getString(R.string.add_information);
        }

        final ArrayAdapter<String> adapterDeath =
                new ArrayAdapter<>(getAppContext(), R.layout.spinner_layout,
                        new String[]{spinnerText});

        holder.spDeathInfo.setAdapter(adapterDeath);

        if (0 == position) {
            holder.ivClearDeathInfo.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if (null != mItems) {
            return mItems.size();
        }

        return 0;
    }

    public void addNewItem(boolean firstPosition) {
        if (firstPosition) {
            mItems.add(new ItemDeathInfo());
            notifyItemInserted(mItems.size() - 1);
            return;
        }

        new DialogDeathInfo((Activity) mContext, new DialogDeathInfoCallbacks() {
            @Override
            public void onDeathInfoChanged(ItemDeathInfo item, int position) {
                mItems.add(item);
                notifyItemInserted(mItems.size() - 1);
            }
        }, new ItemDeathInfo()).showDialog(mItems.size());
    }

    private void updateItem(ItemDeathInfo item, int position) {
        // Update old item
        mItems.remove(position);
        mItems.add(position, item);
        notifyDataSetChanged();
    }

    private void removeItem(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final Spinner   spDeathInfo;
        private final ImageView ivClearDeathInfo;

        ViewHolder(View itemView) {
            super(itemView);
            spDeathInfo = (Spinner) itemView.findViewById(R.id.sp_death_info);
            ivClearDeathInfo = (ImageView) itemView.findViewById(R.id.iv_clear_death_info);

            spDeathInfo.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (ACTION_UP == motionEvent.getAction()) {
                        // Show dialog
                        new DialogDeathInfo((Activity) mContext, new DialogDeathInfoCallbacks() {
                            @Override
                            public void onDeathInfoChanged(ItemDeathInfo item, int position) {
                                updateItem(item, position);
                            }
                        }, mItems.get(getAdapterPosition())).showDialog(getAdapterPosition());
                    }
                    return true;
                }
            });

            ivClearDeathInfo.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            // Confirm
            AlertDialog.Builder builder;

            builder = new AlertDialog.Builder(mContext, R.style.AlertDialogLight);

            builder.setMessage(mContext.getString(R.string.msg_remove_death_info));
            builder.setPositiveButton(mContext.getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    removeItem(getAdapterPosition());
                }
            });

            builder.setNegativeButton(mContext.getString(R.string.btn_cancel), null);
            builder.show();
        }
    }
}
