package com.dhp.hins.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dhp.hins.R;
import com.dhp.hins.item.ItemPersonMin;
import com.dhp.hins.storage.LDatabase;
import com.dhp.hins.view.person.ActivityPersonDetails;
import com.dhp.hins.view.person.ActivityPersonEntry;

import java.util.ArrayList;

import static com.dhp.hins.utils.LConstants.KEY_LAUNCH_TYPE;
import static com.dhp.hins.utils.LConstants.KEY_PERSON_ID;
import static com.dhp.hins.utils.LConstants.LAUNCH_TYPE_EDIT;

/**
 * Created by Prabhat on 17-Apr-17.
 */

public class AdapterPerson extends RecyclerView.Adapter<AdapterPerson.ViewHolder> {

    private ArrayList<ItemPersonMin> mItems;

    private final Context mContext;
    private final LayoutInflater mLayoutInflater;
    private final LDatabase mDb;

    public AdapterPerson(Context context) {
        mContext = context;
        mDb = LDatabase.getInstance(context);
        mItems = mDb.getPersonList();

        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public AdapterPerson.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_person, parent, false);
        return new AdapterPerson.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterPerson.ViewHolder holder, int position) {
        ItemPersonMin itemPerson = mItems.get(position);
        holder.tvPersonId.setText("Person ID: " + itemPerson.personId);
        holder.tvPersonName.setText(itemPerson.name);
    }

    @Override
    public int getItemCount() {
        if (null != mItems) {
            return mItems.size();
        }

        return 0;
    }

    public void addItem(ItemPersonMin itemPerson) {
        mItems.add(itemPerson);
        notifyItemInserted(mItems.size() - 1);
    }

    public void clear() {
        mItems.clear();
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
//        mDb.removeHousehold(mItems.get(position).personIdKey);
//        mItems.remove(position);
//        notifyItemRemoved(position);
//        Toasty.success(mContext, mContext.getString(R.string.msg_remove_success), Toast.LENGTH_SHORT).show();
    }

    public void onSearchTextChanged(String searchText) {
        mItems = mDb.getPersonListSearch(searchText);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener {

        private TextView tvPersonId;
        private TextView tvPersonName;

        ViewHolder(View itemView) {
            super(itemView);

            tvPersonId = (TextView) itemView.findViewById(R.id.tv_person_id);
            tvPersonName = (TextView) itemView.findViewById(R.id.tv_person_name);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(mContext, ActivityPersonDetails.class);
            intent.putExtra(KEY_PERSON_ID, mItems.get(getAdapterPosition()).personIdKey);
            mContext.startActivity(intent);
        }

        @Override
        public boolean onLongClick(View view) {
            if (!mDb.isPersonActive(mItems.get(getAdapterPosition()).personIdKey)) {
                return true;
            }

            String options_array[] = {mContext.getString(R.string.option_edit)};

            AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialogLight);

            builder.setItems(options_array, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    switch (which) {
                        case 0: {
                            Intent intent = new Intent(mContext, ActivityPersonEntry.class);
                            intent.putExtra(KEY_LAUNCH_TYPE, LAUNCH_TYPE_EDIT);
                            intent.putExtra(KEY_PERSON_ID, mItems.get(getAdapterPosition()).personIdKey);
                            mContext.startActivity(intent);
                        }
                        break;
                    }
                }
            });

            builder.show();

            return true;
        }
    }
}