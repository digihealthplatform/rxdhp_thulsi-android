package com.dhp.hins.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dhp.hins.R;
import com.dhp.hins.item.ItemHouseholdMin;
import com.dhp.hins.storage.LDatabase;
import com.dhp.hins.view.household.ActivityHouseholdDetails;
import com.dhp.hins.view.household.ActivityHouseholdEntry;

import java.util.ArrayList;

import static com.dhp.hins.utils.LConstants.KEY_HOUSEHOLD_ID;
import static com.dhp.hins.utils.LConstants.KEY_LAUNCH_TYPE;
import static com.dhp.hins.utils.LConstants.LAUNCH_TYPE_EDIT;

/**
 * Created by Prabhat on 15-Apr-17.
 */

public class AdapterHousehold extends RecyclerView.Adapter<AdapterHousehold.ViewHolder> {

    private ArrayList<ItemHouseholdMin> mItems;

    private final Context mContext;
    private final LayoutInflater mLayoutInflater;
    private final LDatabase mDb;

    public AdapterHousehold(Context context) {
        mContext = context;
        mDb = LDatabase.getInstance(context);
        mItems = mDb.getHouseholdList();

        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public AdapterHousehold.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_household, parent, false);
        return new AdapterHousehold.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterHousehold.ViewHolder holder, int position) {
        ItemHouseholdMin itemHousehold = mItems.get(position);

        holder.tvHouseholdId.setText(itemHousehold.houseNo);

        holder.tvHouseholdDetails.setText(mContext.getString(R.string.label_locality) + " " + itemHousehold.locality);
    }

    @Override
    public int getItemCount() {
        if (null != mItems) {
            return mItems.size();
        }

        return 0;
    }

    public void addItem(ItemHouseholdMin itemHousehold) {
        mItems.add(itemHousehold);
        notifyItemInserted(mItems.size() - 1);
    }

    public void clear() {
        mItems.clear();
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
//        mDb.removeHousehold(mItems.get(position).householdIdKey);
//        mItems.remove(position);
//        notifyItemRemoved(position);
//        Toasty.success(mContext, mContext.getString(R.string.msg_remove_success), Toast.LENGTH_SHORT).show();
    }

    public void onSearchTextChanged(String searchText) {
        mItems = mDb.getHouseholdList(searchText);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener {

        private TextView tvHouseholdId;
        private TextView tvHouseholdDetails;

        ViewHolder(View itemView) {
            super(itemView);

            tvHouseholdId = (TextView) itemView.findViewById(R.id.tv_household_id);
            tvHouseholdDetails = (TextView) itemView.findViewById(R.id.tv_household_details);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(mContext, ActivityHouseholdDetails.class);
            intent.putExtra(KEY_HOUSEHOLD_ID, mItems.get(getAdapterPosition()).householdIdKey);
            mContext.startActivity(intent);
        }

        @Override
        public boolean onLongClick(View view) {
            if (!mDb.isHouseholdActive(mItems.get(getAdapterPosition()).householdIdKey)) {
                return true;
            }

            String options_array[] = {mContext.getString(R.string.option_edit)};

            AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialogLight);

            builder.setItems(options_array, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    switch (which) {
                        case 0: {
                            Intent intent = new Intent(mContext, ActivityHouseholdEntry.class);
                            intent.putExtra(KEY_LAUNCH_TYPE, LAUNCH_TYPE_EDIT);
                            intent.putExtra(KEY_HOUSEHOLD_ID, mItems.get(getAdapterPosition()).householdIdKey);
                            mContext.startActivity(intent);
                        }
                        break;
                    }
                }
            });

            builder.show();

            return true;
        }
    }
}
